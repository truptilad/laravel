<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNetBankingColunms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting__incomes', function (Blueprint $table) {

            $table->string('cheque_bank')->nullable();
            $table->string('cheque_branch')->nullable();
            $table->date('cheque_date')->nullable();
            $table->string('transfer_to')->nullable();
            $table->string('reference_code')->nullable();
            $table->string('transfer_from')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting__incomes', function (Blueprint $table) {
           
            $table->dropColumn('cheque_bank')->nullable();
            $table->dropColumn('cheque_branch')->nullable();
            $table->dropColumn('cheque_date')->nullable();
            $table->dropColumn('transfer_to')->nullable();
            $table->dropColumn('reference_code')->nullable();
            $table->dropColumn('transfer_from')->nullable();

        });
    }
}
