<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTwoColumnsToAccounting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting__transactions', function (Blueprint $table) {
           $table->unsignedInteger('expense_id')->references('id')->on('accounting__expenses')->nullable();
            $table->unsignedInteger('payment_id')->references('id')->on('accounting__payments')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
