<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBranchAndCOmpanyInInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting__invoices', function (Blueprint $table) {
            $table->unsignedInteger('branch_id')->references('id')->on('master__branch');
            $table->unsignedInteger('company_id')->references('id')->on('master__companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting__invoices', function (Blueprint $table) {
            $table->dropColumn(['branch_id','company_id']);
        });
    }
}
