<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(env('APP_ENV') == "DEV"){
            $this->down();
        }
        Schema::create('accounting__accounttypes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('type')->nullable();
            $table->string('record_status')->nullable()->default("A");
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('accounting__accounts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->decimal('balance')->nullable();
            $table->unsignedInteger('user_id')->references('id')->on('user');
            $table->unsignedInteger('company_id')->references('id')->on('master__companies');
            $table->unsignedInteger('branch_id')->references('id')->on('master__branch');
            $table->unsignedInteger('account_type_id')->references('id')->on('accounting__accounttypes');
            $table->string('record_status')->nullable()->default("A");
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('accounting__expensecategories', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('type')->nullable();
            $table->text('description')->nullable();
            $table->string('record_status')->nullable()->default("A");
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('accounting__invoices', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('date')->nullable();
            $table->unsignedInteger('vendor_id')->references('id')->on('master__vendors')->nullable();
            $table->decimal('discount')->nullable();
            $table->decimal('amount')->nullable();
            $table->decimal('tax')->nullable();
            $table->date('due_date')->nullable();
            $table->decimal('fine')->nullable();
            $table->string('payment_status')->nullable();
            $table->unsignedInteger('status_id')->references('id')->on('workflow__workflowstatuses');
            $table->text('comment')->nullable();
            $table->string('invoice_no')->nullable();
            $table->unsignedInteger('file_id')->references('id')->on('filemanager__files');
            $table->string('record_status')->nullable()->default("A");
            $table->unsignedInteger('created_by')->references('id')->on('user');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('accounting__payments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('mode')->nullable();
            $table->decimal('amount')->nullable();
            $table->string('status')->nullable();
            $table->date('date')->nullable();
            $table->unsignedInteger('attachment_id')->references('id')->on('filemanager__files');
            $table->unsignedInteger('registration_id')->references('id')->on('admin__registrations');
            $table->string('payment_status')->nullable();
            $table->integer('transaction_ref_number')->nullable();
            $table->text('description')->nullable();
            $table->unsignedInteger('account_id')->references('id')->on('accounting__acounts');
            $table->integer('cheque_number')->nullable();
            $table->string('cheque_branch')->nullable();
            $table->date('cheque_date')->nullable();
            $table->string('cheque_bank')->nullable();
            $table->string('reciept_number')->nullable();
            $table->string('record_status')->nullable()->default("A");
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('accounting__expenses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('package_id')->references('id')->on('admin__packages')->nullable();
            $table->unsignedInteger('category_id')->references('id')->on('accounting__expensecategories');
            $table->decimal('amount')->nullable();
            $table->date('date')->nullable();
            $table->string('currency')->nullable();
            $table->unsignedInteger('registration_id')->references('id')->on('admin__registrations');
            $table->string('payment_mode')->nullable();
            $table->unsignedInteger('account_id')->references('id')->on('accounting__accounts');
            $table->unsignedInteger('branch_id')->references('id')->on('accounting__branch');
            $table->string('status')->nullable();
            $table->unsignedInteger('attachment_id')->references('id')->on('filemanager__files')->nullable();
            $table->string('record_status')->nullable()->default("A");
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('accounting__transactions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->decimal('amount')->nullable();
            $table->date('date')->nullable();
            $table->string('transaction_type')->nullable();
            $table->unsignedInteger('account_id')->references('id')->on('accounting__accounts');
            $table->unsignedInteger('registration_id')->references('id')->on('admin__registrations');
            $table->unsignedInteger('package_id')->references('id')->on('admin__packages');
            $table->unsignedInteger('branch_id')->references('id')->on('master__branch');
            $table->integer('credit_account')->nullable();
            $table->integer('debit_account')->nullable();
            $table->string('record_status')->nullable()->default("A");
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting__accounttypes');
        Schema::dropIfExists('accounting__accounts');
        Schema::dropIfExists('accounting__expensecategories');
        Schema::dropIfExists('accounting__invoices');
        Schema::dropIfExists('accounting__payments');
        Schema::dropIfExists('accounting__expenses');
        Schema::dropIfExists('accounting__transactions');
    }
}
