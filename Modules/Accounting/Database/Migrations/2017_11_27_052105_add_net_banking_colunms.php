<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNetBankingColunms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting__payments', function (Blueprint $table) {
            $table->string('reference_code')->nullable();
        });
        Schema::table('accounting__payments', function (Blueprint $table) {
            $table->string('transfer_from')->nullable();
        });
        Schema::table('accounting__payments', function (Blueprint $table) {
            $table->string('transfer_to')->nullable();
        });
        Schema::table('accounting__payments', function (Blueprint $table) {
            $table->string('netTransferTrnsaction_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting__payments', function (Blueprint $table) {
            $table->dropColumn('reference_code')->nullable();
            $table->dropColumn('transfer_from')->nullable();
            $table->dropColumn('transfer_to')->nullable();
            $table->dropColumn('netTransferTrnsaction_id')->nullable();
        });
    }
}
