<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting__expenses', function (Blueprint $table) {
           $table->unsignedInteger('transaction_id')->references('id')->on('accounting__transactions');
        });

        Schema::table('accounting__payments', function (Blueprint $table) {
            $table->unsignedInteger('transaction_id')->references('id')->on('accounting__transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting__expenses', function (Blueprint $table) {
            $table->dropIfExists('transaction_id')->references('id')->on('accounting__transactions');
        });

        Schema::table('accounting__payments', function (Blueprint $table) {
            $table->dropIfExists('transaction_id')->references('id')->on('accounting__transactions');
        });


    }
}
