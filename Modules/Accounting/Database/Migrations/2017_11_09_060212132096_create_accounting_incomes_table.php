<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting__incomes', function (Blueprint $table) {
        
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('income_source_name');
            $table->unsignedInteger('package_id')->references('id')->on('admin__packages')->nullable();
            $table->unsignedInteger('category_id')->references('id')->on('accounting__expensecategories');
            $table->decimal('amount')->nullable();
            $table->date('date')->nullable();
            $table->string('currency')->nullable();
            $table->string('payment_mode')->nullable();
            $table->unsignedInteger('account_id')->references('id')->on('accounting__accounts');
            $table->unsignedInteger('branch_id')->references('id')->on('accounting__branch');
            $table->string('status')->nullable();
            $table->unsignedInteger('attachment_id')->references('id')->on('filemanager__files')->nullable();
            $table->string('record_status')->nullable()->default("A");
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting__incomes');
    }
}
