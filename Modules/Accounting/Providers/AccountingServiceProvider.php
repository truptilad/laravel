<?php

namespace Modules\Accounting\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Accounting\Events\Handlers\RegisterAccountingSidebar;

class AccountingServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterAccountingSidebar::class);
    }

    public function boot()
    {
        $this->publishConfig('accounting', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Accounting\Repositories\AccountTypeRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentAccountTypeRepository(new \Modules\Accounting\Entities\AccountType());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CacheAccountTypeDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Accounting\Repositories\AccountRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentAccountRepository(new \Modules\Accounting\Entities\Account());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CacheAccountDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Accounting\Repositories\ExpenseCategoryRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentExpenseCategoryRepository(new \Modules\Accounting\Entities\ExpenseCategory());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CacheExpenseCategoryDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Accounting\Repositories\ExpenseRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentExpenseRepository(new \Modules\Accounting\Entities\Expense());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CacheExpenseDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Accounting\Repositories\InvoiceRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentInvoiceRepository(new \Modules\Accounting\Entities\Invoice());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CacheInvoiceDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Accounting\Repositories\QuotationRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentQuotationRepository(new \Modules\Accounting\Entities\Quotation());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CacheQuotationDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Accounting\Repositories\PaymentRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentPaymentRepository(new \Modules\Accounting\Entities\Payment());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CachePaymentDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Accounting\Repositories\TransactionRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentTransactionRepository(new \Modules\Accounting\Entities\Transaction());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CacheTransactionDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Accounting\Repositories\CompanyRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentCompanyRepository(new \Modules\Accounting\Entities\Company());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CacheCompanyDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Accounting\Repositories\BranchRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentBranchRepository(new \Modules\Accounting\Entities\Branch());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CacheBranchDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Accounting\Repositories\vendorRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentvendorRepository(new \Modules\Accounting\Entities\vendor());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CachevendorDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Accounting\Repositories\categoryRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentcategoryRepository(new \Modules\Accounting\Entities\category());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CachecategoryDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Accounting\Repositories\categoryTypeRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentcategoryTypeRepository(new \Modules\Accounting\Entities\categoryType());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CachecategoryTypeDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Accounting\Repositories\IncomeRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentIncomeRepository(new \Modules\Accounting\Entities\Income());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CacheIncomeDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Accounting\Repositories\OutstandingRepository',
            function () {
                $repository = new \Modules\Accounting\Repositories\Eloquent\EloquentOutstandingRepository(new \Modules\Accounting\Entities\Outstanding());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Accounting\Repositories\Cache\CacheOutstandingDecorator($repository);
            }
        );
// add bindings







    }
}
