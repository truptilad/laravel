<?php

namespace Modules\Accounting\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface TransactionRepository extends BaseRepository
{
}
