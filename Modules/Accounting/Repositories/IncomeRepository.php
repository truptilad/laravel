<?php

namespace Modules\Accounting\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface IncomeRepository extends BaseRepository
{
}
