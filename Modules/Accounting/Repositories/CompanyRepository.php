<?php

namespace Modules\Accounting\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface CompanyRepository extends BaseRepository
{
}
