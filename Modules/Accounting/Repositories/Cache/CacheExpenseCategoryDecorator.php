<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\ExpenseCategoryRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheExpenseCategoryDecorator extends BaseCacheDecorator implements ExpenseCategoryRepository
{
    public function __construct(ExpenseCategoryRepository $expensecategory)
    {
        parent::__construct();
        $this->entityName = 'accounting.expensecategories';
        $this->repository = $expensecategory;
    }
}
