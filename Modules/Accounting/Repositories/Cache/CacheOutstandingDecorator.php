<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\OutstandingRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheOutstandingDecorator extends BaseCacheDecorator implements OutstandingRepository
{
    public function __construct(OutstandingRepository $outstanding)
    {
        parent::__construct();
        $this->entityName = 'accounting.outstandings';
        $this->repository = $outstanding;
    }
}
