<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\QuotationRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheQuotationDecorator extends BaseCacheDecorator implements QuotationRepository
{
    public function __construct(QuotationRepository $quotation)
    {
        parent::__construct();
        $this->entityName = 'accounting.quotations';
        $this->repository = $quotation;
    }
}
