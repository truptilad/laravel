<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\TransactionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheTransactionDecorator extends BaseCacheDecorator implements TransactionRepository
{
    public function __construct(TransactionRepository $transaction)
    {
        parent::__construct();
        $this->entityName = 'accounting.transactions';
        $this->repository = $transaction;
    }
}
