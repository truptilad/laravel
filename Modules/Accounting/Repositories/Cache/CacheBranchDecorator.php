<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\BranchRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheBranchDecorator extends BaseCacheDecorator implements BranchRepository
{
    public function __construct(BranchRepository $branch)
    {
        parent::__construct();
        $this->entityName = 'accounting.branches';
        $this->repository = $branch;
    }
}
