<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\PaymentRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePaymentDecorator extends BaseCacheDecorator implements PaymentRepository
{
    public function __construct(PaymentRepository $payment)
    {
        parent::__construct();
        $this->entityName = 'accounting.payments';
        $this->repository = $payment;
    }
}
