<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\IncomeRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheIncomeDecorator extends BaseCacheDecorator implements IncomeRepository
{
    public function __construct(IncomeRepository $income)
    {
        parent::__construct();
        $this->entityName = 'accounting.incomes';
        $this->repository = $income;
    }
}
