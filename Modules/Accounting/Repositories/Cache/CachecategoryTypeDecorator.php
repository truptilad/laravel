<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\categoryTypeRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachecategoryTypeDecorator extends BaseCacheDecorator implements categoryTypeRepository
{
    public function __construct(categoryTypeRepository $categorytype)
    {
        parent::__construct();
        $this->entityName = 'accounting.categorytypes';
        $this->repository = $categorytype;
    }
}
