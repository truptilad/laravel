<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\AccountTypeRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheAccountTypeDecorator extends BaseCacheDecorator implements AccountTypeRepository
{
    public function __construct(AccountTypeRepository $accounttype)
    {
        parent::__construct();
        $this->entityName = 'accounting.accounttypes';
        $this->repository = $accounttype;
    }
}
