<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\vendorRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachevendorDecorator extends BaseCacheDecorator implements vendorRepository
{
    public function __construct(vendorRepository $vendor)
    {
        parent::__construct();
        $this->entityName = 'accounting.vendors';
        $this->repository = $vendor;
    }
}
