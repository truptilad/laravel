<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\categoryRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachecategoryDecorator extends BaseCacheDecorator implements categoryRepository
{
    public function __construct(categoryRepository $category)
    {
        parent::__construct();
        $this->entityName = 'accounting.categories';
        $this->repository = $category;
    }
}
