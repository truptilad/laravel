<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\ExpenseRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheExpenseDecorator extends BaseCacheDecorator implements ExpenseRepository
{
    public function __construct(ExpenseRepository $expense)
    {
        parent::__construct();
        $this->entityName = 'accounting.expenses';
        $this->repository = $expense;
    }
}
