<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\CompanyRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheCompanyDecorator extends BaseCacheDecorator implements CompanyRepository
{
    public function __construct(CompanyRepository $company)
    {
        parent::__construct();
        $this->entityName = 'accounting.companies';
        $this->repository = $company;
    }
}
