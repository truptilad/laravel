<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\AccountRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheAccountDecorator extends BaseCacheDecorator implements AccountRepository
{
    public function __construct(AccountRepository $account)
    {
        parent::__construct();
        $this->entityName = 'accounting.accounts';
        $this->repository = $account;
    }
}
