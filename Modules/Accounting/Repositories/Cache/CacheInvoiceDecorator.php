<?php

namespace Modules\Accounting\Repositories\Cache;

use Modules\Accounting\Repositories\InvoiceRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheInvoiceDecorator extends BaseCacheDecorator implements InvoiceRepository
{
    public function __construct(InvoiceRepository $invoice)
    {
        parent::__construct();
        $this->entityName = 'accounting.invoices';
        $this->repository = $invoice;
    }
}
