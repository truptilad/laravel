<?php

namespace Modules\Accounting\Repositories\Eloquent;

use Carbon\Carbon;
use Modules\Accounting\Entities\Invoice;
use Modules\Accounting\Repositories\InvoiceRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Master\Entities\Branch;
use Modules\Master\Entities\Client;
use Modules\Master\Repositories\BranchRepository;
use Modules\Master\Repositories\CompanyRepository;
use Modules\Sales\Entities\Order;
use Modules\User\Repositories\UserRepository;
use Modules\Workflow\Repositories\RequestTypeRepository;

class EloquentInvoiceRepository extends EloquentBaseRepository implements InvoiceRepository
{
    /**
     *  Generates invoice from orders
     * @param $orders
     * @param Client|null $client
     * @param Branch|null $branch
     */
    public function generateOrderInvoice($orders,Client $client = null,Branch $branch = null){

        $requestType = app(RequestTypeRepository::class)->find(INVOICE_REQUEST_TYPE);

        $invoice = new Invoice();

        $invoice->date = Carbon::now();
        $invoice->due_date = Carbon::now()->addDay(7); #TODO Set parameter in company table for default term
        $invoice->status_id = $requestType->initial_request_status_id;
        $invoice->discount  = 0;
        $invoice->amount = 0;
        $invoice->tax = 0;
        $invoice->fine =0;
        $invoice->save();

        $OrderCreatedBy = "";


        foreach ($orders as $order){
            //Add order to invoice only if the invoice is not already generated
            if($order->invoice_no === null){
                $invoice->discount += $order->discount;
                $invoice->tax += $order->tax;
                $invoice->amount += $order->total;
                $invoice->orders()->save($order);
                $invoice->branch_id = $order->branch_id;
                $invoice->client_id = $order->reseller_id;
            }
        }

        if($branch !== null){
            $invoice->branch_id = $branch->id;
            $invoice->company_id = $branch->company_id;

        }
        else{
            $invoice->company_id = app(BranchRepository::class)
                ->find($invoice->branch_id)
                ->company_id;
        }

        if($client !== null){
            $invoice->client_id = $client->id;
        }

        $invoice->invoice_no = Invoice::getNextInvoiceNo($invoice->branch_id);
        $invoice->save();
    }


    /**
     * @param $clientId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getInvoiceForClient($clientId){
        $invoices = $this->allWithBuilder()
            ->where('client_id','=',$clientId)
            ->with('payments')
            ->get();


        foreach ($invoices as $invoice){
            $invoice->paid_amount = array_sum($invoice->payments->pluck('amount')->toArray());
        }

        return  $invoices;

    }




}
