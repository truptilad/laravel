<?php

namespace Modules\Accounting\Repositories\Eloquent;

use Modules\Accounting\Repositories\QuotationRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentQuotationRepository extends EloquentBaseRepository implements QuotationRepository
{
}
