<?php

namespace Modules\Accounting\Repositories\Eloquent;

use Modules\Accounting\Repositories\IncomeRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentIncomeRepository extends EloquentBaseRepository implements IncomeRepository
{
}
