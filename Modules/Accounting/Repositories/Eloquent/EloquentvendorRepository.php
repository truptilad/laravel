<?php

namespace Modules\Accounting\Repositories\Eloquent;

use Modules\Accounting\Repositories\vendorRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentvendorRepository extends EloquentBaseRepository implements vendorRepository
{
}
