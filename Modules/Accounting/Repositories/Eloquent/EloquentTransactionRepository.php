<?php

namespace Modules\Accounting\Repositories\Eloquent;


use Modules\Accounting\Entities\Expense;
use Modules\Accounting\Entities\Payment;
use Modules\Accounting\Repositories\TransactionRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentTransactionRepository extends EloquentBaseRepository implements TransactionRepository
{
    public function createExpenseTransaction($request)
    {
        $data = [
            'account_id' => $request->account_id,
            'branch_id' => BRANCH_ID,
            'transaction_type' => DEBIT_INDICATOR,
            'date' => $request->date,
            'amount' => $request->amount,
            'credit_account' => null,
            'debit_account' => $request->account_id,
            'package_id' => $request->package_id
        ];

        $transaction = $this->create($data);

        return $transaction;
    }


    public function updateExpenseTransaction($transaction,$request)
    {
        $updatetransaction =app(TransactionRepository::class)->update($transaction,
            [
                'account_id' => $request->account_id,
                'branch_id' => BRANCH_ID,
                'transaction_type' => DEBIT_INDICATOR,
                'date' => $request->date,
                'amount' => $request->amount,
                'credit_account' => null,
                'debit_account' => $request->account_id,
                'package_id' => $request->package_id
            ]);

        return $updatetransaction;
    }

    public function createPaymentTransaction($request)
    {
        $data = [
            'account_id' => $request['account_id'],
            'branch_id' => BRANCH_ID,
            'transaction_type' => CREDIT_INDICATOR,
            'date' => $request['date'],
            'amount' => $request['amount'],
            'credit_account' => $request['account_id'],
            'debit_account' => null,
            'registration_id' => $request['registration_id']
        ];
        $transaction = $this->create($data);
        return $transaction;
    }

    public function updatePaymentTransaction($transaction,$transfer)
    {
        $transfer =app(TransactionRepository::class)->update($transaction,
            [
                'account_id' => $transfer->account_id,
                'branch_id' => BRANCH_ID,
                'transaction_type' => CREDIT_INDICATOR,
                'date' => $transfer->date,
                'amount' => $transfer->amount,
                'credit_account' => $transfer->account_id,
                'debit_account' => null,
                'registration_id' => $transfer->registration_id
            ]);
        return $transfer;
    }

    public function createIncomeTransaction($request)
    {
        $data = [
            'account_id' => $request->account_id,
            'branch_id' => BRANCH_ID,
            'transaction_type' => CREDIT_BY_INCOME,
            'date' => $request->date,
            'amount' => $request->amount,
            'credit_account' => $request->account_id,
            'debit_account' => null,
            'package_id' => $request->package_id
        ];
        $transaction = $this->create($data);
        return $transaction;
    }

    public function updateIncomeTransaction($transaction,$transfer)
    {
        $data = [
                'account_id' => $transfer->account_id,
                'branch_id' => BRANCH_ID,
                'transaction_type' => CREDIT_BY_INCOME,
                'date' => $transfer->date,
                'amount' => $transfer->amount,
                'credit_account' => $transfer->account_id,
                'debit_account' => null,
                'package_id' => $transfer->package_id
            ];
        return $transfer;
    }

}
