<?php

namespace Modules\Accounting\Repositories\Eloquent;

use Modules\Accounting\Repositories\BranchRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentBranchRepository extends EloquentBaseRepository implements BranchRepository
{
}
