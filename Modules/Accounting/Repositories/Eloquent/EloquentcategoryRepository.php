<?php

namespace Modules\Accounting\Repositories\Eloquent;

use Modules\Accounting\Repositories\categoryRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentcategoryRepository extends EloquentBaseRepository implements categoryRepository
{
}
