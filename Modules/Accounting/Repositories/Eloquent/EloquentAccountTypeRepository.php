<?php

namespace Modules\Accounting\Repositories\Eloquent;

use Modules\Accounting\Repositories\AccountTypeRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentAccountTypeRepository extends EloquentBaseRepository implements AccountTypeRepository
{
}
