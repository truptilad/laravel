<?php

namespace Modules\Accounting\Repositories\Eloquent;

use Modules\Accounting\Repositories\OutstandingRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentOutstandingRepository extends EloquentBaseRepository implements OutstandingRepository
{
}
