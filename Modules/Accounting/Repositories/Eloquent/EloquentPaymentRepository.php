<?php

namespace Modules\Accounting\Repositories\Eloquent;

use Modules\Accounting\Repositories\PaymentRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPaymentRepository extends EloquentBaseRepository implements PaymentRepository
{
}
