<?php

namespace Modules\Accounting\Repositories\Eloquent;

use Modules\Accounting\Repositories\ExpenseCategoryRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentExpenseCategoryRepository extends EloquentBaseRepository implements ExpenseCategoryRepository
{
}
