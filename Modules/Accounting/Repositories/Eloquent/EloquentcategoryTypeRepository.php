<?php

namespace Modules\Accounting\Repositories\Eloquent;

use Modules\Accounting\Repositories\categoryTypeRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentcategoryTypeRepository extends EloquentBaseRepository implements categoryTypeRepository
{
}
