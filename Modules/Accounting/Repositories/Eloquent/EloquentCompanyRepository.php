<?php

namespace Modules\Accounting\Repositories\Eloquent;

use Modules\Accounting\Repositories\CompanyRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentCompanyRepository extends EloquentBaseRepository implements CompanyRepository
{
}
