<?php

namespace Modules\Accounting\Repositories\Eloquent;

use Modules\Accounting\Repositories\AccountRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentAccountRepository extends EloquentBaseRepository implements AccountRepository
{
}
