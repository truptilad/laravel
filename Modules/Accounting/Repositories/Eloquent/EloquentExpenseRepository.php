<?php

namespace Modules\Accounting\Repositories\Eloquent;

use Modules\Accounting\Repositories\ExpenseRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentExpenseRepository extends EloquentBaseRepository implements ExpenseRepository
{
}
