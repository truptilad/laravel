<?php

namespace Modules\Accounting\Entities;

use Carbon\Carbon;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Admin\Entities\Registration;
use Modules\Admin\Entities\Package;
use Modules\Accounting\Entities\Account;
use Modules\Filemanager\Entities\File;

class Payment extends Model
{
    use SoftDeletes;

    protected $table = 'accounting__payments';
    protected $dates = ['deleted_at'];
    protected  $fillable = [
        'package_id',
        'payment_status',
        'registration_id',
        'account_id',
        'attachment_id',
        'mode',
        'date',
        'amount',
        'payment_status',
        'description',
        'cheque_number',
        'cheque_branch',
        'cheque_bank',
        'cheque_date',
        'transaction_id',
        'reference_code',
        'transfer_from',
        'transfer_to',
        'netTransferTrnsaction_id',
    ];

    /**
     * @param $value
     */
    public function setdateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value);

    }

    /**
     * @param $value
     */
    public function setchequedateAttribute($value)
    {
        $this->attributes['cheque_date'] = Carbon::parse($value);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function registration()
    {
        return $this->belongsTo(registration::class,'registration_id','id');
    }

    public function packageDetails()
    {
        return $this->belongsTo(Package::class,'package_id','id');
    }

    public function account()
    {
        return $this->belongsTo(Account::class,'account_id','id');
    }

    public function file()
    {
        return $this->belongsTo(File::class,'attachment_id','id');

    }
}
