<?php

namespace Modules\Accounting\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Outstanding extends Model
{
    use Translatable;

    protected $table = 'accounting__outstandings';
    public $translatedAttributes = [];
    protected $fillable = [];
}
