<?php

namespace Modules\Accounting\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
  	use SoftDeletes; 

    protected $table = 'accounting__branches';
    protected $fillable = ['id','name','branch_id','description','location','city'];
}
