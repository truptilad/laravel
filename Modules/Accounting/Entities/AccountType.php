<?php

namespace Modules\Accounting\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountType extends Model
{
    use SoftDeletes;

    protected $table = 'accounting__accounttypes';
    protected $dates = ['deleted_at'];
    protected $fillable = [
      'type'
    ];

}
