<?php

namespace Modules\Accounting\Entities;

use Carbon\Carbon;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    protected $table = 'accounting__transactions';
    public $translatedAttributes = [];
    protected $fillable = [
        'account_id',
        'registration_id',
        'branch_id',
        'transaction_type',
        'date',
        'amount',
        'credit_account',
        'debit_account',
        'expense_id',
        'payment_id',
        'created_by',
        'package_id'

    ];


    public function setdateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value);
    }
}
