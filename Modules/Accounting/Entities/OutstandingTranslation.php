<?php

namespace Modules\Accounting\Entities;

use Illuminate\Database\Eloquent\Model;

class OutstandingTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'accounting__outstanding_translations';
}
