<?php

namespace Modules\Accounting\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Accounting\Repositories\ExpenseCategoryRepository;
use Modules\Admin\Entities\Package;

class Income extends Model
{
    use softDeletes;

    protected $table = 'accounting__incomes';
    protected $fillable = [
    'id',
    'income_source_name',
    'category_id',
    'package_id',
		'amount',
		'date',
		'currency',
		'payment_mode',
		'account_id',
		'branch_id',
		'status',
		'attachment_id',
		'transaction_id',
    'cheque_bank',
    'cheque_branch',
    'cheque_date',            
    'transfer_to',
    'reference_code',
    'transfer_from',
    'cheque_number'

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function packageDetail()
    {
      return  $this->belongsTo(package::class,'package_id','id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
      return  $this->belongsTo(ExpenseCategory::class,'category_id','id');
    }
}
