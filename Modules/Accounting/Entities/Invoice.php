<?php

namespace Modules\Accounting\Entities;

use Carbon\Carbon;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Master\Entities\Branch;
use Modules\Master\Entities\Client;
use Modules\Master\Entities\Company;
use Modules\Master\Repositories\BranchRepository;
use Modules\Sales\Entities\Order;
use Modules\Workflow\Entities\WorkflowStatus;

class Invoice extends Model
{
    use SoftDeletes;

    protected $table = 'accounting__invoices';
    public $translatedAttributes = [];
    protected $fillable = [
        'date',
        'discount',
        'amount',
        'tax',
        'due_date',
        'fine',
        'payment_status',
        'status_id',
        'comment',
        'invoice_no',
        'file_id',
        'client_id',
        'dispatch_number',
        'dispatch_through',
        'payment_terms'
    ];

    /**
     * Gets next available order no
     * @param $branchId
     * @return string
     */
    public static function getNextInvoiceNo($branchId){
        $branch = app(BranchRepository::class)->find($branchId);

        if($branch->invoice_number_counter == null){
            $branch->invoice_number_counter = 1;
        }
        else{
            $branch->invoice_number_counter += 1;
        }

        $branch->save();

        $orderPrefix = "";

        if($branch->invoice_number_prefix == null){
            $orderPrefix = $branch->code;
        }
        else{
            $orderPrefix = $branch->$branch->invoice_number_prefix;
        }

        return $orderPrefix .'-'.date('y').'-'.$branch->invoice_number_counter; #TODO put invoice date format parameter on company table
    }

    /**
     * Orders in invoice
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders(){
        return $this->hasMany(Order::class,'invoice_id','id');
    }

    public function status(){
        return $this->belongsTo(WorkflowStatus::class,'status_id','id');
    }


    /**
     * @param $value
     */
    public function setdateAttribute($value){
        $this->attributes['date'] = Carbon::parse($value);
    }


    /**
     * @param $value
     */
    public function setduedateAttribute($value){
        $this->attributes['due_date'] = Carbon::parse($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch(){
        return $this->belongsTo(Branch::class,'branch_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company(){
        return $this->belongsTo(Company::class,'company_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function client() {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }

    /**
     * @return mixed
     */
    public function payments(){
        return $this->hasMany(Payment::class,'invoice_id','id');
    }



}
