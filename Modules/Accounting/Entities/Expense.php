<?php

namespace Modules\Accounting\Entities;

use Carbon\Carbon;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Accounting\Repositories\AccountRepository;
use Modules\Accounting\Repositories\ExpenseCategoryRepository;
use Modules\Admin\Entities\Package;

class Expense extends Model
{
    use SoftDeletes;

    protected $table = 'accounting__expenses';
    
    protected $fillable = [
        'name',
        'type',
        'package_id',
        'account_id',
        'cheque_number',
        'status',
        'branch_id',
        'category_id',
        'subcategory_id',
        'date',
        'payment_mode',
        'amount',
        'attachment_id',
        'transaction_id',
        'reference_code',
        'transfer_from',
        'transfer_to',
        'cheque_bank',
        'cheque_branch',
        'cheque_date',
        'netTransferTrnsaction_id',
    ];

    /**
     * @param $value
     */
    public function setdateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function packageDetail()
    {
      return  $this->belongsTo(package::class,'package_id','id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
      return  $this->belongsTo(ExpenseCategory::class,'category_id','id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function expensecategory()
    {
        return $this->belongsTo(ExpenseCategory::class,'subcategory_id','id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class,'account_id','id');
    }
}
