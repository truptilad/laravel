<?php

namespace Modules\Accounting\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpenseCategory extends Model
{
    use SoftDeletes;

    protected $table = 'accounting__expensecategories';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'type_id',
        'description'
    ];

    public function childexpenseCategory()
    {
        return $this->hasMany(ExpenseCategory::class,'type_id','id');
    }
    
    
    public function parentexpenseCategory()
    {
        return $this->hasMany(ExpenseCategory::class,'type_id','id');
    }

    public function expenseCategorytype()
    {
        return $this->belongsTo(ExpenseCategory::class,'type_id','id');
    }
}
