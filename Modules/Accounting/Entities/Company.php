<?php

namespace Modules\Accounting\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
  	use SoftDeletes; 

    protected $table = 'accounting__companies';
    protected $fillable = ['id','name'];
}
