<?php

namespace Modules\Accounting\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Accounting\Entities\Branch;
use Modules\Accounting\Entities\Company;

class Account extends Model
{
    use SoftDeletes;

    protected $table = 'accounting__accounts';
    public $translatedAttributes = [];
    protected $fillable = [
        'name',
        'description',
        'balance',
        'user_id',
        'company_id',
        'branch_id',
        'account_type_id',
        'acc_number',
        'ifsc_code',
        'bank_name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class,'company_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class,'branch_id','id');
    }
}
