<?php

namespace Modules\Accounting\Entities;

use Illuminate\Database\Eloquent\Model;

class IncomeTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'accounting__income_translations';
}
