<?php

namespace Modules\Accounting\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    use Translatable;

    protected $table = 'accounting__quotations';
    public $translatedAttributes = [];
    protected $fillable = [];
}
