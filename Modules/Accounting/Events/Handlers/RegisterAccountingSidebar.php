<?php

namespace Modules\Accounting\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterAccountingSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {

            $group->item(trans('accounting::accounts.title.accounts'), function (Item $item) {
                    $item->icon('fa fa-user ');
                    $item->weight(3);
                    $item->append('admin.accounting.account.index');
                    $item->route('admin.accounting.account.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.accounts.index')
                    );
                });
            $group->item(trans('accounting::expenses.title.expenses'), function (Item $item) {
                    $item->icon('fa fa-money');
                    $item->weight(4);
                    $item->append('admin.accounting.expense.create');
                    $item->route('admin.accounting.expense.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.expenses.index')
                    );
                });
            $group->item(trans('accounting::payments.title.payments'), function (Item $item) {
                    $item->icon('fa fa-credit-card');
                    $item->weight(5);
                    $item->append('admin.accounting.payment.create');
                    $item->route('admin.accounting.payment.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.payments.index')
                    );
                });
            $group->item(trans('accounting::outstandings.title.outstandings'), function (Item $item) {
                    $item->icon('fa fa-circle-o');
                    $item->weight(6);
                    $item->append('admin.accounting.outstanding.index');
                    $item->route('admin.accounting.outstanding.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.outstandings.index')
                    );
                });
            $group->item(trans('accounting::incomes.title.incomes'), function (Item $item) {
                    $item->icon('fa fa-inr');
                    $item->weight(7);
                    $item->append('admin.accounting.income.create');
                    $item->route('admin.accounting.income.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.incomes.index')
                    );
                });
            $group->item(trans('accounting::transactions.title.transactions'), function (Item $item) {
                    $item->icon('fa fa-random');
                    $item->weight(8);
                    $item->append('admin.accounting.transaction.create');
                    $item->route('admin.accounting.transaction.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.transactions.index')
                    );
                });


            $group->item(trans('accounting::accountings.title.name'), function (Item $item) {
                $item->icon('fa fa-cog');
                $item->weight(10);
                $item->authorize(
                    $this->auth->hasAccess('accounting.module.index')
                );
                $item->item(trans('accounting::accounttypes.title.accounttypes'), function (Item $item) {
                    $item->icon('fa fa-tags');
                    $item->weight(0);
                    $item->append('admin.accounting.accounttype.create');
                    $item->route('admin.accounting.accounttype.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.accounttypes.index')
                    );
                });
                
                $item->item(trans('accounting::expensecategories.title.expensecategories'), function (Item $item) {
                    $item->icon('fa fa-list-ul');
                    $item->weight(0);
                    $item->append('admin.accounting.expensecategory.create');
                    $item->route('admin.accounting.expensecategory.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.expensecategories.index')
                    );
                });
                
                $item->item(trans('accounting::invoices.title.invoices'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.accounting.invoice.create');
                    $item->route('admin.accounting.invoice.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.invoices.index')
                    );
                });
                $item->item(trans('accounting::quotations.title.quotations'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.accounting.quotation.create');
                    $item->route('admin.accounting.quotation.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.quotations.index')
                    );
                });
                $item->item(trans('accounting::companies.title.companies'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.accounting.company.create');
                    $item->route('admin.accounting.company.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.companies.index')
                    );
                });
                $item->item(trans('accounting::branches.title.branches'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.accounting.branch.create');
                    $item->route('admin.accounting.branch.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.branches.index')
                    );
                });
                $item->item(trans('accounting::vendors.title.vendors'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.accounting.vendor.create');
                    $item->route('admin.accounting.vendor.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.vendors.index')
                    );
                });
                $item->item(trans('accounting::categories.title.categories'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.accounting.category.create');
                    $item->route('admin.accounting.category.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.categories.index')
                    );
                });
                $item->item(trans('accounting::categorytypes.title.categorytypes'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.accounting.categorytype.create');
                    $item->route('admin.accounting.categorytype.index');
                    $item->authorize(
                        $this->auth->hasAccess('accounting.categorytypes.index')
                    );
                });               
// append

            });
        });

        return $menu;
    }
}
