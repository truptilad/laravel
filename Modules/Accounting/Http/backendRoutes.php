<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/accounting'], function (Router $router) {
    $router->bind('accounttype', function ($id) {
        return app('Modules\Accounting\Repositories\AccountTypeRepository')->find($id);
    });
    $router->get('accounttypes', [
        'as' => 'admin.accounting.accounttype.index',
        'uses' => 'AccountTypeController@index',
        'middleware' => 'can:accounting.accounttypes.index'
    ]);
    $router->get('accounttypes/create', [
        'as' => 'admin.accounting.accounttype.create',
        'uses' => 'AccountTypeController@create',
        'middleware' => 'can:accounting.accounttypes.create'
    ]);
    $router->post('accounttypes', [
        'as' => 'admin.accounting.accounttype.store',
        'uses' => 'AccountTypeController@store',
        'middleware' => 'can:accounting.accounttypes.create'
    ]);
    $router->get('accounttypes/{accounttype}/edit', [
        'as' => 'admin.accounting.accounttype.edit',
        'uses' => 'AccountTypeController@edit',
        'middleware' => 'can:accounting.accounttypes.edit'
    ]);
    $router->put('accounttypes/{accounttype}', [
        'as' => 'admin.accounting.accounttype.update',
        'uses' => 'AccountTypeController@update',
        'middleware' => 'can:accounting.accounttypes.edit'
    ]);
    $router->post('accounttypes/update', [
        'as' => 'admin.accounting.accounttype.update',
        'uses' => 'AccountTypeController@update',
        'middleware' => 'can:accounting.accounttypes.edit'
    ]);
    $router->delete('accounttypes/{accounttype}', [
        'as' => 'admin.accounting.accounttype.destroy',
        'uses' => 'AccountTypeController@destroy',
        'middleware' => 'can:accounting.accounttypes.destroy'
    ]);
    $router->bind('account', function ($id) {
        return app('Modules\Accounting\Repositories\AccountRepository')->find($id);
    });
    $router->get('accounts', [
        'as' => 'admin.accounting.account.index',
        'uses' => 'AccountController@index',
        'middleware' => 'can:accounting.accounts.index'
    ]);
    $router->get('accounts/create', [
        'as' => 'admin.accounting.account.create',
        'uses' => 'AccountController@create',
        'middleware' => 'can:accounting.accounts.create'
    ]);
    $router->post('accounts', [
        'as' => 'admin.accounting.account.store',
        'uses' => 'AccountController@store',
        'middleware' => 'can:accounting.accounts.create'
    ]);
    $router->get('accounts/{account}/edit', [
        'as' => 'admin.accounting.account.edit',
        'uses' => 'AccountController@edit',
        'middleware' => 'can:accounting.accounts.edit'
    ]);
    $router->put('accounts/{account}', [
        'as' => 'admin.accounting.account.update',
        'uses' => 'AccountController@update',
        'middleware' => 'can:accounting.accounts.edit'
    ]);
    $router->post('accounts/update', [
        'as' => 'admin.accounting.account.update',
        'uses' => 'AccountController@update',
        'middleware' => 'can:accounting.accounts.edit'
    ]);
    $router->get('accounts/getAccount', [
        'as' => 'admin.accounting.account.getAccount',
        'uses' => 'AccountController@getAccount',
        'middleware' => 'can:accounting.accounts.edit'
    ]);
    $router->delete('accounts/{account}', [
        'as' => 'admin.accounting.account.destroy',
        'uses' => 'AccountController@destroy',
        'middleware' => 'can:accounting.accounts.destroy'
    ]);
    $router->bind('expensecategory', function ($id) {
        return app('Modules\Accounting\Repositories\ExpenseCategoryRepository')->find($id);
    });
    $router->get('expensecategories', [
        'as' => 'admin.accounting.expensecategory.index',
        'uses' => 'ExpenseCategoryController@index',
        'middleware' => 'can:accounting.expensecategories.index'
    ]);
    $router->get('expensecategories/create', [
        'as' => 'admin.accounting.expensecategory.create',
        'uses' => 'ExpenseCategoryController@create',
        'middleware' => 'can:accounting.expensecategories.create'
    ]);
    $router->post('expensecategories', [
        'as' => 'admin.accounting.expensecategory.store',
        'uses' => 'ExpenseCategoryController@store',
        'middleware' => 'can:accounting.expensecategories.create'
    ]);
    $router->get('expensecategories/{expensecategory}/edit', [
        'as' => 'admin.accounting.expensecategory.edit',
        'uses' => 'ExpenseCategoryController@edit',
        'middleware' => 'can:accounting.expensecategories.edit'
    ]);
    $router->put('expensecategories/{expensecategory}', [
        'as' => 'admin.accounting.expensecategory.update',
        'uses' => 'ExpenseCategoryController@update',
        'middleware' => 'can:accounting.expensecategories.edit'
    ]);
    $router->post('expensecategories/update', [
        'as' => 'admin.accounting.expensecategory.update',
        'uses' => 'ExpenseCategoryController@update',
        'middleware' => 'can:accounting.expensecategories.edit'
    ]);
    $router->get('expensecategories/subcategory', [
        'as' => 'admin.accounting.expensecategory.subcategory',
        'uses' => 'ExpenseCategoryController@subcategory',
        'middleware' => 'can:accounting.expensecategories.edit'
    ]);
    $router->delete('expensecategories/{expensecategory}', [
        'as' => 'admin.accounting.expensecategory.destroy',
        'uses' => 'ExpenseCategoryController@destroy',
        'middleware' => 'can:accounting.expensecategories.destroy'
    ]);
    $router->bind('expense', function ($id) {
        return app('Modules\Accounting\Repositories\ExpenseRepository')->find($id);
    });
    $router->get('expenses', [
        'as' => 'admin.accounting.expense.index',
        'uses' => 'ExpenseController@index',
        'middleware' => 'can:accounting.expenses.index'
    ]);
    $router->get('expenses/create', [
        'as' => 'admin.accounting.expense.create',
        'uses' => 'ExpenseController@create',
        'middleware' => 'can:accounting.expenses.create'
    ]);
    $router->post('expenses', [
        'as' => 'admin.accounting.expense.store',
        'uses' => 'ExpenseController@store',
        'middleware' => 'can:accounting.expenses.create'
    ]);
    $router->get('expenses/{expense}/edit', [
        'as' => 'admin.accounting.expense.edit',
        'uses' => 'ExpenseController@edit',
        'middleware' => 'can:accounting.expenses.edit'
    ]);
    $router->put('expenses/{expense}', [
        'as' => 'admin.accounting.expense.update',
        'uses' => 'ExpenseController@update',
        'middleware' => 'can:accounting.expenses.edit'
    ]);
    $router->delete('expenses/{expense}', [
        'as' => 'admin.accounting.expense.destroy',
        'uses' => 'ExpenseController@destroy',
        'middleware' => 'can:accounting.expenses.destroy'
    ]);
    $router->bind('invoice', function ($id) {
        return app('Modules\Accounting\Repositories\InvoiceRepository')->find($id);
    });
    $router->get('invoices', [
        'as' => 'admin.accounting.invoice.index',
        'uses' => 'InvoiceController@index',
        'middleware' => 'can:accounting.invoices.index'
    ]);
    $router->get('invoices/create', [
        'as' => 'admin.accounting.invoice.create',
        'uses' => 'InvoiceController@create',
        'middleware' => 'can:accounting.invoices.create'
    ]);
    $router->post('invoices', [
        'as' => 'admin.accounting.invoice.store',
        'uses' => 'InvoiceController@store',
        'middleware' => 'can:accounting.invoices.create'
    ]);
    $router->get('invoices/{invoice}/edit', [
        'as' => 'admin.accounting.invoice.edit',
        'uses' => 'InvoiceController@edit',
        'middleware' => 'can:accounting.invoices.edit'
    ]);
    $router->put('invoices/{invoice}', [
        'as' => 'admin.accounting.invoice.update',
        'uses' => 'InvoiceController@update',
        'middleware' => 'can:accounting.invoices.edit'
    ]);
    $router->post('invoices/getInvoice', [
        'as' => 'admin.accounting.invoice.getInvoice',
        'uses' => 'InvoiceController@getInvoice',
        'middleware' => 'can:accounting.invoices.edit'
    ]);
    $router->delete('invoices/{invoice}', [
        'as' => 'admin.accounting.invoice.destroy',
        'uses' => 'InvoiceController@destroy',
        'middleware' => 'can:accounting.invoices.destroy'
    ]);
        $router->get('invoices/{invoice}', [
        'as' => 'admin.accounting.invoice.view',
        'uses' => 'InvoiceController@view',
        'middleware' => 'can:accounting.invoices.index'
    ]);
    $router->bind('quotation', function ($id) {
        return app('Modules\Accounting\Repositories\QuotationRepository')->find($id);
    });
    $router->get('quotations', [
        'as' => 'admin.accounting.quotation.index',
        'uses' => 'QuotationController@index',
        'middleware' => 'can:accounting.quotations.index'
    ]);
    $router->get('quotations/create', [
        'as' => 'admin.accounting.quotation.create',
        'uses' => 'QuotationController@create',
        'middleware' => 'can:accounting.quotations.create'
    ]);
    $router->post('quotations', [
        'as' => 'admin.accounting.quotation.store',
        'uses' => 'QuotationController@store',
        'middleware' => 'can:accounting.quotations.create'
    ]);
    $router->get('quotations/{quotation}/edit', [
        'as' => 'admin.accounting.quotation.edit',
        'uses' => 'QuotationController@edit',
        'middleware' => 'can:accounting.quotations.edit'
    ]);
    $router->put('quotations/{quotation}', [
        'as' => 'admin.accounting.quotation.update',
        'uses' => 'QuotationController@update',
        'middleware' => 'can:accounting.quotations.edit'
    ]);
    $router->delete('quotations/{quotation}', [
        'as' => 'admin.accounting.quotation.destroy',
        'uses' => 'QuotationController@destroy',
        'middleware' => 'can:accounting.quotations.destroy'
    ]);
    $router->bind('payment', function ($id) {
        return app('Modules\Accounting\Repositories\PaymentRepository')->find($id);
    });
    $router->get('payments', [
        'as' => 'admin.accounting.payment.index',
        'uses' => 'PaymentController@index',
        'middleware' => 'can:accounting.payments.index'
    ]);
    $router->get('payments/create', [
        'as' => 'admin.accounting.payment.create',
        'uses' => 'PaymentController@create',
        'middleware' => 'can:accounting.payments.create'
    ]);
    $router->post('payments', [
        'as' => 'admin.accounting.payment.store',
        'uses' => 'PaymentController@store',
        'middleware' => 'can:accounting.payments.create'
    ]);
    $router->get('payments/{payment}/edit', [
        'as' => 'admin.accounting.payment.edit',
        'uses' => 'PaymentController@edit',
        'middleware' => 'can:accounting.payments.edit'
    ]);
    $router->put('payments/{payment}', [
        'as' => 'admin.accounting.payment.update',
        'uses' => 'PaymentController@update',
        'middleware' => 'can:accounting.payments.edit'
    ]);
    $router->delete('payments/{payment}', [
        'as' => 'admin.accounting.payment.destroy',
        'uses' => 'PaymentController@destroy',
        'middleware' => 'can:accounting.payments.destroy'
    ]);
    $router->bind('transaction', function ($id) {
        return app('Modules\Accounting\Repositories\TransactionRepository')->find($id);
    });
    $router->get('transactions', [
        'as' => 'admin.accounting.transaction.index',
        'uses' => 'TransactionController@index',
        'middleware' => 'can:accounting.transactions.index'
    ]);
    $router->get('transactions/create', [
        'as' => 'admin.accounting.transaction.create',
        'uses' => 'TransactionController@create',
        'middleware' => 'can:accounting.transactions.create'
    ]);
    $router->post('transactions', [
        'as' => 'admin.accounting.transaction.store',
        'uses' => 'TransactionController@store',
        'middleware' => 'can:accounting.transactions.create'
    ]);
    $router->get('transactions/{transaction}/edit', [
        'as' => 'admin.accounting.transaction.edit',
        'uses' => 'TransactionController@edit',
        'middleware' => 'can:accounting.transactions.edit'
    ]);
    $router->put('transactions/{transaction}', [
        'as' => 'admin.accounting.transaction.update',
        'uses' => 'TransactionController@update',
        'middleware' => 'can:accounting.transactions.edit'
    ]);
    $router->delete('transactions/{transaction}', [
        'as' => 'admin.accounting.transaction.destroy',
        'uses' => 'TransactionController@destroy',
        'middleware' => 'can:accounting.transactions.destroy'
    ]);
    $router->bind('company', function ($id) {
        return app('Modules\Accounting\Repositories\CompanyRepository')->find($id);
    });
    $router->get('companies', [
        'as' => 'admin.accounting.company.index',
        'uses' => 'CompanyController@index',
        'middleware' => 'can:accounting.companies.index'
    ]);
    $router->get('companies/create', [
        'as' => 'admin.accounting.company.create',
        'uses' => 'CompanyController@create',
        'middleware' => 'can:accounting.companies.create'
    ]);
    $router->post('companies', [
        'as' => 'admin.accounting.company.store',
        'uses' => 'CompanyController@store',
        'middleware' => 'can:accounting.companies.create'
    ]);
    $router->get('companies/{company}/edit', [
        'as' => 'admin.accounting.company.edit',
        'uses' => 'CompanyController@edit',
        'middleware' => 'can:accounting.companies.edit'
    ]);
    $router->put('companies/{company}', [
        'as' => 'admin.accounting.company.update',
        'uses' => 'CompanyController@update',
        'middleware' => 'can:accounting.companies.edit'
    ]);
    $router->delete('companies/{company}', [
        'as' => 'admin.accounting.company.destroy',
        'uses' => 'CompanyController@destroy',
        'middleware' => 'can:accounting.companies.destroy'
    ]);
    $router->bind('branch', function ($id) {
        return app('Modules\Accounting\Repositories\BranchRepository')->find($id);
    });
    $router->get('branches', [
        'as' => 'admin.accounting.branch.index',
        'uses' => 'BranchController@index',
        'middleware' => 'can:accounting.branches.index'
    ]);
    $router->get('branches/create', [
        'as' => 'admin.accounting.branch.create',
        'uses' => 'BranchController@create',
        'middleware' => 'can:accounting.branches.create'
    ]);
    $router->post('branches', [
        'as' => 'admin.accounting.branch.store',
        'uses' => 'BranchController@store',
        'middleware' => 'can:accounting.branches.create'
    ]);
    $router->get('branches/{branch}/edit', [
        'as' => 'admin.accounting.branch.edit',
        'uses' => 'BranchController@edit',
        'middleware' => 'can:accounting.branches.edit'
    ]);
    $router->put('branches/{branch}', [
        'as' => 'admin.accounting.branch.update',
        'uses' => 'BranchController@update',
        'middleware' => 'can:accounting.branches.edit'
    ]);
    $router->delete('branches/{branch}', [
        'as' => 'admin.accounting.branch.destroy',
        'uses' => 'BranchController@destroy',
        'middleware' => 'can:accounting.branches.destroy'
    ]);
    $router->bind('vendor', function ($id) {
        return app('Modules\Accounting\Repositories\vendorRepository')->find($id);
    });
    $router->get('vendors', [
        'as' => 'admin.accounting.vendor.index',
        'uses' => 'vendorController@index',
        'middleware' => 'can:accounting.vendors.index'
    ]);
    $router->get('vendors/create', [
        'as' => 'admin.accounting.vendor.create',
        'uses' => 'vendorController@create',
        'middleware' => 'can:accounting.vendors.create'
    ]);
    $router->post('vendors', [
        'as' => 'admin.accounting.vendor.store',
        'uses' => 'vendorController@store',
        'middleware' => 'can:accounting.vendors.create'
    ]);
    $router->get('vendors/{vendor}/edit', [
        'as' => 'admin.accounting.vendor.edit',
        'uses' => 'vendorController@edit',
        'middleware' => 'can:accounting.vendors.edit'
    ]);
    $router->put('vendors/{vendor}', [
        'as' => 'admin.accounting.vendor.update',
        'uses' => 'vendorController@update',
        'middleware' => 'can:accounting.vendors.edit'
    ]);
    $router->delete('vendors/{vendor}', [
        'as' => 'admin.accounting.vendor.destroy',
        'uses' => 'vendorController@destroy',
        'middleware' => 'can:accounting.vendors.destroy'
    ]);
    $router->bind('category', function ($id) {
        return app('Modules\Accounting\Repositories\categoryRepository')->find($id);
    });
    $router->get('categories', [
        'as' => 'admin.accounting.category.index',
        'uses' => 'categoryController@index',
        'middleware' => 'can:accounting.categories.index'
    ]);
    $router->get('categories/create', [
        'as' => 'admin.accounting.category.create',
        'uses' => 'categoryController@create',
        'middleware' => 'can:accounting.categories.create'
    ]);
    $router->post('categories', [
        'as' => 'admin.accounting.category.store',
        'uses' => 'categoryController@store',
        'middleware' => 'can:accounting.categories.create'
    ]);
    $router->get('categories/{category}/edit', [
        'as' => 'admin.accounting.category.edit',
        'uses' => 'categoryController@edit',
        'middleware' => 'can:accounting.categories.edit'
    ]);
    $router->put('categories/{category}', [
        'as' => 'admin.accounting.category.update',
        'uses' => 'categoryController@update',
        'middleware' => 'can:accounting.categories.edit'
    ]);
    $router->delete('categories/{category}', [
        'as' => 'admin.accounting.category.destroy',
        'uses' => 'categoryController@destroy',
        'middleware' => 'can:accounting.categories.destroy'
    ]);
    $router->bind('categorytype', function ($id) {
        return app('Modules\Accounting\Repositories\categoryTypeRepository')->find($id);
    });
    $router->get('categorytypes', [
        'as' => 'admin.accounting.categorytype.index',
        'uses' => 'categoryTypeController@index',
        'middleware' => 'can:accounting.categorytypes.index'
    ]);
    $router->get('categorytypes/create', [
        'as' => 'admin.accounting.categorytype.create',
        'uses' => 'categoryTypeController@create',
        'middleware' => 'can:accounting.categorytypes.create'
    ]);
    $router->post('categorytypes', [
        'as' => 'admin.accounting.categorytype.store',
        'uses' => 'categoryTypeController@store',
        'middleware' => 'can:accounting.categorytypes.create'
    ]);
    $router->get('categorytypes/{categorytype}/edit', [
        'as' => 'admin.accounting.categorytype.edit',
        'uses' => 'categoryTypeController@edit',
        'middleware' => 'can:accounting.categorytypes.edit'
    ]);
    $router->put('categorytypes/{categorytype}', [
        'as' => 'admin.accounting.categorytype.update',
        'uses' => 'categoryTypeController@update',
        'middleware' => 'can:accounting.categorytypes.edit'
    ]);
    $router->delete('categorytypes/{categorytype}', [
        'as' => 'admin.accounting.categorytype.destroy',
        'uses' => 'categoryTypeController@destroy',
        'middleware' => 'can:accounting.categorytypes.destroy'
    ]);
    $router->bind('income', function ($id) {
        return app('Modules\Accounting\Repositories\IncomeRepository')->find($id);
    });
    $router->get('incomes', [
        'as' => 'admin.accounting.income.index',
        'uses' => 'IncomeController@index',
        'middleware' => 'can:accounting.incomes.index'
    ]);
    $router->get('incomes/create', [
        'as' => 'admin.accounting.income.create',
        'uses' => 'IncomeController@create',
        'middleware' => 'can:accounting.incomes.create'
    ]);
    $router->post('incomes', [
        'as' => 'admin.accounting.income.store',
        'uses' => 'IncomeController@store',
        'middleware' => 'can:accounting.incomes.create'
    ]);
    $router->get('incomes/{income}/edit', [
        'as' => 'admin.accounting.income.edit',
        'uses' => 'IncomeController@edit',
        'middleware' => 'can:accounting.incomes.edit'
    ]);
    $router->put('incomes/{income}', [
        'as' => 'admin.accounting.income.update',
        'uses' => 'IncomeController@update',
        'middleware' => 'can:accounting.incomes.edit'
    ]);
    $router->delete('incomes/{income}', [
        'as' => 'admin.accounting.income.destroy',
        'uses' => 'IncomeController@destroy',
        'middleware' => 'can:accounting.incomes.destroy'
    ]);
    $router->bind('outstanding', function ($id) {
        return app('Modules\Accounting\Repositories\OutstandingRepository')->find($id);
    });
    $router->get('outstandings', [
        'as' => 'admin.accounting.outstanding.index',
        'uses' => 'OutstandingController@index',
        'middleware' => 'can:accounting.outstandings.index'
    ]);
    $router->get('outstandings/create', [
        'as' => 'admin.accounting.outstanding.create',
        'uses' => 'OutstandingController@create',
        'middleware' => 'can:accounting.outstandings.create'
    ]);
    $router->post('outstandings', [
        'as' => 'admin.accounting.outstanding.store',
        'uses' => 'OutstandingController@store',
        'middleware' => 'can:accounting.outstandings.create'
    ]);
    $router->get('outstandings/{outstanding}/edit', [
        'as' => 'admin.accounting.outstanding.edit',
        'uses' => 'OutstandingController@edit',
        'middleware' => 'can:accounting.outstandings.edit'
    ]);
    $router->put('outstandings/{outstanding}', [
        'as' => 'admin.accounting.outstanding.update',
        'uses' => 'OutstandingController@update',
        'middleware' => 'can:accounting.outstandings.edit'
    ]);
    $router->delete('outstandings/{outstanding}', [
        'as' => 'admin.accounting.outstanding.destroy',
        'uses' => 'OutstandingController@destroy',
        'middleware' => 'can:accounting.outstandings.destroy'
    ]);
    $router->get('getModal', [
        'as' => 'getModal',
        'uses' => 'OutstandingController@getModal'
    ]);
    $router->post('postNewPayment', [
        'as' => 'postNewPayment',
        'uses' => 'OutstandingController@postNewPayment'
    ]);
    $router->get('downloadExcel/{type}', [
        'as' => 'downloadExcel',
        'uses' => 'OutstandingController@downloadExcel',
    ]);
    $router->get('getPaymentDetailsModal', [
        'as' => 'getPaymentDetailsModal',
        'uses' => 'ExpenseController@getPaymentDetailsModal'
    ]);
    

// append







});
