<?php

namespace Modules\Accounting\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateExpenseRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'amount' => 'required',
            'package_id' => 'required|not_in:0',
            'category_id' => 'required|not_in:0',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'amount.required' => 'Amount is required',
            'package_id.required' => 'Package name is required',
            'category_id.required' => 'category is required',
        ];
    }

    public function translationMessages()
    {
        return [];
    }
}
