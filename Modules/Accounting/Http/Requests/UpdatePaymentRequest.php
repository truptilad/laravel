<?php

namespace Modules\Accounting\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdatePaymentRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'amount' => 'required',
            'registration_id' => 'required',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'amount.required' => 'Amount is required',
            'registration_id.required' => 'Name is required', 
        ];
    }

    public function translationMessages()
    {
        return [];
    }
}
