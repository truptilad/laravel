<?php

namespace Modules\Accounting\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreatePaymentRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'registration_id' => 'required|not_in:0',
            'amount' => 'required',
    
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'registration_id.required' => 'Name is required',
            'amount.required' => 'Amount is required',
             
        ];
    }

    public function translationMessages()
    {
        return [];
    }
}
