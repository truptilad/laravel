<?php

namespace Modules\Accounting\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateCompanyRequest extends BaseFormRequest
{
    public function rules()
    {
        return [];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
