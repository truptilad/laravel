<?php

namespace Modules\Accounting\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateIncomeRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'income_source_name' => 'required',
            'amount' => 'required',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'income_source_name.required' => 'Loan Source Name is required',
            'amount.required' => 'Amount is required',

        ];
    }

    public function translationMessages()
    {
        return [];
    }
}
