<?php

namespace Modules\Accounting\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateIncomeRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'income_source_name' => 'required',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'income_source_name.required' => 'Income Source Name is required',
        ];
    }

    public function translationMessages()
    {
        return [];
    }
}
