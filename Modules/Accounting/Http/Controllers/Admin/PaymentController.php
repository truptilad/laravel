<?php

namespace Modules\Accounting\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Accounting\Entities\Payment;
use Modules\Accounting\Http\Requests\CreatePaymentRequest;
use Modules\Accounting\Http\Requests\UpdatePaymentRequest;
use Modules\Accounting\Repositories\AccountRepository;
use Modules\Accounting\Repositories\PaymentRepository;
use Modules\Accounting\Repositories\TransactionRepository;
use Modules\Accounting\Repositories\BranchRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Filemanager\Helper\FileMetadata;
use Modules\Filemanager\Repositories\FileRepository;
use Modules\User\Contracts\Authentication;
use Modules\User\Entities\Sentinel\User;
use Modules\User\Repositories\UserRepository;
use Modules\Admin\Repositories\RegistrationRepository;
use Carbon\Carbon;
use DB;

class PaymentController extends AdminBaseController
{
    /**
     * @var PaymentRepository
     */
    private $payment;

    /**
     * @var
     */
    private $auth;

    public function __construct(PaymentRepository $payment,Authentication $auth)
    {
        parent::__construct();

        $this->payment = $payment;
        $this->auth = $auth;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $payments = app(PaymentRepository::class)->allWithBuilder()->with('registration','packageDetails')->orderBy('created_at','DESC')->get();

        return view('accounting::admin.payments.index', compact('payments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // $user = auth()->user()->load('userbranch');
        $user = auth()->user();
        $registrations = app(RegistrationRepository::class)->allWithBuilder()->with('referanceBy')->get();
        
        $branch = app(BranchRepository::class)->all();

        $accounts = app(AccountRepository::class)->all();


        return view('accounting::admin.payments.create',compact('registrations','branch','user','accounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePaymentRequest $request
     * @return Response
     */
    public function store(CreatePaymentRequest $request)
    { 
        try {
            DB::beginTransaction();

            $registration = app(RegistrationRepository::class)->find($request->registration_id);
            
            //Add Entry In Transaction
            $transaction = app(TransactionRepository::class)->createPaymentTransaction($request->all());

            //Make Payment
            $newPayment = $this->payment->create($request->all() + ['package_id' => $request->package_id,'transaction_id' => $transaction->id]);

            $registration->paid = ($registration->paid + $request->amount);
            $registration->pending_amount = $registration->amount -$registration->paid;

            if($registration->paid == $registration->amount) {
                $registration->status = 'Paid';
                $registration->save();    
            }
            else{
                $registration->status = 'Partially Paid';
                $registration->save();    
            }

            if(isset($request->attachment_id))
            {
                if ($request->file('attachment_id')->isValid()) {
                
                    $fileMetadata = new FileMetadata();
                    $fileMetadata->description = 'Description';
                    $fileMetadata->fileStatus = 1;
                    $fileMetadata->ownerUser = $this->auth->user();
                    $fileMetadata->isPublic = true;
                    $fileMetadata->versionNo = 1;
                    $fileMetadata->fileIdentifierFolder = PAYMENT_FOLDER;

                    $fileObject = app(FileRepository::class)->createNewFile($request->attachment_id, PAYMENT_DOCS, $fileMetadata);
                }
                $newPayment->attachment_id = $fileObject->id;
                $newPayment->save();
            }

            DB::commit();

            return redirect()->route('admin.accounting.payment.index')
                ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('accounting::payments.title.payments')]));
            
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->back()
                ->withError($e->getMessage());
        }      
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Payment $payment
     * @return Response
     */
    public function edit(Payment $payment)
    {
    
        $files = app(FileRepository::class)->find($payment->attachment_id);

        $registrations = app(RegistrationRepository::class)->allWithBuilder()->with('referanceBy')->get();

        $registration = app(RegistrationRepository::class)->find($payment->registration_id);
        
        $invoiceformat = $registration->packageDetails['package'].'-'.$payment->status.'-'.$payment->amount;

        $user = app(UserRepository::class)->find(auth()->user()->id);

        $branch = app(BranchRepository::class)->all();

        $accounts = app(AccountRepository::class)->all();


        return view('accounting::admin.payments.edit', compact('payment','registrations','files','invoices','invoiceformat','user','accounts','branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Payment $payment
     * @param  UpdatePaymentRequest $request
     * @return Response
     */
    public function update(Payment $payment, UpdatePaymentRequest $request)
    {
        try
            {

            DB::beginTransaction();

            $registration = app(RegistrationRepository::class)->find($request->registration_id);
            //Update Transaction
            $transaction = app(TransactionRepository::class)->find($payment->transaction_id);

            $updatetransaction = app(TransactionRepository::class)->updatePaymentTransaction($transaction,$request);

            $oldAmount = $payment->amount;

            //Update Payment
            $updatePayment =  $this->payment->update($payment, $request->all());

            if($registration->paid != $request->amount) {

                $registration->paid = $registration->paid - $oldAmount;
                // $updateAmount = $registration->paid
                $registration->paid = ($registration->paid + $request->amount);
                $registration->pending_amount = $registration->amount -$registration->paid;

                if($registration->paid == $registration->amount) {
                    $registration->status = 'Paid';
                    $registration->save();    
                }
                else{
                    $registration->status = 'Partially Paid';
                    $registration->save();    
                }                
            }

            
            if(isset($request->attachment_id))
            {
                if($request->file('attachment_id')->isValid()) {
                    $fileMetadata = new FileMetadata();
                    $fileMetadata->description = 'Description';
                    $fileMetadata->fileStatus = 1;
                    $fileMetadata->ownerUser = $this->auth->user();
                    $fileMetadata->isPublic = true;
                    $fileMetadata->versionNo = 1;
                    $fileMetadata->fileIdentifierFolder = "Attachments";

                    $fileObject = app(FileRepository::class)->createNewFile($request->attachment_id,PAYMENT_DOCS,$fileMetadata);
                }
                $updatePayment->attachment_id = $fileObject->id;
                $updatePayment->save();
            }

            DB::commit();

            return redirect()->route('admin.accounting.payment.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('accounting::payments.title.payments')]));

        } catch (Exception $e) {

            DB::rollback();

            return redirect()->back()
                ->withError($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Payment $payment
     * @return Response
     */
    public function destroy(Payment $payment)
    {
        $this->payment->destroy($payment);

        return redirect()->route('admin.accounting.payment.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('accounting::payments.title.payments')]));
    }
}
