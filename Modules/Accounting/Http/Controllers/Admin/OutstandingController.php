<?php

namespace Modules\Accounting\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Accounting\Entities\Outstanding;
use Modules\Accounting\Http\Requests\CreateOutstandingRequest;
use Modules\Accounting\Http\Requests\UpdateOutstandingRequest;
use Modules\Accounting\Repositories\OutstandingRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Admin\Repositories\RegistrationRepository;
use Modules\Accounting\Repositories\TransactionRepository;
use Modules\Accounting\Repositories\PaymentRepository;
use Maatwebsite\Excel\Facades\Excel;

class OutstandingController extends AdminBaseController
{
    /**
     * @var OutstandingRepository
     */
    private $outstanding;

    public function __construct(OutstandingRepository $outstanding)
    {
        parent::__construct();

        $this->outstanding = $outstanding;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $outstandingRegs = app(RegistrationRepository::class)->allWithBuilder()->with('packageInfo','referanceBy')->where('status','!=','Paid')->get(); 

        return view('accounting::admin.outstandings.index', compact('outstandingRegs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('accounting::admin.outstandings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateOutstandingRequest $request
     * @return Response
     */
    public function store(CreateOutstandingRequest $request)
    {
        $this->outstanding->create($request->all());

        return redirect()->route('admin.accounting.outstanding.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('accounting::outstandings.title.outstandings')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Outstanding $outstanding
     * @return Response
     */
    public function edit(Outstanding $outstanding)
    {
        return view('accounting::admin.outstandings.edit', compact('outstanding'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Outstanding $outstanding
     * @param  UpdateOutstandingRequest $request
     * @return Response
     */
    public function update(Outstanding $outstanding, UpdateOutstandingRequest $request)
    {
        $this->outstanding->update($outstanding, $request->all());

        return redirect()->route('admin.accounting.outstanding.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('accounting::outstandings.title.outstandings')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Outstanding $outstanding
     * @return Response
     */
    public function destroy(Outstanding $outstanding)
    {
        $this->outstanding->destroy($outstanding);

        return redirect()->route('admin.accounting.outstanding.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('accounting::outstandings.title.outstandings')]));
    }

    public function getModal(Request $request) {

        $paymentDetail = app(RegistrationRepository::class)->allWithBuilder()->with('packageInfo')->find($request->id);

        return view('accounting::modal.payment-modal',compact('paymentDetail'));
    }

    public function postNewPayment(Request $request){

        //Add Entry In Transaction
        $transaction = app(TransactionRepository::class)->createPaymentTransaction($request);

        $registration = app(RegistrationRepository::class)->find($request->registration_id);

        //Make Payment
        $newPayment = app(PaymentRepository::class)->allWithBuilder()->create($request->all() + ['package_id' => $request->package_id,'transaction_id' => $transaction->id ]);

        $registration->paid = $registration->paid + $request->amount;
        $registration->pending_amount = $registration->amount -$registration->paid;     
        $registration->save();


        // Send Sms On Payment
        $paymentMessage = "Dear " . $registration['first_name']." ". $registration['lad_name'].","."Your Payment of amount Rs. ".$request->amount." is Successfully Done".".";

        app(RegistrationRepository::class)->sendSms($registration,$paymentMessage);
        
        if($registration->paid  == $registration->amount) {
            $registration->status = 'Paid';
            $registration->save();
            $newPayment->payment_status = 'Paid';
            $newPayment->save();     
        }
        else{
            $registration->status = 'Partially Paid';
            $registration->save();
            $newPayment->payment_status = 'Partially Paid';
            $newPayment->save();    
        }

        
        return response()->json([ 'success' => true]);
    }

    public function downloadExcel($type)
    {
        // $data = app(OutstandingRepository::class)->all();
        
        app(RegistrationRepository::class)->createExcel($data,$type);
        return'fgsdf';

        return redirect()->route('admin.accounting.outstanding.index');
    }

}
