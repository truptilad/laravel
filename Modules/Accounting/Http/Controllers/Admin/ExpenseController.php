<?php

namespace Modules\Accounting\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Accounting\Entities\Expense;
use Modules\Accounting\Http\Requests\CreateExpenseRequest;
use Modules\Accounting\Http\Requests\UpdateExpenseRequest;
use Modules\Accounting\Repositories\ExpenseCategoryRepository;
use Modules\Accounting\Repositories\ExpenseRepository;
use Modules\Accounting\Repositories\TransactionRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Filemanager\Helper\FileMetadata;
use Modules\Filemanager\Repositories\FileRepository;
use Modules\Accounting\Repositories\BranchRepository;
use Modules\Admin\Repositories\PackageRepository;
use Modules\User\Contracts\Authentication;
use Modules\User\Entities\Sentinel\User;
use Modules\User\Repositories\UserRepository;
use Modules\Accounting\Repositories\AccountRepository;
use Modules\Accounting\Repositories\PaymentRepository;
use Modules\Accounting\Repositories\IncomeRepository;
use Carbon\Carbon;

class ExpenseController extends AdminBaseController
{
    /**
     * @var ExpenseRepository
     */
    private $expense;

    /**
     * @var
     */
    private $auth;



    public function __construct(ExpenseRepository $expense,Authentication $auth)
    {
        parent::__construct();

        $this->expense = $expense;
        $this->auth = $auth;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $expenses = app(ExpenseRepository::class)->allWithBuilder()->with('category','packageDetail')->orderBy('date','desc')->get();

        return view('accounting::admin.expenses.index', compact('expenses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $packages = app(PackageRepository::class)->allWithBuilder()
            ->orderBy('package')
            ->pluck('package','id');

        $expensecategory = app(ExpenseCategoryRepository::class)->allWithBuilder()
            ->where('type_id',null)
            ->get();

        $accounts = app(AccountRepository::class)->all();

        $user = app(UserRepository::class)->find(auth()->user()->id);
        
        return view('accounting::admin.expenses.create',compact('category','packages','expensecategory','user','accounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateExpenseRequest $request
     * @return Response
     */
    public function store(CreateExpenseRequest $request)
    {
    
        //Adds Entry In Transaction
        $transaction =app(TransactionRepository::class)->createExpenseTransaction($request);

        //Create Expense
        $newExpense  = $this->expense->create($request->all() + ['transaction_id' => $transaction->id]);
        
        $newExpense->cheque_date = carbon::parse($request->cheque_date)->toDateString();
        $newExpense->save(); 
   
        //Upload File
       if(isset($request->attachment_id))
       {
           if($request->file('attachment_id')->isValid()) {

               $fileMetadata = new FileMetadata();
               $fileMetadata->description = 'Description';
               $fileMetadata->fileStatus = 1;
               $fileMetadata->ownerUser = $this->auth->user();
               $fileMetadata->isPublic = true;
               $fileMetadata->versionNo = 1;
               $fileMetadata->fileIdentifierFolder = "Attachments";
               
               $fileObject = app(FileRepository::class)->createNewFile($request->attachment_id,PAYMENT_DOCS,$fileMetadata);
           }
           $newExpense->attachment_id = $fileObject->id;
           $newExpense->save();
       }
        return redirect()->route('admin.accounting.expense.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('accounting::expenses.title.expenses')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Expense $expense
     * @return Response
     */
    public function edit(Expense $expense)
    {
        $files = app(FileRepository::class)->find($expense->attachment_id);

        $packages = app(PackageRepository::class)->allWithBuilder()
            ->orderBy('package')
            ->pluck('package','id');

        $expensecategoryrepo = app(ExpenseCategoryRepository::class);

        $accounts = app(AccountRepository::class)->all();

        $expensecategory = $expensecategoryrepo->allWithBuilder()
            ->whereNull('type_id')
            ->get();

        return view('accounting::admin.expenses.edit', compact('expense','expensecategory','packages','files','accounts'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Expense $expense
     * @param  UpdateExpenseRequest $request
     * @return Response
     */
    public function update(Expense $expense, UpdateExpenseRequest $request)
    {
        //Update Transaction
        $transaction = app(TransactionRepository::class)->find($expense->transaction_id);
        $updatetransaction = app(TransactionRepository::class)->updateExpenseTransaction($transaction,$request);
        
        
        //Update Expense
        $oldExpense = $this->expense->update($expense, $request->all());
        $oldExpense->cheque_date = carbon::parse($request->cheque_date)->toDateString();
        $oldExpense->save();
        
        
        //Update Filemanager
        if(isset($request->attachment_id))
        {
            if($request->file('attachment_id')->isValid()) {
                $fileMetadata = new FileMetadata();
                $fileMetadata->description = 'Description';
                $fileMetadata->fileStatus = 1;
                $fileMetadata->ownerUser = $this->auth->user();
                $fileMetadata->isPublic = true;
                $fileMetadata->versionNo = 1;
                $fileMetadata->fileIdentifierFolder = "Attachments";

                $fileObject = app(FileRepository::class)->createNewFile($request->attachment_id,PAYMENT_DOCS,$fileMetadata);
            }
            $oldExpense->attachment_id = $fileObject->id;
            $oldExpense->save();
        }

        return redirect()->route('admin.accounting.expense.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('accounting::expenses.title.expenses')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Expense $expense
     * @return Response
     */
    public function destroy(Expense $expense)
    {
        $this->expense->destroy($expense);

        return redirect()->route('admin.accounting.expense.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('accounting::expenses.title.expenses')]));
    }

    public function getPaymentDetailsModal(Request $request)
    {
        if($request->category == 'expense')
        {
            $paymentDetail = $this->expense->find($request->Id);
        }
        elseif ($request->category == 'payment') {
            
            $paymentDetail = app(PaymentRepository::class)->allWithBuilder()->where('id','=',$request->Id)->first();
        }
        else {
            $paymentDetail = app(IncomeRepository::class)->allWithBuilder()->where('id','=',$request->Id)->first();   
        }
        $modeType = $request->modeType;

        return view('accounting::modal.paymentMode-details',compact('modeType','paymentDetail'));
    }
    
}
