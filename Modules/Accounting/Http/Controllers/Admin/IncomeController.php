<?php

namespace Modules\Accounting\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Accounting\Entities\Income;
use Modules\Accounting\Http\Requests\CreateIncomeRequest;
use Modules\Accounting\Http\Requests\UpdateIncomeRequest;
use Modules\Accounting\Repositories\IncomeRepository;
use Modules\Accounting\Repositories\ExpenseCategoryRepository;
use Modules\Accounting\Repositories\AccountRepository;
use Modules\Admin\Repositories\PackageRepository;
use Modules\Accounting\Repositories\TransactionRepository;
use Modules\Filemanager\Helper\FileMetadata;
use Modules\Filemanager\Repositories\FileRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\User\Contracts\Authentication;
use Carbon\Carbon;

class IncomeController extends AdminBaseController
{
    /**
     * @var IncomeRepository
     */
    private $income;

    public function __construct(IncomeRepository $income,Authentication $auth)
    {
        parent::__construct();

        $this->income = $income;
        $this->auth = $auth;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $incomes = app(IncomeRepository::class)->allWithBuilder()->orderBy('date','desc')->get();

        return view('accounting::admin.incomes.index', compact('incomes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $packages = app(PackageRepository::class)->allWithBuilder()
            ->orderBy('package')
            ->pluck('package','id');

        $expensecategory = app(ExpenseCategoryRepository::class)->allWithBuilder()
            ->where('type_id',null)
            ->get();

        $accounts = app(AccountRepository::class)->all();

        return view('accounting::admin.incomes.create',compact('packages','expensecategory','accounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateIncomeRequest $request
     * @return Response
     */
    public function store(CreateIncomeRequest $request)
    {
        
        //Adds Entry In Transaction
        $transaction =app(TransactionRepository::class)->createIncomeTransaction($request);

        $newIncome = $this->income->create($request->all()+ ['transaction_id' => $transaction->id]);
        $newIncome->date = carbon::parse($request->date)->toDateString();
        $newIncome->cheque_date = carbon::parse($request->cheque_date)->toDateString();
        $newIncome->save();

        //Upload File
        if(isset($request->attachment_id))
        {
            if($request->file('attachment_id')->isValid()) {

                $fileMetadata = new FileMetadata();
                $fileMetadata->description = 'Description';
                $fileMetadata->fileStatus = 1;
                $fileMetadata->ownerUser = $this->auth->user();
                $fileMetadata->isPublic = true;
                $fileMetadata->versionNo = 1;
                $fileMetadata->fileIdentifierFolder = "Attachments";
               
                $fileObject = app(FileRepository::class)->createNewFile($request->attachment_id,INCOME_DOCS,$fileMetadata);
            }
            $newIncome->attachment_id = $fileObject->id;
            $newIncome->save();
        }

        return redirect()->route('admin.accounting.income.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('accounting::incomes.title.incomes')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Income $income
     * @return Response
     */
    public function edit(Income $income)
    {

        $files = app(FileRepository::class)->find($income->attachment_id);

        $packages = app(PackageRepository::class)->allWithBuilder()
            ->orderBy('package')
            ->pluck('package','id');

        $expensecategoryrepo = app(ExpenseCategoryRepository::class);

        $accounts = app(AccountRepository::class)->all();

        $expensecategory = $expensecategoryrepo->allWithBuilder()
            ->whereNull('type_id')
            ->get();

        return view('accounting::admin.incomes.edit', compact('income','files','packages','accounts','expensecategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Income $income
     * @param  UpdateIncomeRequest $request
     * @return Response
     */
    public function update(Income $income, UpdateIncomeRequest $request)
    {
        $transaction = app(TransactionRepository::class)->find($income->transaction_id);
        $updatetransaction = app(TransactionRepository::class)->updateIncomeTransaction($transaction,$request);

        $updateIncome = $this->income->update($income, $request->all());
        $updateIncome->date = carbon::parse($request->date)->toDateString();
        $updateIncome->cheque_date = carbon::parse($request->cheque_date)->toDateString();
        $updateIncome->save();

        if(isset($request->attachment_id))
        {
            if($request->file('attachment_id')->isValid()) {
                $fileMetadata = new FileMetadata();
                $fileMetadata->description = 'Description';
                $fileMetadata->fileStatus = 1;
                $fileMetadata->ownerUser = $this->auth->user();
                $fileMetadata->isPublic = true;
                $fileMetadata->versionNo = 1;
                $fileMetadata->fileIdentifierFolder = "Attachments";

                $fileObject = app(FileRepository::class)->createNewFile($request->attachment_id,PAYMENT_DOCS,$fileMetadata);
            }
            $updateIncome->attachment_id = $fileObject->id;
            $updateIncome->save();
        }

        return redirect()->route('admin.accounting.income.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('accounting::incomes.title.incomes')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Income $income
     * @return Response
     */
    public function destroy(Income $income)
    {
        $this->income->destroy($income);

        return redirect()->route('admin.accounting.income.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('accounting::incomes.title.incomes')]));
    }
}
