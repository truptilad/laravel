<?php

namespace Modules\Accounting\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Accounting\Entities\ExpenseCategory;
use Modules\Accounting\Http\Requests\CreateExpenseCategoryRequest;
use Modules\Accounting\Http\Requests\UpdateExpenseCategoryRequest;
use Modules\Accounting\Repositories\ExpenseCategoryRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class ExpenseCategoryController extends AdminBaseController
{
    /**
     * @var ExpenseCategoryRepository
     */
    private $expensecategory;

    public function __construct(ExpenseCategoryRepository $expensecategory)
    {
        parent::__construct();

        $this->expensecategory = $expensecategory;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $expensecategories = $this->expensecategory->all();
        
        $categories = $this->expensecategory->getByAttributes(['type_id' => null]);
        
        return view('accounting::admin.expensecategories.index', compact('expensecategories','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('accounting::admin.expensecategories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateExpenseCategoryRequest $request
     * @return Response
     */
    public function store(CreateExpenseCategoryRequest $request)
    {
        $this->expensecategory->create($request->all());

        return redirect()->route('admin.accounting.expensecategory.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('accounting::expensecategories.title.expensecategories')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  ExpenseCategory $expensecategory
     * @return Response
     */
    public function edit(ExpenseCategory $expensecategory)
    {
        return view('accounting::admin.expensecategories.edit', compact('expensecategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ExpenseCategory $expensecategory
     * @param  UpdateExpenseCategoryRequest $request
     * @return Response
     */
    public function update(ExpenseCategory $expensecategory, UpdateExpenseCategoryRequest $request)
    {
        
        $expensecategory = $this->expensecategory->find($request->category_id);
        $this->expensecategory->update($expensecategory, $request->all());

        return redirect()->route('admin.accounting.expensecategory.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('accounting::expensecategories.title.expensecategories')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ExpenseCategory $expensecategory
     * @return Response
     */
    public function destroy(ExpenseCategory $expensecategory)
    {
        $this->expensecategory->destroy($expensecategory);

        return redirect()->route('admin.accounting.expensecategory.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('accounting::expensecategories.title.expensecategories')]));
    }

    public function subcategory(Request $request)
    {
        $subcategory = $this->expensecategory->getByAttributes(['type_id' => $request->id]);
        
        return response()->json(['subcategory' => $subcategory]);
    }
}
