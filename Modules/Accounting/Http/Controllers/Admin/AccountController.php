<?php

namespace Modules\Accounting\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Accounting\Entities\Account;
use Modules\Accounting\Http\Requests\CreateAccountRequest;
use Modules\Accounting\Http\Requests\UpdateAccountRequest;
use Modules\Accounting\Repositories\AccountRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Accounting\Repositories\BranchRepository;
use Modules\Accounting\Repositories\CompanyRepository;

class AccountController extends AdminBaseController
{
    /**
     * @var AccountRepository
     */
    private $account;

    public function __construct(AccountRepository $account)
    {
        parent::__construct();

        $this->account = $account;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $accounts = $this->account->all();
        $branchs = app(BranchRepository::class)->all();
        $companies = app(CompanyRepository::class)->all();
    
        return view('accounting::admin.accounts.index', compact('accounts','branchs','companies'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('accounting::admin.accounts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateAccountRequest $request
     * @return Response
     */
    public function store(CreateAccountRequest $request)
    {
        $this->account->create($request->all());

        return redirect()->route('admin.accounting.account.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('accounting::accounts.title.accounts')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Account $account
     * @return Response
     */
    public function edit(Account $account)
    {
        return view('accounting::admin.accounts.edit', compact('account'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Account $account
     * @param  UpdateAccountRequest $request
     * @return Response
     */
    public function update(Account $account, UpdateAccountRequest $request)
    {
        $account = $this->account->find($request->account_id);
        $this->account->update($account, $request->all());

        return redirect()->route('admin.accounting.account.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('accounting::accounts.title.accounts')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Account $account
     * @return Response
     */
    public function destroy(Account $account)
    {
        $this->account->destroy($account);

        return redirect()->route('admin.accounting.account.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('accounting::accounts.title.accounts')]));
    }

    /**
     * @param Request $request
     */
    public function getAccount(Request $request)
    {
        $account = $this->account->getByAttributes(['branch_id' => $request->branchId]);

        return response()->json(['success'=>true, 'account' => $account]);
    }
}
