<?php

namespace Modules\Accounting\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Accounting\Entities\AccountType;
use Modules\Accounting\Http\Requests\CreateAccountTypeRequest;
use Modules\Accounting\Http\Requests\UpdateAccountTypeRequest;
use Modules\Accounting\Repositories\AccountTypeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class AccountTypeController extends AdminBaseController
{
    /**
     * @var AccountTypeRepository
     */
    private $accounttype;

    public function __construct(AccountTypeRepository $accounttype)
    {
        parent::__construct();

        $this->accounttype = $accounttype;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $accounttypes = $this->accounttype->all();

        return view('accounting::admin.accounttypes.index', compact('accounttypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('accounting::admin.accounttypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateAccountTypeRequest $request
     * @return Response
     */
    public function store(CreateAccountTypeRequest $request)
    {
        $this->accounttype->create($request->all());

        return redirect()->route('admin.accounting.accounttype.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('accounting::accounttypes.title.accounttypes')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  AccountType $accounttype
     * @return Response
     */
    public function edit(AccountType $accounttype)
    {
        return view('accounting::admin.accounttypes.edit', compact('accounttype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AccountType $accounttype
     * @param  UpdateAccountTypeRequest $request
     * @return Response
     */
    public function update(AccountType $accounttype, UpdateAccountTypeRequest $request)
    {
        $accounttype = $this->accounttype->find($request->accounttype_id);
        
        $this->accounttype->update($accounttype, $request->all());

        return redirect()->route('admin.accounting.accounttype.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('accounting::accounttypes.title.accounttypes')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  AccountType $accounttype
     * @return Response
     */
    public function destroy(AccountType $accounttype)
    {
        $this->accounttype->destroy($accounttype);

        return redirect()->route('admin.accounting.accounttype.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('accounting::accounttypes.title.accounttypes')]));
    }
}
