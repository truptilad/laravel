<?php

namespace Modules\Accounting\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Accounting\Entities\Branch;
use Modules\Accounting\Http\Requests\CreateBranchRequest;
use Modules\Accounting\Http\Requests\UpdateBranchRequest;
use Modules\Accounting\Repositories\BranchRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class BranchController extends AdminBaseController
{
    /**
     * @var BranchRepository
     */
    private $branch;

    public function __construct(BranchRepository $branch)
    {
        parent::__construct();

        $this->branch = $branch;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$branches = $this->branch->all();

        return view('accounting::admin.branches.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('accounting::admin.branches.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateBranchRequest $request
     * @return Response
     */
    public function store(CreateBranchRequest $request)
    {
        $this->branch->create($request->all());

        return redirect()->route('admin.accounting.branch.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('accounting::branches.title.branches')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Branch $branch
     * @return Response
     */
    public function edit(Branch $branch)
    {
        return view('accounting::admin.branches.edit', compact('branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Branch $branch
     * @param  UpdateBranchRequest $request
     * @return Response
     */
    public function update(Branch $branch, UpdateBranchRequest $request)
    {
        $this->branch->update($branch, $request->all());

        return redirect()->route('admin.accounting.branch.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('accounting::branches.title.branches')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Branch $branch
     * @return Response
     */
    public function destroy(Branch $branch)
    {
        $this->branch->destroy($branch);

        return redirect()->route('admin.accounting.branch.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('accounting::branches.title.branches')]));
    }
}
