<?php

namespace Modules\Accounting\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Accounting\Entities\Quotation;
use Modules\Accounting\Http\Requests\CreateQuotationRequest;
use Modules\Accounting\Http\Requests\UpdateQuotationRequest;
use Modules\Accounting\Repositories\QuotationRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class QuotationController extends AdminBaseController
{
    /**
     * @var QuotationRepository
     */
    private $quotation;

    public function __construct(QuotationRepository $quotation)
    {
        parent::__construct();

        $this->quotation = $quotation;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$quotations = $this->quotation->all();

        return view('accounting::admin.quotations.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('accounting::admin.quotations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateQuotationRequest $request
     * @return Response
     */
    public function store(CreateQuotationRequest $request)
    {
        $this->quotation->create($request->all());

        return redirect()->route('admin.accounting.quotation.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('accounting::quotations.title.quotations')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Quotation $quotation
     * @return Response
     */
    public function edit(Quotation $quotation)
    {
        return view('accounting::admin.quotations.edit', compact('quotation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Quotation $quotation
     * @param  UpdateQuotationRequest $request
     * @return Response
     */
    public function update(Quotation $quotation, UpdateQuotationRequest $request)
    {
        $this->quotation->update($quotation, $request->all());

        return redirect()->route('admin.accounting.quotation.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('accounting::quotations.title.quotations')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Quotation $quotation
     * @return Response
     */
    public function destroy(Quotation $quotation)
    {
        $this->quotation->destroy($quotation);

        return redirect()->route('admin.accounting.quotation.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('accounting::quotations.title.quotations')]));
    }
}
