<?php

namespace Modules\Accounting\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Accounting\Entities\Invoice;
use Modules\Accounting\Http\Requests\CreateInvoiceRequest;
use Modules\Accounting\Http\Requests\UpdateInvoiceRequest;
use Modules\Accounting\Repositories\InvoiceRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Master\Repositories\BranchRepository;
use Modules\Master\Repositories\CompanyRepository;

class InvoiceController extends AdminBaseController
{
    /**
     * @var InvoiceRepository
     */
    private $invoice;

    public function __construct(InvoiceRepository $invoice)
    {
        parent::__construct();

        $this->invoice = $invoice;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $invoices = $this->invoice->all();

        return view('accounting::admin.invoices.index', compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('accounting::admin.invoices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateInvoiceRequest $request
     * @return Response
     */
    public function store(CreateInvoiceRequest $request)
    {
        $this->invoice->create($request->all());

        return redirect()->route('admin.accounting.invoice.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('accounting::invoices.title.invoices')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Invoice $invoice
     * @return Response
     */
    public function edit(Invoice $invoice)
    {
        $invoice->load('orders','orders.orderproduct','company','branch','client');
        return view('accounting::admin.invoices.edit', compact('invoice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Invoice $invoice
     * @param  UpdateInvoiceRequest $request
     * @return Response
     */
    public function update(Invoice $invoice, UpdateInvoiceRequest $request)
    {
        $this->invoice->update($invoice, $request->all());

        return redirect()->route('admin.accounting.invoice.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('accounting::invoices.title.invoices')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Invoice $invoice
     * @return Response
     */
    public function destroy(Invoice $invoice)
    {
        $this->invoice->destroy($invoice);

        return redirect()->route('admin.accounting.invoice.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('accounting::invoices.title.invoices')]));
    }


    public function view(Invoice $invoice)
    {
        $invoice->load('orders','orders.orderproduct','company','branch');
        return view('accounting::admin.invoices.invoice', compact('invoice'));
    }


}
