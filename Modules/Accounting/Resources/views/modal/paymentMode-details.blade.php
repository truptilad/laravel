@section('styles')
<style type="text/css">
    .font_20{
        font-size: 20px;
    }
</style>
<?php use Carbon\Carbon; ?>
<div class="modal-dialog modal-lg" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">x</span>
                    <span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title">Cash Transfer Details</h3>
            </div>
            <div class="box-body font_20">
                @if(isset($paymentDetail))
                    @if($modeType == 1)
                        <div class="row">
                            <div class="col-md-12">
                                <label>Date :</label>
                                <span>{{carbon::parse($paymentDetail->cheque_date)->format(PHP_DATE_FORMAT)}}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Bank Name :</label>
                                <span>{{ isset($paymentDetail->cheque_bank)?$paymentDetail->cheque_bank:'-'}} {{ isset($paymentDetail->cheque_branch)?'('.$paymentDetail->cheque_branch.')':''}}</span>
                            </div>    
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Cheque Number :</label>
                                <span>{{ $paymentDetail->cheque_number}}</span>
                            </div>    
                        </div>
                    @else
                        <div class="row">
                            <div class="col-md-6">
                                <label>Referance Id :</label>
                                <span>{{$paymentDetail->reference_code}}</span>
                            </div>    
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Transfer To :</label>
                                <span>{{$paymentDetail->transfer_to}}</span>
                            </div>
                            <div class="col-md-6">
                                <label>Transfer From :</label>
                                <span>{{$paymentDetail->transfer_from}}</span>
                            </div>    
                        </div>
                    @endif
                @else

                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>