@section('styles')
<style type="text/css">
    .font_20{
        font-size: 15px;
    }
</style>
<?php use Carbon\Carbon; ?>
<div class="modal-dialog modal-lg" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">x</span>
                    <span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title">Message</h3>
            </div>
            <div class="box-body font_20">
                <div class="row">
                    <div class="col-md-12">
                        <label>Tour</label>
                        <select class="form-control" name="packageId" id="packageId" value="{{ old('package') }}">

                            @foreach($packages as $package)
                                @if($package->id == $packageId)
                                    <option selected value="{{$package->id}}">{{$package->package}}</option>
                                @else
                                    <option value="{{$package->id}}">{{$package->package}}</option>
                                @endif
                            
                            @endforeach
                        </select>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <label>Message :</label>
                        <textarea class="form-control" rows="5" id="message" value=""></textarea>
                    </div>    
                </div>
                       
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" id="sendSms" onclick="postGroupMessage()">Send</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#packageId').select2({
            placeholder:'Select Tour',
            width:'100%'
        });
    })

    function postGroupMessage() {

        $('#sendSms').prop("disabled", true);

        $.ajax({
            type: 'POST',
            url: '{{ URL::route('postGroupMessage')}}',
            data: {
                packageId: $('#packageId').val(),
                message: $('#message').val(),
               
                _token: $('meta[name="token"]').attr('value'),
            },
            success: function (response) {

                $('#sendSms').prop("disabled", false);
                $('#commonModal').modal('hide');
                toastr.success('Sms Send');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Oops.","Something went wrong, Please try again", "error");
            }
        });

    }

</script>