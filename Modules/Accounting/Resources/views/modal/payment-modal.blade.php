<div class="modal-dialog modal-lg" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">x</span>
                    <span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title">{{ $paymentDetail->first_name }} {{ $paymentDetail->last_name }}</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{isset($paymentDetail->packageInfo)?$paymentDetail->packageInfo->package:''}}</h4>
                    </div>
                    <div class="col-md-6">
                        <span><h4><i class="fa fa-inr"> {{$paymentDetail->amount}}</i></h4></span>
                    </div>
                </div>
                <?php $outstandingAmount = $paymentDetail->amount - $paymentDetail->paid; ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <div class="input-group">
                                <span class="input-group-addon" >
                                <i class="fa fa-inr"></i>
                                </span>
                                <input class="form-control" id="amount" type="text" id="amount" value="{{$outstandingAmount}}" >
                            </div>
                            <span id="amount-alert" style="display: none;color: red">Amount can not be more than Oustanding amount <i class="fa fa-inr"></i> {{$outstandingAmount}}</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Payment-mode">Payment Mode</label><br>
                            <select id="mode" name="mode" class="form-control">
                                <option value="0">Cash</option>
                                <option value="1">Cheque</option>
                                <option value="2">Bank Transfer</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row" id="cheque1" style="display: none;">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cheque_date" >Cheque date* </label>
                            <input id="cheque_date" type="text" name="cheque_date" class="form-control">
                            <span id="error-chequeDate" style="display: none;color: red">Cheque Date is required</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cheque_number" >Cheque number*</label>
                            <input id="cheque_number" type="text" name="cheque_number" class="form-control">
                            <span id="error-cheque_number" style="display: none;color: red">Cheque Number is required</span>
                        </div>
                    </div>
                </div>
                <div id="cheque"  class="row" style="display: none;">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cheque_bank" >Cheque bank* </label>
                            <input id="cheque_bank" type="text" name="cheque_bank" class="form-control" >
                            <span id="error-bankName" style="display: none;color: red">Bank Name is required</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cheque_branch">Cheque branch*</label>
                            <input id="cheque_branch" type="text" name="cheque_branch" class="form-control">
                            <span id="error-bankBranch" style="display: none;color: red">Bank Branch is required</span>
                        </div>
                    </div>
                </div>
                <div class="row" id="net_transfer" hidden>
                    <div class="col-md-6">
                        {!!
                         Former::text('reference_code')->label('Reference Id*')
                        !!}
                        <label id="error-reference_code" style="display: none;color: red">Referance Id is required</label>
                    </div>

                    <div class="col-md-6">
                        {!!
                         Former::text('transfer_from')
                         !!}
                    </div>
                </div>
                <br>
                <div class="row" id="net_transfer1" hidden>
                    <div class="col-md-6">
                        {!!
                         Former::text('transfer_to')->lable('Transfer To')
                        !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">{{ trans('core::core.button.cancel') }}</button>
                <button type="button" class="btn btn-primary pull-right" onclick="makeNewPayment({{ $paymentDetail->id}})">Save</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('#mode').select2({
            placeholder:'Select Mode',
            width:'100%'
        });

        $("#mode").change(function () {
            if ($('#mode').val() == 1) {
                $("#cheque").show();
                $("#cheque1").show();
                $("#net_transfer").hide();
                $("#net_transfer1").hide();
            }
            else if($('#mode').val() == 2) {
                
                $("#net_transfer").show();
                $("#net_transfer1").show();
                $("#cheque").hide();
                $("#cheque1").hide();
            }
            else{
                $("#cheque").hide();
                $("#cheque1").hide();
                $("#net_transfer").hide();
                $("#net_transfer1").hide();
            }
        });

        $('#cheque_date,#date').datetimepicker({
            format:'{{PHP_DATE_FORMAT}}',
            value:new Date(),
            timepicker:false
        });

        $('#amount').change(function() {
            if($('#amount').val() > {{$outstandingAmount}} ){
                $('#amount-alert').show();
            }
            else{
                $('#amount-alert').hide();   
            }
        });


    });

    function makeNewPayment($registrationId) {

        if ($('#mode').val() == 1){

            if($('#cheque_date').val() == ''){
                $('#error-chequeDate').show();
                $('#error-bankName').hide();
                $('#error-bankBranch').hide();
                $('#error-cheque_number').hide();
            }
            else if($('#cheque_number').val() == ''){
                $('#error-cheque_number').show();
                $('#error-bankBranch').hide();
                $('#error-bankName').hide();
                $('#error-chequeDate').hide();   
            }
            else if($('#cheque_bank').val() == ''){

                $('#error-bankName').show();
                $('#error-bankBranch').hide();
                $('#error-chequeDate').hide();
                $('#error-cheque_number').hide();   
            }
            else if($('#cheque_branch').val() == ''){
                $('#error-bankBranch').show();
                $('#error-bankName').hide();
                $('#error-chequeDate').hide();
                $('#error-cheque_number').hide();   
            }
            else{
                $('#error-chequeDate').hide();
                $('#error-bankName').hide();
                $('#error-bankBranch').hide();
                $('#error-cheque_number').hide();   
                
                savePayment($registrationId);
            }
        }
        else if ($('#mode').val() == 2){

            if ($('#reference_code').val() == '' ){
                
                $('#error-reference_code').show();
            }
            else{
                
                $('#error-reference_code').hide();
                savePayment($registrationId);   
            }

        }
        else{
            savePayment($registrationId);
        }
    }

    function savePayment($registrationId) {

        $.ajax({
            type: 'POST',
            url: '{{ URL::route('postNewPayment')}}',
            data: {
                registration_id: $registrationId,
                package_id: {{$paymentDetail->package}},
                mode: $('#mode').val(),
                amount: $('#amount').val(),
                account_id: 1,
                cheque_bank: $('#cheque_bank').val(),
                cheque_branch: $('#cheque_branch').val(),
                cheque_date: $('#cheque_date').val(),
                cheque_number: $('#cheque_number').val(),
                date: new Date(),
                _token: $('meta[name="token"]').attr('value'),
            },
            success: function (response) {
                
                $('#commonModal').modal('hide');
                toastr.success('Payment Done');
                location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Oops.", "Something went wrong, Please try again", "error");
            }
        });

    }


</script>