 <div class="box-body">
    <div class="row">
        <div class="col-md-6">
            {!!
             Former::text('date')
             !!}
        </div>
    </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="registration_id" class="control-label col-md-3 col-lg-3 col-sm-6">Registered Client</label> 
                    <div class="col-md-9 col-lg-9 col-sm-6">
                        <select data-placeholder="Select Registered Client" id="registration_id" name="registration_id" class="form-control" >
                            @foreach($registrations as $registration)
                                <option ></option>
                                @if($registration->referanceBy != null && isset($registration->referanceBy))
                                    @if(isset($payment))
                                        @if($payment->registration_id == $registration->id )
                                            <option value="{{$registration->id}}" selected>{{ $registration->first_name}} {{ $registration->last_name}}  ( Referance : {{$registration->referanceBy->first_name}} {{ $registration->referanceBy->last_name }} )</option>
                                        @else
                                            <option value="{{$registration->id}}">{{ $registration->first_name}} {{ $registration->last_name}}  ( Referance : {{$registration->referanceBy->first_name}} {{ $registration->referanceBy->last_name }} )</option>
                                        @endif
                                    @else
                                        <option value="{{$registration->id}}">{{ $registration->first_name}} {{ $registration->last_name}}  ( Referance : {{$registration->referanceBy->first_name}} {{ $registration->referanceBy->last_name }} )</option>
                                    @endif
                                @else
                                    @if(isset($payment))
                                        @if($payment->registration_id == $registration->id )
                                            <option value="{{ $registration->id}}" selected>{{ $registration->first_name}} {{ $registration->last_name}}</option>
                                        @else
                                            <option value="{{ $registration->id}}">{{ $registration->first_name}} {{ $registration->last_name}}</option>
                                        @endif
                                    @else
                                        <option value="{{ $registration->id}}">{{ $registration->first_name}} {{ $registration->last_name}}</option>
                                    @endif
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                {!!
                    Former::select('package_id')->label('Tour')
                    ->data_placeholder('Select Tour')
                    ->addClass('select')
                !!}

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!!
                 Former::select('account_id')->label('account')
                    ->data_placeholder('Select Account')
                    ->fromQuery($accounts,'bank_name','id')
                    ->addClass('select')
                 !!}
            </div>
            <div class="col-md-6">
                {!!
                 Former::text('amount')
                 !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!!
                Former::select('mode')
                ->addOption(['0' => 'Cash','1' => 'Cheque','2' => 'Bank Transfer'])
                ->addClass('select')
                !!}
            </div>
            
        </div>
        <div class="row" id="cheque" hidden>
            <div class="col-md-6">
                {!!
                 Former::text('cheque_bank')->label('Cheque Bank*')
                !!}
                <span class="float_right" id="error-bankName" style="display: none;color: red">Bank Name is required</span>
            </div>
            <div class="col-md-6">
                {!!
                 Former::text('cheque_branch')->label('Cheque Branch*')
                !!}
                <span class="float_right" id="error-bankBranch" style="display: none;color: red">Bank Branch is required</span>
            </div>
        </div>
        <div class="row" id="cheque1" hidden>
            <div class="col-md-6">
                {!!
                 Former::text('cheque_date')->label('Cheque Date*')
                 !!}
                <span class="float_right" id="error-chequeDate" style="display: none;color: red">Cheque Date is required</span>
            </div>
            <div class="col-md-6">
                {!!
                Former::text('cheque_number')->label('Cheque Number*')
                 !!}
                <span class="float_right" id="error-cheque_number" style="display: none;color: red">Cheque Number is required</span>
            </div>
        </div>
        <div class="class" id="net_transfer" hidden>
            <div class="col-md-6">
                {!!
                 Former::text('reference_code')->label('Reference Id*')
                 !!}
                <label class="float_right" id="error-reference_code" style="display: none;color: red">Referance Id is required</label>
            </div>
            <div class="col-md-6">
                {!!
                 Former::text('transfer_from')
                 !!}
            </div>
        </div>
        <div class="class" id="net_transfer1" hidden>
            <div class="col-md-6">
                {!!
                 Former::text('transfer_to')
                 !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!!
                 Former::textarea('description')
                 !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                @if(isset($payment) && isset($files))
                    {!!
                    Former::file('attachment_id')->label('attachment')->route('admin.filemanager.file.getFile'  )->help( link_to_route('admin.filemanager.file.getFile',$files->filename , [$files->id])  )
                    !!}
                @else
                    {!!
                   Former::file('attachment_id')->label('attachment')
                   !!}
                @endif
            </div>
        </div>
    </div>
