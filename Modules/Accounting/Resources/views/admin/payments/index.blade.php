@extends('layouts.master')

@section('content-header')

@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4>
                        {{ trans('accounting::payments.title.payments') }}
                    <a href="{{ route('admin.accounting.payment.create') }}" class="btn btn-primary pull-right btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('accounting::payments.button.create payment') }}
                    </a>
                    </h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ trans('accounting::payments.table.date') }}</th>
                                <th>{{ trans('accounting::payments.table.package') }}</th>
                                <th>{{ trans('accounting::payments.table.register_client') }}</th>
                                <th>{{ trans('accounting::payments.table.amount') }}</th>
                                <th>{{ trans('accounting::payments.table.mode') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($payments)): ?>
                            <?php foreach ($payments as $payment): ?>
                            <tr>
                                <td>{{isset($payment->date)?$payment->date:''}}</td>
                                <td>{{isset($payment->packageDetails)?$payment->packageDetails['package']:''}}</td>
                                <td>{{isset($payment->registration)?$payment->registration['first_name']:''}}</td>
                                <td>{{$payment->amount}}</td>
                                @if($payment->mode == 0)
                                    <td><a>Cash</a></td>
                                @elseif ($payment->mode == 1)
                                    <td><a href="#" onclick="getModeDetails({{$payment->mode}},{{$payment->id}},'payment' )">Cheque</a></td>
                                @else
                                    <td><a href="#" onclick="getModeDetails({{$payment->mode}},{{$payment->id}},'payment')">Bank Transfer</a></td>
                                @endif
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.accounting.payment.edit', [$payment->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.accounting.payment.destroy', [$payment->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ trans('accounting::payments.table.date') }}</th>
                                <th>{{ trans('accounting::payments.table.package') }}</th>
                                <th>{{ trans('accounting::payments.table.register_client') }}</th>
                                <th>{{ trans('accounting::payments.table.amount') }}</th>
                                <th>{{ trans('accounting::payments.table.mode') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('accounting::payments.title.create payment') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.accounting.payment.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": false,
                "info": true,
                "autoWidth": true,
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });

        function getModeDetails($modeType, $expenseId,$category) {

                $.ajax({
                type: 'GET',
                url: '{{ URL::route('getPaymentDetailsModal')}}',
                data: {
                    category:$category,
                    Id:$expenseId,
                    modeType: $modeType,
                    _token: $('meta[name="token"]').attr('value'),
                },
                success: function (response) {

                    $('#commonModal').modal('show');
                    $('#commonModal').html(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Oops.", "Something went wrong, Please try again", "error");
                }
            });
            
        }
    </script>
@endpush
