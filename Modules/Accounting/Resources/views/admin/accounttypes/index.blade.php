@extends('layouts.master')

@section('content-header')
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div id="create" class="col-xs-12" hidden>
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create Accounttype</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        {!! Form::open(['route' => ['admin.accounting.accounttype.store'], 'method' => 'post' , 'id'=>'create-form']) !!}
                        <div class="box-body">
                            <div class="form-group has-feedback {{ $errors->has('type') ? ' has-error has-feedback' : '' }}">
                                <label for="type-name">Type</label>
                                <input type="text" class="form-control" id="type" name="type" autofocus
                                       placeholder="Enter Type" value="{{ old('type') }}">
                                {!! $errors->first('type', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button class="btn btn-primary" id="btn-cancel" style="margin-left: 35%"><i class="fa fa-times"></i>Cancel</button>
                            <button type="submit" class="btn btn-primary" style="margin-left: 10%"><i class="fa fa-floppy-o"></i>Save</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4>
                        {{ trans('accounting::accounttypes.title.accounttypes') }}
                        <a id="accounttype" class="btn btn-primary pull-right btn-flat" style="padding: 4px 10px;">
                            <i class="fa fa-pencil"></i> {{ trans('accounting::accounttypes.button.create accounttype') }}
                        </a>
                    </h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Type</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($accounttypes)): ?>
                            <?php foreach ($accounttypes as $accounttype): ?>
                            <tr>
                                <td>{{$accounttype->type}}</td>
                                <td>
                                    <a href="{{ route('admin.accounting.accounttype.edit', [$accounttype->id]) }}">
                                        {{ $accounttype->created_at }}
                                    </a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-flat category-edit-button"
                                        data-type="{{$accounttype->type}}"
                                           data-id="{{$accounttype->id}}"
                                        ><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.accounting.accounttype.destroy', [$accounttype->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Type</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div id="update-div" class="box box-primary" hidden>
                <div class="box-header with-border">
                    <h3 class="box-title">Update Accounttype</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                {!! Form::open(['route' => ['admin.accounting.accounttype.update'], 'method' => 'post','id'=>'update-form']) !!}
                <div class="box-body">
                    <div class="form-group has-feedback {{ $errors->has('type') ? ' has-error has-feedback' : '' }}">
                        <label for="type-name">Type</label>
                        <input type="text" class="form-control" id="type" name="type" autofocus placeholder="Enter Type" value="{{ old('type') }}">
                        {!! $errors->first('type', '<span class="help-block">:message</span>') !!}
                    </div>
                    <input type="hidden" name="accounttype_id" id="accounttype-id">
                    <input type="hidden" name="old_name" id="old-name">
                </div>

                <!-- /.box-body -->

                <div class="box-footer">
                    <button class="btn btn-primary" id="btn-cancel-update" style="margin-left: 35%"><i class="fa fa-times"></i>Cancel</button>
                    <button type="submit" class="btn btn-primary" style="margin-left: 10%"><i class="fa fa-floppy-o"></i>Save</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('accounting::accounttypes.title.create accounttype') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.accounting.accounttype.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
        $("#accounttype").click(function (event) {
            event.preventDefault();
            $("#create").show();
            $("#accounttype").hide();
        });
        $("#btn-cancel").click(function (event) {
            event.preventDefault();
            $("#accounttype").show();
            $("#create").hide();
        })
        $(".category-edit-button").click(function () {

            $("#accounttype-list").hide();
            $("#accounttype").hide();
            $("#accounttype-id").val($(this).data("id"));
            $("#old-name").val($(this).data("type"));

            $("#update-form").find('input[name="type"]').val($(this).data("type"));
            $("#update-div").show();
        });
        $("#btn-cancel-update").click(function (event) {
            event.preventDefault();
            $("#update-div").hide();
            $("#accounttype").show();
            $("#accounttype-list").show();
        })
    </script>
@endpush
