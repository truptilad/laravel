@extends('layouts.master')

@section('content-header')
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div id="create" class="col-xs-12" hidden>
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create Account</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        {!! Form::open(['route' => ['admin.accounting.account.store'], 'method' => 'post','id'=>'create-form']) !!}
                        <div class="box-body">
                            <div class="form-group has-feedback {{ $errors->has('company_id') ? ' has-error has-feedback' : '' }}">
                                <label for="company">Company</label>
                                <select class="company dropdown" id="company-id" name="company_id">
                                    @foreach($companies as $company)
                                        <option value="{{ $company->id }}">{{ $company->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('branch_id') ? ' has-error has-feedback' : '' }}">
                                <label for="branch">Branch</label>
                                <select class="branch dropdown" id="branch-id" name="branch_id">
                                    @foreach($branchs as $branch)
                                        <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('name') ? ' has-error has-feedback' : '' }}">
                                <label for="type-name">Account Holder Name</label>
                                <input type="text" class="form-control" id="name" name="name" autofocus placeholder="Enter Account Holder Name" value="{{ old('name') }}">
                                {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('bank_name') ? ' has-error has-feedback' : '' }}">
                                <label for="bank-name">Bank Name</label>
                                <input type="text" class="form-control" id="bank-name" name="bank_name" autofocus placeholder="Enter Bank Name" value="{{ old('bank_name') }}">
                                {!! $errors->first('bank_name', '<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('ifsc_code') ? ' has-error has-feedback' : '' }}">
                                <label for="type-ifsc">IFSC</label>
                                <input type="text" class="form-control" id="ifsc-code" name="ifsc_code" autofocus placeholder="Enter IFSC Code" value="{{ old('ifsc') }}">
                                {!! $errors->first('ifsc_code', '<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('acc_number') ? ' has-error has-feedback' : '' }}">
                                <label for="acc_number">A/c Number</label>
                                <input type="text" class="form-control" id="acc-number" name="acc_number" autofocus placeholder="Enter Account Number" value="{{ old('acc_number') }}">
                                {!! $errors->first('acc_number', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button class="btn btn-primary" id="btn-cancel" style="margin-left: 35%"><i class="fa fa-times"></i>Cancel</button>
                            <button type="submit" class="btn btn-primary" style="margin-left: 10%"><i class="fa fa-floppy-o"></i>Save</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <div id="account-list" class="box box-primary">
                <div class="box-header with-border">
                    <h4>
                        {{ trans('accounting::accounts.title.accounts') }}
                        <a id="account" class="btn btn-primary pull-right btn-flat" style="padding: 4px 10px;">
                            <i class="fa fa-pencil"></i> {{ trans('accounting::accounts.button.create account') }}
                        </a>
                    </h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Company</th>
                                <th>Branch</th>
                                <th>Bank Name</th>
                                <th>A/c Holder Name</th>
                                <th>A/c Number</th>
                                <th>IFSC Code</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (isset($accounts))
                            @foreach ($accounts as $account)
                            <tr>
                                <td>{{isset($account['company'])?$account['company']['name']:'NA' }}</td>
                                <td>{{isset($account['branch'])?$account['branch']['name']:'NA'}}</td>
                                <td>{{$account['bank_name']}}</td>
                                <td>{{$account['name']}}</td>
                                <td>{{$account['acc_number']}}</td>
                                <td>{{$account['ifsc_code']}}</td>
                                <td>
                                    <a href="{{ route('admin.accounting.account.edit', [$account->id]) }}">
                                        {{ $account['created_at'] }}
                                    </a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-flat category-edit-button" data-acc_number="{{$account->acc_number}}" data-ifsc_code="{{$account->ifsc_code}}" data-bank_name="{{$account->bank_name}}" data-name="{{$account->name}}" data-company_id="{{$account->company_id}}" data-branch_id="{{$account->branch_id}}" data-id="{{$account->id}}"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.accounting.account.destroy', [$account->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Company</th>
                                <th>Branch</th>
                                <th>Bank Name</th>
                                <th>A/c Holder Name</th>
                                <th>A/c Number</th>
                                <th>IFSC Code</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div id="update-div" class="box box-primary" hidden>
                <div class="box-header with-border">
                    <h3 class="box-title">Update Account Type</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                {!! Form::open(['route' => ['admin.accounting.account.update'], 'method' => 'post','id'=>'update-form']) !!}
                <div class="box-body">
                    <div class="form-group has-feedback {{ $errors->has('company_id') ? ' has-error has-feedback' : '' }}">
                        <label for="company">Company</label>
                        <select class="company dropdown" id="company-id" name="company_id">
                                @foreach($companies as $company)
                                    <option value="{{ $company->id }}">{{ $company->name }}</option>
                                @endforeach
                        </select>
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('branch_id') ? ' has-error has-feedback' : '' }}">
                        <label for="branch">Branch</label>
                        <select class="branch dropdown" id="branch-id" name="branch_id">
                            @foreach($branchs as $branch)
                                <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('name') ? ' has-error has-feedback' : '' }}">
                        <label for="type-name">Account Holder Name</label>
                        <input type="text" class="form-control" id="name" name="name" autofocus placeholder="Enter Account Holder Name" value="{{ old('name') }}">
                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('bank_name') ? ' has-error has-feedback' : '' }}">
                        <label for="bank-name">Bank Name</label>
                        <input type="text" class="form-control" id="bank-name" name="bank_name" autofocus placeholder="Enter Bank Name" value="{{ old('bank_name') }}">
                        {!! $errors->first('bank_name', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('ifsc_code') ? ' has-error has-feedback' : '' }}">
                        <label for="type-ifsc">IFSC</label>
                        <input type="text" class="form-control" id="ifsc-code" name="ifsc_code" autofocus placeholder="Enter IFSC Code" value="{{ old('ifsc') }}">
                        {!! $errors->first('ifsc_code', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('acc_number') ? ' has-error has-feedback' : '' }}">
                        <label for="acc_number">A/c Number</label>
                        <input type="text" class="form-control" id="acc-number" name="acc_number" autofocus placeholder="Enter Account Number" value="{{ old('acc_number') }}">
                        {!! $errors->first('acc_number', '<span class="help-block">:message</span>') !!}
                    </div>
                    <input type="hidden" name="account_id" id="account-id">
                    <input type="hidden" name="old_name" id="old-name">
                </div>

                <!-- /.box-body -->

                <div class="box-footer">
                    <button class="btn btn-primary" id="btn-cancel-update" style="margin-left: 35%"><i class="fa fa-times"></i>Cancel</button>
                    <button type="submit" class="btn btn-primary" style="margin-left: 10%"><i class="fa fa-floppy-o"></i>Save</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('accounting::accounts.title.create account') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.accounting.account.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });

        $('.company').select2({
            width: '100%'
        });

        $('.branch').select2({
            width: '100%'
        });

        $("#account").click(function (event) {
            event.preventDefault();
            $("#create").show();
            $("#account").hide();
        });
        $("#btn-cancel").click(function (event) {
            event.preventDefault();
            $("#account").show();
            $("#create").hide();
        })
        $(".category-edit-button").click(function () {

            $("#account-list").hide();
            $("#account").hide();
            $("#account-id").val($(this).data("id"));
            $("#old-name").val($(this).data("name"));
            $("#update-form").find('input[name="name"]').val($(this).data("name"));
            $("#update-form").find('select[name="company_id"]').val($(this).data("company_id")).trigger('change','select');
            $("#update-form").find('select[name="branch_id"]').val($(this).data("branch_id")).trigger('change','select');;
            $("#update-form").find('input[name="bank_name"]').val($(this).data("bank_name"));
            $("#update-form").find('input[name="acc_number"]').val($(this).data("acc_number"));
            $("#update-form").find('input[name="ifsc_code"]').val($(this).data("ifsc_code"));
            $("#update-form").find('select[name="type_id"]').val($(this).data("type_id")).trigger('change','select');
            $("#update-form").find('input[name="description"]').val($(this).data("description"));
            $("#update-div").show();
        });
        $("#btn-cancel-update").click(function (event) {
            event.preventDefault();
            $("#update-div").hide();
            $("#account").show();
            $("#account-list").show();
        });
        $('.type').select2({
            width : '100%'
        });
    </script>
@endpush
