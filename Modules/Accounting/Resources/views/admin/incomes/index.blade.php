@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('accounting::incomes.title.incomes') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('accounting::incomes.title.incomes') }}</li>
    </ol>
@stop

@section('content')
<?php use Carbon\Carbon; ?> 
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.accounting.income.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('accounting::incomes.button.create income') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>{{ trans('accounting::incomes.table.date') }}</th>
                                    <th>{{ trans('accounting::incomes.table.income_source') }}</th>
                                    <th>{{ trans('accounting::incomes.table.package') }}</th>
                                    <th>{{ trans('accounting::incomes.table.amount') }}</th>
                                    <th>{{ trans('accounting::incomes.table.mode') }}</th>
                                    <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($incomes)): ?>
                            <?php foreach ($incomes as $income): ?>
                            <tr>
                                <td>{{isset($income->date)?carbon::parse($income->date)->format(PHP_DATE_FORMAT):''}}</td>
                                <td>{{$income->income_source_name}}</td>
                                <td>{{isset($income->packageDetail)?$income->packageDetail->package : ' '}}</td>
                                <td>{{$income->amount}}</td>
                                @if($income->payment_mode == 0)
                                    <td><a>Cash</a></td>
                                @elseif ($income->payment_mode == 1)
                                    <td><a href="#" onclick="getModeDetails({{$income->payment_mode}},{{$income->id}},'loan')">Cheque</a></td>
                                @else
                                    <td><a href="#" onclick="getModeDetails({{$income->payment_mode}},{{$income->id}},'loan')">Bank Transfer</a></td>
                                @endif

                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.accounting.income.edit', [$income->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.accounting.income.destroy', [$income->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ trans('accounting::incomes.table.date') }}</th>
                                <th>{{ trans('accounting::incomes.table.income_source') }}</th>
                                <th>{{ trans('accounting::incomes.table.package') }}</th>
                                <th>{{ trans('accounting::incomes.table.amount') }}</th>
                                <th>{{ trans('accounting::incomes.table.mode') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('accounting::incomes.title.create income') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.accounting.income.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": false,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });

        function getModeDetails($modeType, $expenseId,$category) {

                $.ajax({
                type: 'GET',
                url: '{{ URL::route('getPaymentDetailsModal')}}',
                data: {
                    category:$category,
                    Id:$expenseId,
                    modeType: $modeType,
                    _token: $('meta[name="token"]').attr('value'),
                },
                success: function (response) {

                    $('#commonModal').modal('show');
                    $('#commonModal').html(response);


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Oops.", "Something went wrong, Please try again", "error");
                }
            });
            
        }
    </script>
@endpush
