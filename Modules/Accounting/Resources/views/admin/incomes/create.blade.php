@extends('layouts.master')

@section('content-header')

@stop
@section('styles')
    {!! Theme::style('vendor/datetimepicker/jquery.datetimepicker.min.css') !!}
@stop

@section('content')

    
    {!! Former::open_for_files()
       ->route('admin.accounting.income.store')
       ->method('POST')
   !!}

    {{ csrf_field() }}

     <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="nav-tabs-custom">
                    <div class="box-header with-border">
                        <h4>
                            {{ trans('accounting::incomes.title.create income') }}
                        </h4>
                        @include('partials.form-tab-headers')
                    </div>
                    <div class="box-body">
                        @include('accounting::admin.incomes.partials.create-fields')
                    </div>
                    <div class="box-footer">
                        <a class="btn btn-danger btn-flat" href="{{ route('admin.accounting.income.index')}}"
                           style="margin-left: 35%"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}
                        </a>
                        <button type="submit" class="btn btn-primary btn-flat" style="margin-left: 10%"><i class="fa fa-floppy-o"></i>Save
                        </button>
                    </div>
                </div> {{-- end nav-tabs-custom --}}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    {!! Theme::script('vendor/datetimepicker/jquery.datetimepicker.full.min.js') !!}
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.accounting.income.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            $('#payment_mode,#account_id,#status,#subcategory_id').select2({
                placholder: $(this).data('placeholder')
            });


            $("#date,#cheque_date").datetimepicker({
                format:'{{PHP_DATE_FORMAT}}',
                value:new Date(),
                timepicker:false
            });

            $('#package_id').select2();

            var $category = $('#category_id');
            $category.select2().on('change',function () {
                $.ajax({
                    type: 'GET',
                    url: '{{URL::route('admin.accounting.expensecategory.subcategory')}}',
                    data: {
                        id: $category.val(),
                        _token: $('meta[name="token"]').attr('value'),
                    },
                    success: function (response) {
                        $.each(response,function (i,item) {
                            $.each(item ,function (i,item) {
                                $('#subcategory_id').append('<option value='+item.id+'>'+item.name+'<option>');
                            })
                        })
                    },
                    error: function (xhr, ajaxOption, thrownError) {
                        swal("Oops.", "Something went wrong, Please try again", "error");
                    }
                });
            });

            $("#payment_mode").change(function () {
                if ($('#payment_mode').val() == 1) {
                    $("#cheque").show();
                    $("#cheque1").show();
                    $("#net_transfer").hide();
                    $("#net_transfer1").hide();
                }
                else if($('#payment_mode').val() == 2) {
                    
                    $("#net_transfer").show();
                    $("#net_transfer1").show();
                    $("#cheque").hide();
                    $("#cheque1").hide();
                }
                else{
                    $("#cheque").hide();
                    $("#cheque1").hide();
                    $("#net_transfer").hide();
                    $("#net_transfer1").hide();
                }
            });

            $('form').on('submit', function() {

                if ($('#payment_mode').val() == 1){

                    if($('#cheque_bank').val() == ''){

                        $('#error-bankName').show();
                        $('#error-bankBranch').hide();
                        $('#error-chequeDate').hide();
                        $('#error-cheque_number').hide();
                        return false;
                    }
                    else if($('#cheque_date').val() == ''){

                        $('#error-chequeDate').show();
                        $('#error-bankName').hide();
                        $('#error-bankBranch').hide();
                        $('#error-cheque_number').hide();
                        return false;
                    }
                    else if($('#cheque_branch').val() == ''){
                        $('#error-bankBranch').show();
                        $('#error-bankName').hide();
                        $('#error-chequeDate').hide();
                        $('#error-cheque_number').hide();
                        return false;   
                    }
                    else if($('#cheque_number').val() == ''){
                        $('#error-cheque_number').show();
                        $('#error-bankBranch').hide();
                        $('#error-bankName').hide();
                        $('#error-chequeDate').hide();
                        return false;   
                    }
                    else{
                        $('#error-chequeDate').hide();
                        $('#error-bankName').hide();
                        $('#error-bankBranch').hide();
                        $('#error-cheque_number').hide();   
                        
                        return true;
                    }
                }
                else if ($('#payment_mode').val() == 2){

                    if ($('#reference_code').val() == '' ){
                        
                        $('#error-reference_code').show();
                        return false;
                    }
                    else{
                        
                        $('#error-reference_code').hide();
                        return true;   
                    }

                }
                else{
                    return true;
                }
            })

        });
    </script>
@endpush
