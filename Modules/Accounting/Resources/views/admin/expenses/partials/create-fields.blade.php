 <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                {!!
                 Former::text('date')
                 !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!!
                Former::select('package_id')->label('Tour')
                ->addOption(null)
                ->data_placeholder('Select Tour')
                ->fromQuery($packages,'name','id')
                ->addClass('select')
                !!}
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="category_id" class="control-label col-lg-3 col-sm-4">Category</label>
                
                    <div class="col-lg-9 col-sm-8">
                        <select id='category_id'  name="category_id" placeholder="Select Category" style="width: 100%">
                            @foreach ($expensecategory as $category)
                                <optgroup label='{{ $category->name }}'>
                                    @if(isset($category->childexpenseCategory))   
                                        @foreach($category->childexpenseCategory as $child)
                                            <option selected=""></option>
                                            @if(isset($expense))
                                                @if($expense->category_id == $child->id )
                                                    <option value="{{ $child->id }}" selected>{{$child->name}}</option>
                                                @else
                                                    <option value='{{ $child->id }}' >{{$child->name }}</option>
                                                @endif
                                            @else
                                                <option value='{{ $child->id }}' >{{$child->name }}</option>
                                            @endif
                                        @endforeach
                                    @else
                                        <option selected=""></option>
                                        @if($expense->category_id == $category->id )
                                            <option value='{{ $child->id }}' selected>{{$child->name }}</option>
                                        @else
                                            <option value='{{ $child->id }}' >{{$child->name }}</option>
                                        @endif
                                    @endif
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                {!!
                 Former::select('account_id')->label('account')
                 ->fromQuery($accounts,'bank_name','id')
                 ->addClass('select')
                !!}
            </div>
            <div class="col-md-6">
                {!!
                 Former::text('amount')
                 !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!!
                 Former::select('payment_mode')->label('mode')
                 ->data_placeholder('Select Payment Mode')
                 ->addOption(['0' => 'Cash','1' => 'Cheque','2' => 'Bank Transfer' ])
                 ->addClass('select')
                 !!}
            </div>
            <div class="col-md-6">
                {!!
                 Former::select('status')->label('status')
               ->data_placeholder('Select Status')
               ->addOption(['0' => 'Paid' , '1' => 'Pending'])
               ->addClass('select')
                !!}
            </div>
        </div>
        <div class="row" id="cheque" hidden>
            <div class="col-md-6">
                {!!
                 Former::text('cheque_bank')->label('Cheque Bank*')
                 !!}
                <span class="float_right" id="error-bankName" style="display: none;color: red">Bank Name is required</span>
            </div>
            <div class="col-md-6">
                {!!
                 Former::text('cheque_branch')->label('Cheque Branch*')
                 !!}
                <span class="float_right" id="error-bankBranch" style="display: none;color: red">Bank Branch is required</span>
            </div>
        </div>
        <div class="row" id="cheque1" hidden>
            <div class="col-md-6">
                {!!
                 Former::text('cheque_date')->label('Cheque Date*')
                 !!}
                <span class="float_right" id="error-chequeDate" style="display: none;color: red">Cheque Date is required</span>
            </div>
            <div class="col-md-6">
                {!!
                 Former::text('cheque_number')->label('Cheque Number*')
                 !!}
                 <span class="float_right" id="error-cheque_number" style="display: none;color: red">Cheque Number is required</span>
            </div>
        </div>
        <div class="row" id="net_transfer" hidden>
            <div class="col-md-6">
                {!!
                 Former::text('reference_code')->label('Reference Id*')
                 !!}
                <label class="float_right" id="error-reference_code" style="display: none;color: red">Referance Id is required</label>
            </div>
            <div class="col-md-6">
                {!!
                 Former::text('transfer_from')
                 !!}
            </div>
        </div>
        <div class="class" id="net_transfer1" hidden>
            <div class="col-md-6">
                {!!
                 Former::text('transfer_to')
                 !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                @if(isset($expense->attachment_id))
                {!!
                Former::file('attachment_id')->label('attachment')->route('admin.filemanager.file.getFile'  )->help( link_to_route('admin.filemanager.file.getFile',$files->filename , [$files->id])  )
                !!}
                 @else
                    {!!
                   Former::file('attachment_id')->label('attachment')
                   !!}
                @endif
       </div>
            
        </div>
    </div>
