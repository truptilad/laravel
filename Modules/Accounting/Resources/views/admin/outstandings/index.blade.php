@extends('layouts.master')

@section('content-header')
@stop

@section('styles')
    {!! Theme::style('vendor/datetimepicker/jquery.datetimepicker.min.css') !!}
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h2>
                        {{ trans('accounting::outstandings.title.outstandings') }}
                    </h2>
                        <!-- <a href="{{ URL::to('downloadExcel/xls') }}"><button class="btn btn-success">Download Excel xls</button></a>
                        <a href="{{ URL::to('downloadExcel/xlsx') }}"><button class="btn btn-success">Download Excel xlsx</button></a> -->
                    <!-- <a href="{{ URL::route('downloadExcel','csv') }}"><button class="btn btn-info pull-right">Download CSV</button></a> -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ trans('accounting::outstandings.table.name') }}</th>
                                <th>{{ trans('accounting::outstandings.table.reference_by') }}</th>
                                <th>{{ trans('accounting::outstandings.table.package') }}</th>
                                <th>{{ trans('accounting::outstandings.table.amount') }} <i class="fa fa-inr"></i></th>
                                <th>{{ trans('accounting::outstandings.table.recieved_amount') }} <i class="fa fa-inr"></i></th>
                                <th>{{ trans('accounting::outstandings.table.outstanging') }} <i class="fa fa-inr"></i></th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($outstandingRegs)): ?>
                            <?php foreach ($outstandingRegs as $outstandingRegs): ?>
                            <tr>
                                <td>
                                    {{ $outstandingRegs->first_name }} {{ $outstandingRegs->last_name }}
                                </td>
                                <td>
                                    {{ isset($outstandingRegs->referanceBy)?$outstandingRegs->referanceBy->first_name :'Self' }} {{ isset($outstandingRegs->referanceBy)?$outstandingRegs->referanceBy->last_name :'' }}
                                </td>
                                <td>
                                    {{ isset($outstandingRegs->packageInfo)?$outstandingRegs->packageInfo->package:'NA' }}
                                </td>
                                <td>
                                    {{ isset($outstandingRegs->amount)?$outstandingRegs->amount:'-' }}
                                </td>
                                <td>
                                    {{ isset($outstandingRegs->paid)?$outstandingRegs->paid:'-' }}
                                </td>
                                <td>
                                    <?php $outstandingAmount = $outstandingRegs->amount - $outstandingRegs->paid; ?>
                                    {{ $outstandingAmount }}
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-success btn-sm" onclick="getModal({{$outstandingRegs->id}})"><i class="fa fa-credit-card"></i> Make Payment </a>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ trans('accounting::outstandings.table.name') }}</th>
                                <th>{{ trans('accounting::outstandings.table.reference_by') }}</th>
                                <th>{{ trans('accounting::outstandings.table.package') }}</th>
                                <th>{{ trans('accounting::outstandings.table.amount') }} <i class="fa fa-inr"></i></th>
                                <th>{{ trans('accounting::outstandings.table.recieved_amount') }} <i class="fa fa-inr"></i></th>
                                <th>{{ trans('accounting::outstandings.table.outstanging') }} <i class="fa fa-inr"></i></th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')

@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('accounting::outstandings.title.create outstanding') }}</dd>
    </dl>
@stop

@push('js-stack')
{!! Theme::script('vendor/datetimepicker/jquery.datetimepicker.full.min.js') !!}
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.accounting.outstanding.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });

        function getModal($outstandingId) {

                $.ajax({
                type: 'GET',
                url: '{{ URL::route('getModal')}}',
                data: {
                    id: $outstandingId,
                    _token: $('meta[name="token"]').attr('value'),
                },
                success: function (response) {

                    $('#commonModal').modal('show');
                    $('#commonModal').html(response);


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Oops.", "Something went wrong, Please try again", "error");
                }
            });
        }

        $(document).ready(function() {
            $('#mode').select2({
                placeholder:'Select Mode'
            });
        })

    </script>
    <!-- @include('partials.common-modal') -->
@endpush
