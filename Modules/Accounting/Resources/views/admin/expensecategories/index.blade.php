@extends('layouts.master')

@section('content-header')
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div id="create" class="col-xs-12" hidden>
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create ExpenseCategory</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        {!! Form::open(['route' => ['admin.accounting.expensecategory.store'], 'method' => 'post','id'=>'create-form']) !!}
                        <div class="box-body">
                            <div class="form-group has-feedback {{ $errors->has('name') ? ' has-error has-feedback' : '' }}">
                                <label for="type-name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" autofocus placeholder="Enter Name" value="{{ old('name') }}">
                                {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('type_id') ? ' has-error has-feedback' : '' }}">
                                <label for="type-name">Type</label>
                                <select class="type dropdown" id="type" name="type_id">
                                    <option></option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">
                                            {{$category->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('description') ? ' has-error has-feedback' : '' }}">
                                <label for="type-name">Description</label>
                                <input type="text" class="form-control" id="description" name="description" autofocus placeholder="Enter Description" value="{{ old('description') }}">
                                {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button class="btn btn-primary" id="btn-cancel" style="margin-left: 35%"><i class="fa fa-times"></i>Cancel</button>
                            <button type="submit" class="btn btn-primary" style="margin-left: 10%"><i class="fa fa-floppy-o"></i>Save</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <div id="expensecategory-list" class="box box-primary">
                <div class="box-header with-border">
                    <h4>
                        {{ trans('accounting::expensecategories.title.expensecategories') }}
                        <a id="expensecategory" class="btn btn-primary pull-right btn-flat" style="padding: 4px 10px;">
                            <i class="fa fa-pencil"></i> {{ trans('accounting::expensecategories.button.create expensecategory') }}
                        </a>
                    </h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Description</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($expensecategories)): ?>
                            <?php foreach ($expensecategories as $expensecategory): ?>
                            <tr>
                                <td>{{$expensecategory->name}}</td>
                                <td>{{isset($expensecategory->expenseCategorytype)?$expensecategory->expenseCategorytype->name:'-'}}</td>
                                <td>{{$expensecategory->description}}</td>
                                <td>
                                    <a href="{{ route('admin.accounting.expensecategory.edit', [$expensecategory->id]) }}">
                                        {{ $expensecategory->created_at }}
                                    </a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-flat category-edit-button"
                                        data-name="{{$expensecategory->name}}"
                                        data-type_id = "{{$expensecategory->type_id}}"
                                        data-description = "{{$expensecategory->description}}"
                                        data-id="{{$expensecategory->id}}"
                                        ><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.accounting.expensecategory.destroy', [$expensecategory->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Description</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div id="update-div" class="box box-primary" hidden>
                <div class="box-header with-border">
                    <h3 class="box-title">Update ExpenseCategory</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                {!! Form::open(['route' => ['admin.accounting.expensecategory.update'], 'method' => 'post','id'=>'update-form']) !!}
                <div class="box-body">
                    <div class="form-group has-feedback {{ $errors->has('name') ? ' has-error has-feedback' : '' }}">
                        <label for="type-name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" autofocus placeholder="Enter Name" value="{{ old('name') }}">
                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('type_id') ? ' has-error has-feedback' : '' }}">
                        <label for="type-name">Type</label>
                        <select class="type dropdown" id="type" name="type_id">
                            <option></option>
                            @foreach($categories as $category)
                            <option value="{{$category->id}}">
                            {{$category->name}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('description') ? ' has-error has-feedback' : '' }}">
                        <label for="type-name">Description</label>
                        <input type="text" class="form-control" id="description" name="description" autofocus placeholder="Enter Description" value="{{ old('description') }}">
                        {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
                    </div>
                    <input type="hidden" name="category_id" id="category-id">
                    <input type="hidden" name="old_name" id="old-name">
                </div>

                <!-- /.box-body -->

                <div class="box-footer">
                    <button class="btn btn-primary" id="btn-cancel-update" style="margin-left: 35%"><i class="fa fa-times"></i>Cancel</button>
                    <button type="submit" class="btn btn-primary" style="margin-left: 10%"><i class="fa fa-floppy-o"></i>Save</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('accounting::expensecategories.title.create expensecategory') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.accounting.expensecategory.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
        $("#expensecategory").click(function (event) {
            event.preventDefault();
            $("#create").show();
            $("#expensecategory").hide();
        });
        $("#btn-cancel").click(function (event) {
            event.preventDefault();
            $("#expensecategory").show();
            $("#create").hide();
        })
        $(".category-edit-button").click(function () {

            $("#expensecategory-list").hide();
            $("#expensecategory").hide();
            $("#category-id").val($(this).data("id"));
            $("#old-name").val($(this).data("name"));
            $("#update-form").find('input[name="name"]').val($(this).data("name"));
            $("#update-form").find('select[name="type_id"]').val($(this).data("type_id")).trigger('change','select');
            $("#update-form").find('input[name="description"]').val($(this).data("description"));
            $("#update-div").show();
        });
        $("#btn-cancel-update").click(function (event) {
            event.preventDefault();
            $("#update-div").hide();
            $("#expensecategory").show();
            $("#expensecategory-list").show();
        });
        $('.type').select2({
            width : '100%'
        });
    </script>
@endpush
