@extends('layouts.master')

@section('content-header')

@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">

            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4>
                        {{ trans('accounting::invoices.title.invoices') }}
                        <a href="{{ route('admin.accounting.invoice.create') }}" class="btn btn-primary pull-right btn-flat" style="padding: 4px 10px;">
                            <i class="fa fa-pencil"></i> {{ trans('accounting::invoices.button.create invoice') }}
                        </a>
                    </h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Date</th>
                                <th>Due Date</th>
                                <th>Status</th>
                                <th>Amount</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($invoices)): ?>
                            <?php foreach ($invoices as $invoice): ?>
                            <tr>
                                <td>
                                    {{$invoice->invoice_no}}
                                </td>
                                <td>
                                    {{Utils::parseDate($invoice->date)}}
                                </td>
                                <td>
                                    {{Utils::parseDate($invoice->due_date)}}
                                </td>
                                <td>
                                    {{$invoice->status}}
                                </td>
                                <td>
                                    {{$invoice->amount}}
                                </td>
                                <td>
                                    <a href="{{ route('admin.accounting.invoice.edit', [$invoice->id]) }}">
                                        {{ $invoice->created_at }}
                                    </a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.accounting.invoice.edit', [$invoice->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <a href="{{ route('admin.accounting.invoice.view', [$invoice->id]) }}" class="btn btn-default btn-flat">View</a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.accounting.invoice.destroy', [$invoice->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('accounting::invoices.title.create invoice') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.accounting.invoice.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
