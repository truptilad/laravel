@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('accounting::invoices.title.edit invoice') }}
    </h1>

@stop

@section('content')
    {!!Former::populateField('invoice_no',$invoice->invoice_no)!!}
    {!! Former::populateField('date' , Utils::parseDate($invoice->date)) !!}
    {!! Former::populateField('due_date',Utils::parseDate($invoice->due_date)) !!}
    {!! Former::populateField('payment_terms',$invoice->payment_terms) !!}
    {!! Former::populateField('dispatch_number',$invoice->dispatch_number) !!}
    {!! Former::populateField('dispatch_through',$invoice->dispatch_through) !!}
    {!! Former::horizontal_open()
      ->route('admin.accounting.invoice.update',$invoice->id)
      ->method('put')
  !!}

    {{ csrf_field() }}


{{--    {!! Form::open(['route' => ['admin.accounting.invoice.update', $invoice->id], 'method' => 'put']) !!}--}}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                            @include('accounting::admin.invoices.partials.edit-fields')
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-left btn-flat" href="{{ route('admin.accounting.invoice.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.accounting.invoice.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
<script type="text/javascript">
    $("#date").datetimepicker({
        format:'{{PHP_DATE_FORMAT}}',
        value:'{{isset($invoice->date)?\Carbon\Carbon::parse($invoice->date)->format(PHP_DATE_FORMAT) : '' }}',
        timepicker:false
    });

    $("#due_date").datetimepicker({
        format:'{{PHP_DATE_FORMAT}}',
        value:'{{isset($invoice->due_date)?\Carbon\Carbon::parse($invoice->due_date)->format(PHP_DATE_FORMAT) : '' }}',
        timepicker:false
    });

</script>
@endpush
