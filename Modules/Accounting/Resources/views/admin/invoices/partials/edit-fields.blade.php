
<section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> VISION ENTERPRISES.
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">

            <!-- /.col -->
            <div class="col-md-4 invoice-col">
                To
                <address>
                    <strong>{{$invoice->client->client_name}}</strong><br>
                    {{$invoice->client->address}}<br>
                    {{isset($invoice->client->city)?$invoice->client->city->cityname():''}}<br>
                    State Code: {{isset($invoice->client->city)?$invoice->client->city->state->state_code:''}}<br>
                    Aadhar No: {{$invoice->client->aadhar_no}}<br>
                    Pan No: {{$invoice->client->pan_no}}<br>
                    GST No: {{$invoice->client->gst_no}}<br>
                    <i class="fa fa-phone" aria-hidden="true"> {{$invoice->client->contact_no}}</i><br>
                    <i class="fa fa-envelope" aria-hidden="true"> {{$invoice->client->email_id}}</i><br>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <label class="col-sm-5">Invoice No:</label>
                        <div class="col-sm-7"style="margin-bottom: 5px">
                                {!!
                             Former::text('invoice_no')->raw()
                             !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-sm-6">Mode/Payment Terms</label>
                        <div class="col-sm-6">
                            {!!
                             Former::text('payment_terms')->raw()
                             !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="col-sm-5">Invoice Date</label>
                        <div class="col-sm-7" style="margin-bottom: 5px">
                            {!!
                             Former::text('date')->raw()
                             !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-sm-6">Payment Due</label>
                        <div class="col-sm-6" style="margin-bottom: 5px">
                        {!!
                         Former::text('due_date')->raw()
                         !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="col-sm-5">Despatch No:</label>
                        <div class="col-sm-7">
                        {!!
                         Former::text('dispatch_number')->raw()
                         !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-sm-6">Despatch Through</label>
                        <div class="col-sm-6">
                        {!!
                         Former::text('dispatch_through')->raw()
                         !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="data-table table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>HSN Code</th>
                        <th>Quantity</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($invoice->orders as $order)
                        @foreach($order->orderproduct as $orderproduct)
                            <tr>
                                <td>{{$orderproduct->product->name}}</td>
                                <td>{{$orderproduct->product->hsn_code}}</td>
                                <td>{{$orderproduct->quantity}}</td>
                                <td>{{Utils::formatMoney($orderproduct->taxable_amount)}}</td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th rowspan="2" colspan="2"></th>
                        <th>Tax</th>
                        <th>
                            <span class="help-block" id="total" name="total">
                                {{Utils::formatMoney($invoice->tax)}}
                             </span>
                        </th>
                    </tr>
                    <tr>
                        <th>Total Amount</th>
                        <th>
                             <span class="help-block" id="total" name="total">
                                 {{Utils::formatMoney($invoice->amount)}}
                             </span>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
                <p class="lead">Payment Methods:</p>

                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                    1. Payment must be done after receiving product with in the 15 working days<br>
                    2. The Company reserves the right to alter the rates by mutual negotiations.<br>
                    3. Warranty on product is as per the terms and conditions of the  warranty document provided in the product by the Manufacturer/Vendor of the Product. The responsibility for performance of the terms and conditions of the warranty remains with the Manufacturer of the product.<br>
                    4. Any dispute relating to the order or in connection with these conditions of sale shall be deemed to have arisen in bhopal and all litigation there to shall be subject to Jurisdiction of Courts at bhopal only.
                </p>
            </div>
            <div class="col-xs-1">

            </div>
            <div class="col-xs-4">
                <p class="lead"> Account Detail:</p>
                <b>Bank Name:   </b>{{$invoice->branch->bank_name}}<br>
                <b>A/c Holder Name: </b>{{$invoice->branch->acc_holder_name}}<br>
                <b>A/c Number:  </b>{{$invoice->branch->acc_number}}<br>
                <b>IFSC Code:   </b>{{$invoice->branch->ifsc_code}}<br>
                <b>PAN No:  </b>{{$invoice->company->pan_no}}<br>
                <b>GST No:  </b>{{$invoice->company->gst_no}}
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
                </button>
                <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                    <i class="fa fa-download"></i> Generate PDF
                </button>
            </div>
        </div>
    </section>