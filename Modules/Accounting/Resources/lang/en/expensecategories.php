<?php

return [
    'list resource' => 'List expensecategories',
    'create resource' => 'Create expensecategories',
    'edit resource' => 'Edit expensecategories',
    'destroy resource' => 'Destroy expensecategories',
    'title' => [
        'expensecategories' => 'ExpenseCategory',
        'create expensecategory' => 'Create a expensecategory',
        'edit expensecategory' => 'Edit a expensecategory',
    ],
    'button' => [
        'create expensecategory' => 'Create a expensecategory',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
