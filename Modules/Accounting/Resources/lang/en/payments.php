<?php

return [
    'list resource' => 'List payments',
    'create resource' => 'Create payments',
    'edit resource' => 'Edit payments',
    'destroy resource' => 'Destroy payments',
    'title' => [
        'payments' => 'Payment',
        'create payment' => 'Add Payment',
        'edit payment' => 'Edit Payment',
    ],
    'button' => [
        'create payment' => 'New payment',
    ],
    'table' => [
        'date' => 'Date',
        'package' => 'Tour',
        'register_client' => 'Register Client',
        'amount' => 'Amount',
        'mode' => 'Mode',
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
