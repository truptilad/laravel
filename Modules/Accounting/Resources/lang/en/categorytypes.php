<?php

return [
    'list resource' => 'List categorytypes',
    'create resource' => 'Create categorytypes',
    'edit resource' => 'Edit categorytypes',
    'destroy resource' => 'Destroy categorytypes',
    'title' => [
        'categorytypes' => 'categoryType',
        'create categorytype' => 'Create a categorytype',
        'edit categorytype' => 'Edit a categorytype',
    ],
    'button' => [
        'create categorytype' => 'Create a categorytype',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
