<?php

return [
    'list resource' => 'List outstandings',
    'create resource' => 'Create outstandings',
    'edit resource' => 'Edit outstandings',
    'destroy resource' => 'Destroy outstandings',
    'title' => [
        'outstandings' => 'Outstanding',
        'create outstanding' => 'Create a outstanding',
        'edit outstanding' => 'Edit a outstanding',
    ],
    'button' => [
        'create outstanding' => 'Create a outstanding',
    ],
    'table' => [
        'name' => 'Name',
        'reference_by' => 'Reference By',
        'package' => 'Tour',
        'amount' => 'Amount',
        'recieved_amount' => 'Recieved Amount',
        'outstanging' => 'Outstanding',
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
