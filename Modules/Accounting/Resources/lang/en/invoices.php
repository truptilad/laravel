<?php

return [
    'list resource' => 'List invoices',
    'create resource' => 'Create invoices',
    'edit resource' => 'Edit invoices',
    'destroy resource' => 'Destroy invoices',
    'title' => [
        'invoices' => 'Invoice',
        'create invoice' => 'Create a invoice',
        'edit invoice' => 'Edit Invoice',
    ],
    'button' => [
        'create invoice' => 'Create a invoice',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
