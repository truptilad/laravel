<?php

return [
    'list resource' => 'List branches',
    'create resource' => 'Create branches',
    'edit resource' => 'Edit branches',
    'destroy resource' => 'Destroy branches',
    'title' => [
        'branches' => 'Branch',
        'create branch' => 'Create a branch',
        'edit branch' => 'Edit a branch',
    ],
    'button' => [
        'create branch' => 'Create a branch',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
