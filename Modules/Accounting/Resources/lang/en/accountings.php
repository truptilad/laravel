<?php
return [
    'title' => [
        'name' => 'Accounting',
        'accounts' => 'Account',
        'create account' => 'Create a account',
        'edit account' => 'Edit a account',
    ],
    'button' => [
        'create account' => 'Create a account',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];