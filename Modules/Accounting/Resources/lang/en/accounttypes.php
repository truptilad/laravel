<?php

return [
    'list resource' => 'List accounttypes',
    'create resource' => 'Create accounttypes',
    'edit resource' => 'Edit accounttypes',
    'destroy resource' => 'Destroy accounttypes',
    'title' => [
        'accounttypes' => 'AccountType',
        'create accounttype' => 'Create a accounttype',
        'edit accounttype' => 'Edit a accounttype',
    ],
    'button' => [
        'create accounttype' => 'Create a accounttype',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
