<?php

return [
    'list resource' => 'List quotations',
    'create resource' => 'Create quotations',
    'edit resource' => 'Edit quotations',
    'destroy resource' => 'Destroy quotations',
    'title' => [
        'quotations' => 'Quotation',
        'create quotation' => 'Create a quotation',
        'edit quotation' => 'Edit a quotation',
    ],
    'button' => [
        'create quotation' => 'Create a quotation',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
