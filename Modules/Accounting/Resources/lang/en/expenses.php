<?php

return [
    'list resource' => 'List expenses',
    'create resource' => 'Create expenses',
    'edit resource' => 'Edit expenses',
    'destroy resource' => 'Destroy expenses',
    'title' => [
        'expenses' => 'Expense',
        'create expense' => 'New expense',
        'edit expense' => 'Edit a expense',
    ],
    'button' => [
        'create expense' => 'New expense',
    ],
    'table' => [
        'date' => 'Date',
        'package' => 'Tour',
        'category' => 'Category',
        'amount' => 'Amount',
        'mode' => 'Mode',
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
