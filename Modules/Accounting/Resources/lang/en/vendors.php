<?php

return [
    'list resource' => 'List vendors',
    'create resource' => 'Create vendors',
    'edit resource' => 'Edit vendors',
    'destroy resource' => 'Destroy vendors',
    'title' => [
        'vendors' => 'vendor',
        'create vendor' => 'Create a vendor',
        'edit vendor' => 'Edit a vendor',
    ],
    'button' => [
        'create vendor' => 'Create a vendor',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
