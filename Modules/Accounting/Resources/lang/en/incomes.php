<?php

return [
    'list resource' => 'List Loan',
    'create resource' => 'Create Loan',
    'edit resource' => 'Edit Loan',
    'destroy resource' => 'Destroy Loan',
    'title' => [
        'incomes' => 'Loan',
        'create income' => 'Create a Loan',
        'edit income' => 'Edit a Loan Details',
    ],
    'button' => [
        'create income' => 'New Loan',
    ],
    'table' => [
        'date' => 'Date',
        'income_source' => 'Income Source',
        'package' => 'Tour',
        'amount' => 'Amount',
        'mode' => 'Mode',
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'income' => 'Loan',
];
