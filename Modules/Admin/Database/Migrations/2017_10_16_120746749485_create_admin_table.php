<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin__registrations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('package')->refrences('id')->on('admin__packages')->nullable();
            $table->integer('refrences_by')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->text('address')->nullable();
            $table->text('city')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('adhar_no')->nullable();
            $table->text('voting_no')->nullable();
            $table->string('pan_card_no')->nullable();
            $table->text('other_proof')->nullable();
            $table->integer('profile_img_id')->nullable();
            $table->integer('age')->nullable();
            $table->text('email_id')->nullable();
            $table->string('password')->nullable();
            $table->string('TTDSEVAID')->nullable();
            $table->string('TTDSEVAPASSWORD')->nullable();
            $table->string('status')->nullable();
            $table->string('record_status')->nullable()->default("A");
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('admin__packages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('package')->nullable();
            $table->decimal('package_amount')->nullable();
            $table->string('description')->nullable();
            $table->string('record_status')->nullable()->default("A");
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin__registrations');
        Schema::dropIfExists('admin__packages');
    }
}
