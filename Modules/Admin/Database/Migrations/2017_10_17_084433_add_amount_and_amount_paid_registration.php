<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmountAndAmountPaidRegistration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin__registrations', function (Blueprint $table) {
            $table->integer('amount')->nullable();
            $table->integer('paid')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin__registrations', function (Blueprint $table) {
            $table->dropColumn('amount');
            $table->dropColumn('paid');
        });
    }
}
