<?php

namespace Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Admin\Events\Handlers\RegisterAdminSidebar;

class AdminServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterAdminSidebar::class);
    }

    public function boot()
    {
        $this->publishConfig('admin', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Admin\Repositories\RegistrationRepository',
            function () {
                $repository = new \Modules\Admin\Repositories\Eloquent\EloquentRegistrationRepository(new \Modules\Admin\Entities\Registration());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Admin\Repositories\Cache\CacheRegistrationDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Admin\Repositories\PackageRepository',
            function () {
                $repository = new \Modules\Admin\Repositories\Eloquent\EloquentPackageRepository(new \Modules\Admin\Entities\Package());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Admin\Repositories\Cache\CachePackageDecorator($repository);
            }
        );
// add bindings


    }
}
