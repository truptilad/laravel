<?php

return [
    'admin.registrations' => [
        'index' => 'admin::registrations.list resource',
        'create' => 'admin::registrations.create resource',
        'edit' => 'admin::registrations.edit resource',
        'view' => 'admin::registrations.view resource',
        'destroy' => 'admin::registrations.destroy resource',
    ],
    'admin.packages' => [
        'index' => 'admin::packages.list resource',
        'create' => 'admin::packages.create resource',
        'edit' => 'admin::packages.edit resource',
        'view' => 'admin::packages.view resource',
        'destroy' => 'admin::packages.destroy resource',
    ],
// append


];
