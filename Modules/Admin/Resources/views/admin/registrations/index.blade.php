@extends('layouts.master')

@section('content-header')
   
@stop

@section('content')
<?php use Carbon\Carbon; ?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="row" style="padding: 10px;" >                
                <div class="col-md-6">
                    <div class="form-group" >
                        <label>Select Registered Name :</label>
                        <input type="text" class="form-control" name="first_name" id="first_name" value="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" >
                        <label>Select Referance By :</label>
                        <input type="text" class="form-control" name="referance_by" id="referance_by" value="">
                    </div>    
                </div>
            </div>
            <div class="row" style="padding: 10px;" >
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Select Status :</label>
                        <input type="text" class="form-control" name="status" id="status" value="">
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3>
                        {{ trans('admin::registrations.title.registrations') }}
                        
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-default btn-sm" id="daterange-btn" style="font-size: 15px">
                            <span id="daterange" value="">
                                <i class="fa fa-calendar"></i> 
                            </span>
                                  <i class="fa fa-caret-down"></i>
                            </button>
                        </div>

                        <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                            <a href="{{ route('admin.admin.registration.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                                <i class="fa fa-pencil"></i> {{ trans('admin::registrations.button.create registration') }}
                            </a>
                             <a href="{{ route('admin.accounting.payment.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;margin-left: 10px;" >
                                <i class="fa fa-credit-card"></i> {{ trans('admin::registrations.button.make payment') }}
                            </a>
                        </div>
                        <!-- <a href="{{ URL::route('getRegistrationExcel',CSV_TYPE) }}" class="btn btn-info pull-right" style="margin-right: 10px;">Download CSV</a> -->
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ trans('admin::registrations.table.registration_id')}}</th>
                                <th>{{ trans('admin::registrations.table.package_name')}}</th>
                                <th>{{ trans('admin::registrations.table.name')}}</th>
                                <th>{{ trans('admin::registrations.table.referance_by')}}</th>
                                <th>{{ trans('admin::registrations.table.contact')}}</th>
                                <th>{{ trans('admin::registrations.table.pending_amount')}}</th>
                                <th>{{ trans('admin::registrations.table.status')}}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody id="result">
                               
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ trans('admin::registrations.table.registration_id')}}</th>
                                <th>{{ trans('admin::registrations.table.package_name')}}</th>
                                <th>{{ trans('admin::registrations.table.name')}}</th>
                                <th>{{ trans('admin::registrations.table.referance_by')}}</th>
                                <th>{{ trans('admin::registrations.table.contact')}}</th>
                                <th>{{ trans('admin::registrations.table.pending_amount')}}</th>
                                <th>{{ trans('admin::registrations.table.status')}}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    <input type="hidden" id="start-date">
    <input type="hidden" id="end-date">
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('admin::registrations.title.create registration') }}</dd>
    </dl>
@stop

@push('js-stack')


    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.admin.registration.create') ?>" }
                ]
            });

            $('#status,#referance_by,#pending_amount,#first_name').on('change', function(){
                fetchRequestedInfo();
            });

             $("#start-date").val(moment().startOf('month'));
             $("#end-date").val(moment().endOf('month'));

             $('#daterange').append(moment().startOf('month').format('MMMM D, YYYY')+' - '+moment().endOf('month').format('MMMM D, YYYY'));


            $('#daterange-btn').daterangepicker({

                    ranges: {
                        'Last 7 Days': [moment(), moment().subtract(7, 'days')],
                        'Last 10 Days': [moment(), moment().subtract(10, 'days')],
                        'Last 20 Days': [moment(), moment().subtract(20, 'days')],
                        'Last Month': [moment().subtract("month",1).startOf('month'), moment().subtract("month",1).endOf('month')],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],

                    },
                    startDate:moment().startOf('month') ,
                    endDate: moment().endOf('month'),
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    $("#start-date").val(start);
                    $("#end-date").val(end);
                    fetchRequestedInfo();

                }).on( function getData() {});

            // $(".filter").select2({
            //     width: '100%',
            //     placeholder: 'Select for filter',
            // }).on('select2:select', function(){
            //     fetchRequestedInfo();
            // });

            fetchRequestedInfo();
        });

        
    </script>
    <?php $locale = locale(); ?>

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>

    <script type="text/javascript">
        
        function fetchRequestedInfo() {

            // window.location.href='http://www.google.com';

            $(function () {
                tabl = $('.data-table').dataTable({
                    "destroy":true,
                    // "sDom": '<"top"flp>rt<"bottom"i><"clear">',
                    "paginate": true,
                    processing: true,
                    serverSide: true,
                    "ajax": {
                        "type":'get',
                        "url": '{{ route("fetchAllData") }}',
                        "data": function(d) {


                            var status = $("#status").val();
                            var referance_by = $("#referance_by").val();
                            var first_name = $("#first_name").val();
                            var startDate = $("#start-date").val();
                            var endDate = $("#end-date").val();

                            d.status = status;
                            d.referance_by = referance_by;
                            d.first_name = first_name;
                            d.startDate = startDate;
                            d.endDate =endDate;
                            d._token = $('meta[name="token"]').attr('value');
                            
                        }

                    },
                    "lengthChange": true,
                    "filter": true,
                    "sort": true,
                    "info": true,
                    "autoWidth": true,
                    "order": [[ 3, "desc" ],[ 5, "desc" ],[ 6, "desc" ]],
                    "language": {
                        "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                    },
                    "search": {
                        "caseInsensitive": false
                    },
                    columns: [

                        {
                            data:'id',
                        },

                        {   data: 'packageInfo.package',
                            render: function ( data, type, full, meta ) {
                                if(full.package_info != null){

                                    var editPath = "{{ route('admin.admin.package.view', ["id"]) }}";
                                    var editRepath = editPath.replace("id",full.package_info.id);

                                    return '<a href="'+editRepath+'" >'+full.package_info.package+'</a>';
                                }
                                else{
                                    return '-';
                                }
                                
                             }
                        },
                        {   data: 'first_name',

                            render: function ( data, type, full, meta ) {
                                var full_name = full.first_name+' '+full.last_name;
                                return full_name;
                               
                             }
                        },
                        {  
                            render: function ( data, type, full, meta ) {
                                if(full.referance_by != null) {
                                    var full_name = full.referance_by.first_name+' '+full.referance_by.last_name;
                                    return full_name;
                                }
                                else{
                                    return "Self";
                                }
                               
                             }
                        },
                        {   data: 'phone_no',       
                            render: function (data, type, full, meta ) {
                                return full.phone_no;
                            
                            }
                        },
                        {   data: 'pending_amount',
                            render: function ( data, type, full, meta ) {
                                var paidAmount = full.amount - full.paid;
                                return paidAmount;
                                
                             }
                        },
                        {   data: 'status',
                            render: function ( data, type, full, meta ) {
                                return full.status;
                            }

                        },
                        {
                            render: function ( data, type, full, meta ) {
                                var editPath = "{{ route('admin.admin.registration.edit',["id"]) }}";
                                var editRepath = editPath.replace("id",full.id);

                                var deletePath = "{{ route('admin.admin.registration.destroy',["id"]) }}";
                                var deleteRepath = deletePath.replace("id",full.id);

                                var action = '<a onclick="getModal('+full.id+')" class="btn btn-default btn-flat">'+'<i class="fa fa-credit-card">'+'</i>'+'</a>'+'<a href="'+editRepath+'" class="btn btn-default btn-flat">'+'<i class="fa fa-pencil">'+'</i>'+'</a>'+
                                        '<button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="'+deleteRepath+'">'+'<i class="fa fa-trash">'+'</i>'+'</button>';
                                return action;
                                
                             }
                        }
                        
                    ],
                    // initComplete: function () {
                    //     this.api().columns().every(function () {
                    //         var column = this;
                    //         var input = document.createElement("input");
                    //         $(input).appendTo($(column.footer()).empty())
                    //         .on('change', function () {
                    //             column.search($(this).val()).draw();

                    //         });
                    //     });
                    // }


                });
               

            });
        }


        function getModal($outstandingId) {

                $.ajax({
                type: 'GET',
                url: '{{ URL::route('getModal')}}',
                data: {
                    id: $outstandingId,
                    _token: $('meta[name="token"]').attr('value'),
                },
                success: function (response) {

                    $('#commonModal').modal('show');
                    $('#commonModal').html(response);


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Oops.", "Something went wrong, Please try again", "error");
                }
            });
        }


    </script>
@endpush
