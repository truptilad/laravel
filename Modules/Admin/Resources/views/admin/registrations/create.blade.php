@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('admin::registrations.title.create registration') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.admin.registration.index') }}">{{ trans('admin::registrations.title.registrations') }}</a></li>
        <li class="active">{{ trans('admin::registrations.title.create registration') }}</li>
    </ol>
@stop
@section('styles')
    {!! Theme::style('vendor/datetimepicker/jquery.datetimepicker.min.css') !!}
@stop
<?php use Carbon\Carbon; ?>
@section('content')

    {!! Former::open_for_files()
        ->route('admin.admin.registration.store')
        ->method('POST')
    !!}
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                    @include('partials.form-tab-headers')
                    @include('admin::admin.registrations.partials.create-fields')
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right btn-flat">{{ trans('admin::registrations.button.create') }}</button>
                        <a class="btn btn-danger btn-flat" href="{{ route('admin.admin.registration.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
{!! Theme::script('vendor/datetimepicker/jquery.datetimepicker.full.min.js') !!}
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.admin.registration.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            $('#paid-amount').show();
        });

    </script>
      <script>
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.upload-image img').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".file-input").change(function(){
            $('.img-circle').append('<scr>');
            readURL(this);
        });
    </script>
    <script>
        $(function() {

            // We can attach the `fileselect` event to all file inputs on the page
            $(document).on('change', ':file', function() {
                var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });

            // We can watch for our custom `fileselect` event like this
            $(document).ready( function() {
                $(':file').on('fileselect', function(event, numFiles, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) $('.img-circle').append('<scr>');
                    }

                });
            });

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#package').select2().on('select2:select', function (e) {
                var package = JSON.parse('@json($packageAll)');

                var package = package.find(function (a) {
                    
                    if(a.id == e.params.data.id)
                    {
                        $('#amount').val(a.package_amount);
                    }
                });
            });


            $('#refrences_by,#user_id,#package').select2({
                placholder:$(this).data('placeholder')
            });

            $('#date_of_birth').datetimepicker({
                timepicker:false,
                maxDate: new Date,
                format:'{{PHP_DATE_FORMAT}}',
                onChangeDateTime:function(dp,$input){
                    if(dp != new Date()) {
                        $('#age').val(getAge(dp));
                    }
                    

                }
            });


            $('#user_id').select2({
                placholder:$(this).data('placeholder')
            }).on("select2:selecting", function(e) {

                    $.ajax({
                    type: 'GET',
                    url: '{{ URL::route('getUserDetails')}}',
                    data: {
                        user_id: e.params.args.data.id,
                        _token: $('meta[name="token"]').attr('value'),
                    },
                    success: function (response) {

                        $("#first_name").val(response.userDetails.first_name);
                        $("#last_name").val(response.userDetails.last_name);
                        $("#address").val(response.userDetails.address);
                        $("#city").val(response.userDetails.city);
                        $("#date_of_birth").val(moment(response.userDetails.date_of_birth).format('DD-MMM-YYYY'));
                        $("#age").val(response.userDetails.age);
                        $("#adhar_no").val(response.userDetails.adhar_no);
                        $("#voting_no").val(response.userDetails.voting_no);
                        $("#pan_card_no").val(response.userDetails.pan_card_no);
                        $("#phone_no").val(response.userDetails.phone_no);
                        $("#TTDSEVAID").val(response.userDetails.TTDSEVAID);
                        $("#TTDSEVAPASSWORD").val(response.userDetails.TTDSEVAPASSWORD);
                        $("#email").val(response.userDetails.email);
                        $("#password").val(response.userDetails.password);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Oops.", "Something went wrong, Please try again", "error");
                    }
                });
            })
        });

        function getAge(dateString) 
        {
            var today = new Date();
            var birthDate = new Date(dateString);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
            {
                age--;
            }
           
            return age;
        }
    </script>
@endpush
