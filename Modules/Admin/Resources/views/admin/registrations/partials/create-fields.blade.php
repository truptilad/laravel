<div class="">
    <div class="box-header">
        
    </div>
   <div class="box-body">
        <div class="row">
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="upload-image">
                            <img class="profile-user-img img-responsive img-circle" src="{{ isset($registration->file)?$registration->file->path:Theme::url('img/avatar5.png')}}" alt="profile picture" style="width: 110px;height: 115px;">
                        </div><!-- Preview Div-->
                        <br>
                        <div class="input-group" style="padding-bottom: 25px;">
                            <label class="input-group-btn" class="upload-data">
                                <span class="btn btn-primary">
                                    Browse&hellip; <input type="file" name="profile_img_id" class="file-input" style="display: none;" id="profile_img_id">
                                </span>
                            </label>
                            <input type="text" class="form-control" value="{{ isset($registration->file)?$registration->file->filename:'choose your file'}}" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                 <div class="row">
                        <div class="col-md-6">
                            {!!
                                Former::select('user_id')->label('Existing User')
                               ->addOption(null)
                               ->data_placeholder('Select Existing user')
                               ->fromQuery($users,'first_name','id')

                             !!}
                        </div>
                        <div class="col-md-6">
                            {!!
                                Former::select('refrences_by')
                               ->addOption(null)
                               ->data_placeholder('Select Referance')
                               ->fromQuery($registrations,'first_name','id')

                             !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            {!!
                                Former::text('first_name')->label('First*')
                                ->placeholder('Enter First Name')
                             !!}
                        </div>
                        <div class="col-md-4">
                            {!!
                                Former::text('father_name')->label('Father*')
                                ->placeholder('Enter Father Name')
                             !!}
                        </div>
                        <div class="col-md-4">
                            {!!
                                Former::text('last_name')->label('Last*')
                                ->placeholder('Enter last Name')
                             !!}
                        </div>
                    </div>
                   
            </div>
        </div>
        
        <div class="row">
             <div class="col-md-4">
                {!!
                    
                    Former::select('package')->label('Tour*')
                    ->placeholder('Select Tour')
                    ->fromQuery($packages,'package','id')

                 !!}
            </div>
            <div class="col-md-4">
                {!!
                    Former::integer('amount')
                    ->placeholder('Enter Amount')
                 !!}
            </div>
            <div class="col-md-4" id="paid-amount" hidden>
                {!!
                    Former::integer('paid')->value('0.00')
                 !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!!
                     Former::textarea('address')
                     ->placeholder('Enter Your postal Address')
                    
                 !!}
            </div>
            <div class="col-md-6">
                {!!
                     Former::text('city')
                     ->placeholder('Enter Your City')
                 !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!!
                    Former::text('date_of_birth')
                    ->placeholder('Select Date Of Birth')
                    
                 !!}
            </div>
            <div class="col-md-6">
                {!!
                     Former::integer('age')
                 !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!!
                    Former::text('adhar_no')
                    ->placeholder('Enter Addhar No.')
                 !!}
            </div>
            <div class="col-md-6">
                {!!
                    Former::text('voting_no')
                    ->placeholder('Enter Voting No.')
               
               !!}
            </div>
        </div>
        <div class="row">
             <div class="col-md-6">
                {!!
                    Former::text('pan_card_no')
                    ->placeholder('Enter Pan Card No.')
                 !!}
            </div>
            <div class="col-md-6">
                {!!
                     Former::text('phone_no')->label('Phone No.*')
                     ->setAttribute('maxlength',10)
                     ->placeholder('Enter Your Contact No.')
                    
                 !!}
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                {!!
                    Former::text('TTDSEVAID')
                 !!}
            </div>
            <div class="col-md-6">
                {!!
                    Former::text('TTDSEVAPASSWORD')
                 !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!!
                    Former::text('email')
                    ->placeholder('Enter Your Email Address')
               
               !!}
            </div>
            <div class="col-md-6">
                {!!
                    Former::text('password')
               
               !!}
            </div>
            
        </div>
       
    </div>
</div>
