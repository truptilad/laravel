@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('admin::packages.title.edit package') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.admin.package.index') }}">{{ trans('admin::packages.title.packages') }}</a></li>
        <li class="active">{{ trans('admin::packages.title.edit package') }}</li>
    </ol>
@stop

@section('content')

    {{Former::populate($package)}}


    {!! Former::open_for_files()
      ->route('admin.admin.package.update',$package->id)
      ->method('put')
    !!}

    {{ csrf_field() }}
    <?php use Carbon\Carbon; ?>
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                    @include('partials.form-tab-headers')
                    @include('admin::admin.packages.partials.create-fields')

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger btn-flat" href="{{ route('admin.admin.package.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.admin.package.index') ?>" }
                ]
            });

            $('#date_of_birth').datetimepicker({
            timepicker:false,
            format:'{{PHP_DATE_FORMAT}}',
            value:'{{isset($package->date)?carbon::parse($package->date)->format(PHP_DATE_FORMAT) : '' }}',
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
