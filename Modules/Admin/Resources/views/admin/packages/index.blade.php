@extends('layouts.master')

@section('content-header')
    
@stop

@section('content')
<?php use Carbon\Carbon; ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3>
                        {{ trans('admin::packages.title.packages') }}
                        <div class="btn-group pull-right">
                            <button class="btn btn-info btn-flat" style="padding: 4px 10px;" onclick="getGroupModal()"><i class="fa fa-comments"></i> Send Message</button>
                        </div>
                        <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                            <a href="{{ route('admin.admin.package.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                            <i class="fa fa-pencil"></i> {{ trans('admin::packages.button.create package') }}
                            </a>
                        </div>
                    </h3>
                    
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>{{ trans('admin::packages.table.package_name')}}</th>
                                <th>{{ trans('admin::packages.table.package_amount')}}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (isset($packages))
                            @foreach ($packages as $package)
                            <tr>
                                <td>
                                    <a href="{{ route('admin.admin.package.view', [$package->id]) }}">
                                        {{ carbon::parse($package->date)->format(PHP_DATE_FORMAT) }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.admin.package.view', [$package->id]) }}">
                                        {{ $package->package }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.admin.package.view', [$package->id]) }}">
                                        {{ $package->package_amount }}
                                    </a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-success btn-flat" onclick="getGroupModal('{{$package->id}}')"><i class="fa fa-comments"></i></a>
                                        <a href="{{ route('admin.admin.package.edit', [$package->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.admin.package.destroy', [$package->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @else
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td><P>No Data Available in Table</P></td>
                                    <td></td>
                                </tr>
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Date</th>
                                <th>{{ trans('admin::packages.table.package_name')}}</th>
                                <th>{{ trans('admin::packages.table.package_amount')}}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                <h3 class="box-title" data-widget="collapse">Add Members For tour</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! Former::open_for_files()
                        ->route('postGroupRegistrations')
                        ->method('POST')
                    !!}

                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <label>Tour*</label>
                            <select class="form-control" name="package_id" id="package" value="{{ old('package') }}">
                                 <option disabled selected value=""></option>
                                @foreach($packages as $package)
                                <option value="{{$package->id}}">{{$package->package}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Referance By</label>
                            <select class="form-control" name="refrences_by" id="refrences_by" value="{{ old('refrences_by') }}">
                                 <option disabled selected value=""></option>
                                @foreach($registrations as $registration)
                                <option value="{{$registration->id}}">{{$registration->first_name}} {{$registration->last_name}}</option>
                                @endforeach
                            </select>
                        </div>    
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-info" onclick="getAllMenbers()">Add Members</button>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12" id="members">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-success pull-right btn-flat" value="Register">
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('admin::packages.title.create package') }}</dd>
    </dl>
@stop

@push('js-stack')
<script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.admin.package.create') ?>" }
                ]
            });
        });
    </script>
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
    
    <?php $locale = locale(); ?>
    <script type="text/javascript">

        $(document).ready(function() {

            $('#package').select2({
                placeholder:'Select Tour',
                width:'100%'
            });
            $('#refrences_by').select2({
                placeholder:'Select Referance By',
                width:'100%'
            });
        });

        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": false,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                },
                "search": {
                    "caseInsensitive": false
                },
            });
        });

        function getAllMenbers() {

            $.ajax({
                type: 'GET',
                url: '{{ URL::route('getAllMenbers')}}',
                data: {
                    _token: $('meta[name="token"]').attr('value'),
                },
                success: function (response) {

                    var groupedArr = createGroupedArray(response.allMembers,3);
                    var result = JSON.stringify(groupedArr);
                    console.log(result);

                    $('#members').html('');
                    $.each(response.allMembers, function($key, $value){

                        $('#members').append('<label>'+'<input type="checkbox" name="members_id[]" value="'+this.id+'" class="flat-red" >'+'          '+this.first_name+' '+this.last_name+'</label>'+'<br/>'); 
                               
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Oops.", "Something went wrong, Please try again", "error");
                }
            });
        }

        function getGroupModal($packageId) {

                $.ajax({
                type: 'GET',
                url: '{{ URL::route('getGroupModal')}}',
                data: {
                    packageId: $packageId,
                    _token: $('meta[name="token"]').attr('value'),
                },
                success: function (response) {

                    $('#commonModal').modal('show');
                    $('#commonModal').html(response);


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Oops.", "Something went wrong, Please try again", "error");
                }
            });
        }

        function postGroupMessage($packageId) {

            $.ajax({
                type: 'POST',
                url: '{{ URL::route('postGroupMessage')}}',
                data: {
                    packageId: $packageId,
                    message: $('#message').val(),
                   
                    _token: $('meta[name="token"]').attr('value'),
                },
                success: function (response) {

                    $('#commonModal').modal('hide');
                    toastr.success('Sms Send');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Oops.","Something went wrong, Please try again", "error");
                }
            });

        }

        var createGroupedArray = function(arr, chunkSize) {
            var groups = [], i;
            for (i = 0; i < arr.length; i += chunkSize) {
                groups.push(arr.slice(i, i + chunkSize));
            }
            return groups;
        }
    </script>
@endpush
