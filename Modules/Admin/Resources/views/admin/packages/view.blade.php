@extends('layouts.master')


@section('content-header')
<div class="row">
	<div class="col-md-6">
		<h3>{{$package->package}}</h3>
	</div>
	<div class="col-md-6">
		<h3><i class="fa fa-inr"></i> {{$package->package_amount}}</h3>
	</div>
	
</div>
@stop

@section('styles')
   <style type="text/css">
   		.tablength{
   			width: 24%;
   			text-align: center;
   			font-size: 18px;  			
   		}
   		ul li.active{
   			background-color: lightblue !important;
   		}
   </style>
@stop

@section('content')
<?php use Carbon\Carbon; ?>




<div class="row">
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            	<p>Registrations</p>
              	<h4><span >{{isset($registrations)?count($registrations):'0'}}</span></h4>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">

              <p>Balance</p>

              <h4><span id="amount">0</span></h4>

            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
            	<p>Received</p>
              	<h4><span id="payment">0</span></h4>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <p>Outstanding</p>
              <h4><span id="outstanding">0</span></h4>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            	<p>Loan</p>
              	<h4><span id="income">0</span></h4>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">

              <p>Expence</p>

              <h4><span id="expense">0</span></h4>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>
<div class="box box-solid">
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                      <li class="tablength active"><a href="#registration-tab" data-toggle="tab">Registration</a></li>
                      <li class="tablength"><a href="#payment-tab" data-toggle="tab">Received Payment</a></li>
                      <li class="tablength"><a href="#income-tab" data-toggle="tab">{{ trans('accounting::incomes.income') }}</a></li>
                      <li class="tablength"><a href="#expence-tab" data-toggle="tab">Expences</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="registration-tab">
                        	<div class="box-body">
			                    <div class="table-responsive">
			                        <table class="data-table table table-bordered table-hover">
			                            <thead>
				                            <tr>
				                                <th>{{ trans('admin::registrations.table.registration_id')}}</th>
				                                <th>{{ trans('admin::registrations.table.name')}}</th>
				                                <th>{{ trans('admin::registrations.table.referance_by')}}</th>
				                                <th>{{ trans('admin::registrations.table.contact')}}</th>
				                                <th>{{ trans('admin::registrations.table.status')}}</th>
				                                <th>Recieved Amount</th>
				                                <th>{{ trans('admin::registrations.table.pending_amount')}}</th>
				                            </tr>
				                        </thead>
			                            <tbody>
			                            	@if($registrations->isNotEmpty())
			                            	<?php $totalAmount = 0; $payment = 0;?>
			                            	@foreach($registrations as $registration)
				                            	<tr>
					                            	<td>{{ $registration->id }}</td>
					                            	<td>{{ $registration->first_name }} {{ $registration->last_name }}</td>
					                            	<td>{{ isset($registration->referanceBy)?$registration->referanceBy->first_name:'Self' }}</td>
					                            	<td>{{ $registration->phone_no }}</td>
					                            	<td>{{ isset($registration->status)?$registration->status:'NA'}}</td>
					                            	<td>{{$registration->paid}}</td>
					                            	<td>{{ $registration->amount - $registration->paid}}</td>
					                            	<td></td>
				                            	</tr>
				                            	<?php $totalAmount = $totalAmount + $registration->amount;
				                            		  $payment = $payment + $registration->paid;
				                            	?>
			                            	@endforeach
			                            	<input type="hidden" name="totalAmount" id="totalAmount" value="{{ $totalAmount}}">
			                            	<input type="hidden" name="payment" id="totalPayment" value="{{$payment}}">
			                            	@else
			                            		<tr>
					                                <td colspan="6"><center>No Data Available</center></td>
				                                <tr>
			                          		@endif
			                            </tbody>
			                            <tfoot>
				                            <tr>
				                                <th>{{ trans('admin::registrations.table.registration_id')}}</th>
				                                <th>{{ trans('admin::registrations.table.name')}}</th>
				                                <th>{{ trans('admin::registrations.table.referance_by')}}</th>
				                                <th>{{ trans('admin::registrations.table.contact')}}</th>
				                                <th>{{ trans('admin::registrations.table.status')}}</th>
				                                <th>Recieved Amount</th>
				                                <th>{{ trans('admin::registrations.table.pending_amount')}}</th>
				                            </tr>
			                            </tfoot>
			                        </table>
			                        <!-- /.box-body -->
			                    </div>
			                </div>
                        </div>
                        <div class="tab-pane " id="payment-tab">
                        	<div class="box-body">
			                    <div class="table-responsive">
			                        <table class="data-table table table-bordered table-hover">
			                        	<thead>
				                            <tr>
				                                <th>Payment Date</th>
				                                <th>Register Client</th>
				                                <th>Reference By</th>
				                                <th><i class="fa fa-inr"></i> Amount</th>
				                            </tr>
			                            </thead>
			                            <tbody>
				                            @if ($payments->isNotEmpty())
				                            @foreach ($payments as $payment)
					                            <tr>
					                                <td>{{isset($payment->date)?carbon::parse($payment->date)->format(PHP_DATE_FORMAT):'NA'}}</td>
					                                <td>{{isset($payment->registration)?$payment->registration->first_name:'NA'}}</td>
					                                <td>{{ isset($payment->registration->referanceBy)?$payment->registration->referanceBy->first_name:'Self'}} {{ isset($payment->registration->referanceBy)?$payment->registration->referanceBy->last_name:''}}</td>
					                                <td>{{$payment->amount}}</td>
					                            </tr>
				                            @endforeach
				                            @else
				                            	<tr>
					                                <td colspan="3"><center><p class="lead">No Data Available</p></center></td>
				                                <tr>
				                            @endif
				                            </tbody>
				                            <tfoot>
				                            	<tr>
					                                <th>Payment Date</th>
					                                <th>Register Client</th>
					                                <th>Reference By</th>
					                                <th>Amount</th>
					                            </tr>
				                            </tfoot>
			                       	</table>
		                       	</div>
	                       	</div>
                        </div>
                        <div class="tab-pane " id="income-tab">
                        	<div class="box-body">
			                    <div class="table-responsive">
			                        <table class="data-table table table-bordered table-hover">
			                        	<thead>
			                                <tr>
			                                    <th>Date</th>
			                                    <th>Income Source</th>
			                                    <th>Amount</th>
			                                    <th>Mode</th>
			                                </tr>
			                            </thead>
			                            <tbody>
				                            @if ($incomes->isNotEmpty())
				                            <?php $totalIncome = 0 ; ?>
				                            @foreach ($incomes as $income)
					                            <tr>
					                                <td>{{isset($income->date)?carbon::parse($income->date)->format(PHP_DATE_FORMAT):'NA'}}</td>
					                                <td>{{$income->income_source_name}}</td>
					                                <td>{{$income->amount}}</td>
					                                @if($income->payment_mode == 0)
					                                    <td>Cheque</td>
					                                @else
					                                <td>Cash</td>
					                                @endif
					                            </tr>
					                            <?php $totalIncome = $totalIncome + $income->amount; ?>
				                            @endforeach
				                            <input type="hidden" name="" id="totalIncome" value="{{$totalIncome}}">
				                            @else
				                            	<tr>
					                                <td colspan="4"><center><p class="lead">No Data Available</p></center></td>
				                                <tr>
				                            @endif
				                            </tbody>
				                            <tfoot>
				                            	<tr>
				                                    <th>Date</th>
				                                    <th>Income Source</th>
				                                    <th>Amount</th>
				                                    <th>Mode</th>
				                                </tr>
				                            </tfoot>
		                        	</table>
	                        	</div>
                        	</div>
                        </div>
                        <div class="tab-pane " id="expence-tab">
                        	<div class="box-body">
			                    <div class="table-responsive">
			                        <table class="data-table table table-bordered table-hover">
			                        	<thead>
			                        		<tr>
			                                    <th>Date</th>
			                                    <th>Category</th>
			                                    <th>Amount</th>
			                                    <th>Account</th>
			                                    <th>Mode</th>
			                                </tr>
			                        	</thead>
			                        	<tbody>
			                        		@if ($expences->isNotEmpty())
			                        		<?php $totalExpense = 0; ?>
				                            @foreach ($expences as $expense)
				                            <tr>
				                                <td>{{isset($expense->date)?carbon::parse($expense->date)->format(PHP_DATE_FORMAT):'NA'}}</td>
				                                <td>{{isset($expense->category)?$expense->category->name : 'NA'}}</td>
				                                <td>{{$expense->amount}}</td>
				                                <td>{{isset($expense->account)?$expense->account->name:'NA'}}</td>
				                                @if($expense->payment_mode == 0)
				                                    <td>Cheque</td>
				                                @else
				                                <td>Cash</td>
				                                @endif
				                            </tr>
				                            <?php $totalExpense = $totalExpense + $expense->amount ?>
				                            @endforeach
				                            @else
				                          		<tr>
				                          			<td colspan="4"><center><p class="lead">No Data Available</p></center></td>
				                          		</tr>
				                            @endif
			                        	</tbody>
			                        	<tfoot>
			                        		<tr>
			                                    <th>Date</th>
			                                    <th>Category</th>
			                                    <th>Amount</th>
			                                    <th>Account</th>
			                                    <th>Mode</th>
			                                </tr>
			                        	</tfoot>
		                        	</table>
	                        	</div>
                        	</div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {

			var amount = ($('#totalAmount').val()).toLocaleString();
			// $('#amount').text(parseInt(amount).toLocaleString());
			$('#payment').text(parseInt($('#totalPayment').val()).toLocaleString());
			$('#outstanding').text(parseInt(amount - $('#totalPayment').val()).toLocaleString());
			$('#income').text(parseInt({{ isset($totalIncome)?$totalIncome:'0' }} ).toLocaleString());
			$('#expense').text(parseInt({{isset($totalExpense)?$totalExpense:'0'}}).toLocaleString());

			var balance = parseFloat($('#totalPayment').val())-parseFloat({{ isset($totalIncome)?$totalIncome:'0' }})-parseFloat({{isset($totalExpense)?$totalExpense:'0'}});

			$('#amount').text(balance);
		})
	</script>
@stop
@stop
