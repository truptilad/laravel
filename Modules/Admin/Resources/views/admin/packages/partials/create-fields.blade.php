<div class="">
    <div class="box-body">
        <div class="row">
            <div class="col-md-8">
                {!!
                    Former::text('package')->label('Tour Name')

                 !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                {!!
                    Former::integer('package_amount')->label('Tour amount')
                 !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                {!!
                    Former::text('date')->label('Tour Date')
                    
                 !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-1">
                <div class="controls">
                    <label for="clients" class="control-label">Description</label>
                </div>      
            </div>
            <div class="col-md-9">
                <div class="controls">
                    <textarea id="description" name="description" class="ckeditor">{{isset($package)?$package->description:''}}</textarea>
                </div>    
            </div>    
        </div>
    </div>
</div>
