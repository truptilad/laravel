<?php

return [
    'list resource' => 'List packages',
    'create resource' => 'Create packages',
    'edit resource' => 'Edit packages',
    'view resource' => 'View Package Details',
    'destroy resource' => 'Destroy packages',
    'title' => [
        'packages' => 'Tour',
        'create package' => 'New Tour',
        'edit package' => 'Edit a Tour',
        'package Details' => 'Tour Details',
    ],
    'button' => [
        'create package' => 'New Tour',
    ],
    'table' => [
        'package_name' => 'Tour Name',
        'package_amount' => 'Amount',
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
