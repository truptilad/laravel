<?php

return [
    'list resource' => 'List registrations',
    'create resource' => 'Create registrations',
    'edit resource' => 'Edit registrations',
    'view resource' => 'View Registrations',
    'destroy resource' => 'Destroy registrations',
    'title' => [
        'registrations' => 'Registration',
        'create registration' => 'Registration',
        'edit registration' => 'Edit a registration',
        'view registration' => 'Registration Details',
    ],
    'button' => [
        'create registration' => 'New registration',
        'make payment' => 'Enter payment',
        'create' => 'Registration'
    ],
    'table' => [
        'created_date' => 'Date',
        'package_name' => 'Tour Name',
        'referance_by' => 'Referance By',
        'registration_id' => 'Registration Id',
        'status' => 'Status',
        'name' => 'Name',
        'pending_amount' => 'Pending Amount',
        'contact' => 'Contact',
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
