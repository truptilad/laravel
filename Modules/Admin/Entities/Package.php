<?php

namespace Modules\Admin\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Admin\Entities\Registration;

class Package extends Model
{
    use SoftDeletes;

    protected $table = 'admin__packages';
    public $translatedAttributes = [];
    protected $fillable = [
    	'id',
    	'package',
    	'package_amount',
    	'description',
        'date',
    ];

    public function registration() {

    	return $this->hasMany(Registration::class,'package','id');
    }
}
