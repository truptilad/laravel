<?php

namespace Modules\Admin\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Admin\Entities\Package;
use Modules\Admin\Entities\Registration;
use Modules\Filemanager\Entities\File;
use Modules\User\Entities\Sentinel\User;
use Carbon\Carbon;

class Registration extends Model
{
    use SoftDeletes;

    protected $table = 'admin__registrations';
    protected $fillable = [
    	'id',
    	'package',
    	'refrences_by',
    	'first_name',
        'father_name',
    	'last_name',
    	'date_of_birth',
    	'address',
    	'city',
    	'phone_no',
    	'adhar_no',
    	'voting_no',
    	'pan_card_no',
    	'other_proof',
    	'profile_img_id',
    	'age',
    	'email_id',
    	'password',
    	'TTDSEVAID',
    	'TTDSEVAPASSWORD',
        'status',
        'amount',
        'paid',
        'user_id',
    ];

    /**
     * @param $value
     */
    public function setdateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value);
    }

    public function packageInfo()
    {
        return $this->belongsTo(Package::class,'package','id');
    }

    public function referanceBy()
    {
        return $this->belongsTo(Registration::class,'refrences_by','id');
    }

     public function file()
    {
        return $this->belongsTo(File::class,'profile_img_id','id');
    }

    public function user() 
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
