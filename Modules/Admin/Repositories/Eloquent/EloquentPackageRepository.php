<?php

namespace Modules\Admin\Repositories\Eloquent;

use Modules\Admin\Repositories\PackageRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPackageRepository extends EloquentBaseRepository implements PackageRepository
{
}
