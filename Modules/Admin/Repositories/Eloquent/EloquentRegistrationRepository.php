<?php

namespace Modules\Admin\Repositories\Eloquent;

use Modules\Admin\Repositories\RegistrationRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Filemanager\Helper\FileMetadata;
use Modules\User\Contracts\Authentication;
use Modules\Filemanager\Repositories\FileRepository;
use Modules\User\Repositories\UserRepository;
use Modules\Admin\Entities\Registration;
use Modules\Accounting\Repositories\TransactionRepository;
use Modules\Accounting\Repositories\PaymentRepository;
use Modules\Admin\Repositories\PackageRepository;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;

class EloquentRegistrationRepository extends EloquentBaseRepository implements RegistrationRepository
{
	public function newRegistration($request) {

		if(isset($request->user_id)){
            $user = app('Modules\User\Entities\Sentinel\User')->find($request->user_id);
			
            if($user->email != $request->email){

                $user->email = $request->email;
                $user->save();
            }
        }
        else
        {
            if(isset($request->email)) {

                $existingUser = app('Modules\User\Entities\Sentinel\User')->where('email','=',$request->email)->first();

                if(isset($existingUser)) {
                    $user = $existingUser;                   
                }
                else 
                {
                    $user = $this->createUser($request->all(),$request->email);
                    
                }
            }
            else
                {
                    $email = $request->phone_no . '@' . $request->first_name . '.com';
                    $user = $this->createUser($request->all(),$email);

                }
        }

        $newRegistration = $this->create($request->all());
        $newRegistration->user_id = $user['id'];
        $newRegistration->date_of_birth = carbon::parse($request->date_of_birth)->toDateString();
        $newRegistration->pending_amount = $newRegistration->amount -$newRegistration->paid;     
        $newRegistration->save();

        if(isset($request->profile_img_id)) {
            if ($request->file('profile_img_id')->isValid()) {

                $fileMetadata = new FileMetadata();

                $fileMetadata->description = "Description";
                $fileMetadata->fileStatus = 1;
                $fileMetadata->ownerUser = auth()->user();
                $fileMetadata->isPublic = true;
                $fileMetadata->versionNo = 1;
                $fileMetadata->fileIdentifierFolder = 'registration';

                $fileObject = app('Modules\Filemanager\Repositories\FileRepository')->createNewFile($request->profile_img_id, PROFILE_IMG, $fileMetadata);

            }

            $newRegistration->profile_img_id = $fileObject->id;
            $newRegistration->save();
            $user->profile_img_id = $fileObject->id;
            $user->save();
        }
            $pendingAmount = ($request->amount - $request->paid);

            $data = array(
                'registration_id'=> $newRegistration->id,
                'package_id' => $request->package,
                'mode' => 0,
                'amount'=> $request->paid,
                'account_id'=> 1,
                'cheque_bank'=> null,
                'cheque_branch'=> null,
                'cheque_date'=> null,
                'cheque_number'=> null,
                'date'=> carbon::now(),
            );

            if($request->paid != '0.00'){
                    //Add Entry In Transaction
                $transaction = app(TransactionRepository::class)->createPaymentTransaction($data);

                //Make Payment
                $newPayment = app(PaymentRepository::class)->allWithBuilder()->create($data + ['transaction_id' => $transaction->id]);

                $paymentMessage = "Dear " . $newRegistration['first_name']." ". $newRegistration['lad_name'].","."Your Payment of amount Rs. ".$request->paid." is Successfully Done".".";

                // $this->sendSms($newRegistration,$paymentMessage);

            }

            
            if($request->paid == '0.00') {
                $newRegistration->status = 'Pending';
                $newRegistration->save();
            }
            elseif ($pendingAmount < $request->amount  && $request->paid > '0.00' ) {
                $newRegistration->status = 'Partially Paid';
                $newRegistration->save();
            }
            else {
                $newRegistration->status = 'Paid';
                $newRegistration->save();
            }
	}


    public function createExcel($data, $type) {

        $excel = Excel::create('Record_Sheet', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);

    }

    public function createUser($request,$email) {

        $user = app(UserRepository::class)->createWithRoles([
                'email' => $email,
                'password' => $request['first_name'],
                'first_name' => $request['first_name'],
                'last_name' => (isset($request['last_name']))?$request['last_name'] : null,
            ], ['passenger' => PASSENGER_ROLE], true);
        $user->user_type_id = PASSENGER;

        $user->father_name = isset($request['father_name'])?$request['father_name']:null;
        $user->address = isset($request['address'])?$request['address']:null;
        $user->city = isset($request['city'])?$request['city']:null;
        $user->phone_no = isset($request['phone_no'])?$request['phone_no']:null;
        $user->adhar_no = isset($request['adhar_no'])?$request['adhar_no']:null;
        $user->voting_no = isset($request['voting_no'])?$request['voting_no']:null;
        $user->pan_card_no = isset($request['pan_card_no'])?$request['pan_card_no']:null;
        $user->other_proof = isset($request['other_proof'])?$request['other_proof']:null;
        $user->profile_img_id = isset($request['first_name'])?$request['first_name']:null;
        $user->age = isset($request['age'])?$request['age']:null;
        $user->TTDSEVAID = isset($request['TTDSEVAID'])?$request['TTDSEVAID']:null;
        $user->TTDSEVAPASSWORD = isset($request['TTDSEVAPASSWORD'])?$request['TTDSEVAPASSWORD']:null;
        $user->date_of_birth = carbon::parse($request['date_of_birth'])->toDateString();

        $user->save();

        return $user;
    }

    function groupRegistration($data){
        
        $package = app(PackageRepository::class)->allWithBuilder()->where('id','=',$data['package_id'])->first();

        foreach ($data['members_id'] as $member) {
                
            $userInfo = app('Modules\User\Entities\Sentinel\User')->where('id','=',$member)->first();

            $newRegister = [
                'package' => $package->id,
                'refrences_by' => $data['refrences_by'],
                'first_name' => $userInfo->first_name,
                'last_name' => $userInfo->last_name,
                'amount' => $package->package_amount,
                'phone_no' => $userInfo->phone_no,
                'status' => 'Pending',
            ];

            $newRegistration = $this->create($newRegister);

            $message = "Dear " . $newRegistration['first_name']." ". $newRegistration['lad_name'].","."You have register for ".$package->package.". "."Tour is Organise on date ".$package->date.".";

            $this->sendSms($newRegistration,$message);
            
        }
    }

    function sendSms($registerData,$message) {

        $config = new \Accunity\SMSSender\SMSConfig();
        $config->mobileNo = $registerData['phone_no'];
        $config->message = $message;
        $smsResponse =  $config->send();
    }
}
