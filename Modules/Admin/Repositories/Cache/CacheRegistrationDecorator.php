<?php

namespace Modules\Admin\Repositories\Cache;

use Modules\Admin\Repositories\RegistrationRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheRegistrationDecorator extends BaseCacheDecorator implements RegistrationRepository
{
    public function __construct(RegistrationRepository $registration)
    {
        parent::__construct();
        $this->entityName = 'admin.registrations';
        $this->repository = $registration;
    }
}
