<?php

namespace Modules\Admin\Repositories\Cache;

use Modules\Admin\Repositories\PackageRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePackageDecorator extends BaseCacheDecorator implements PackageRepository
{
    public function __construct(PackageRepository $package)
    {
        parent::__construct();
        $this->entityName = 'admin.packages';
        $this->repository = $package;
    }
}
