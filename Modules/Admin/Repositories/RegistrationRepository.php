<?php

namespace Modules\Admin\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface RegistrationRepository extends BaseRepository
{
}
