<?php

namespace Modules\Admin\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface PackageRepository extends BaseRepository
{
}
