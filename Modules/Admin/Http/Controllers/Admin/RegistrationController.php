<?php

namespace Modules\Admin\Http\Controllers\Admin;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Modules\Admin\Entities\Registration;
use Modules\Admin\Http\Requests\CreateRegistrationRequest;
use Modules\Admin\Http\Requests\UpdateRegistrationRequest;
use Modules\Admin\Repositories\RegistrationRepository;
use Modules\Admin\Repositories\PackageRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Filemanager\Helper\FileMetadata;
use Modules\User\Contracts\Authentication;
use Modules\Filemanager\Repositories\FileRepository;
use Modules\User\Repositories\UserRepository;
use Modules\User\Entities\Sentinel\User;
use App\DataTables\RegistrattionDataTable;
use Carbon\Carbon;
use Exception;
use DB;


class RegistrationController extends AdminBaseController
{
    /**
     * @var RegistrationRepository
     */
    private $registration;

    public function __construct(RegistrationRepository $registration,Authentication $auth)
    {
        parent::__construct();

        $this->registration = $registration;
        $this->auth = $auth;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $registrations = app(RegistrationRepository::class)->allWithBuilder()->with('packageInfo')->get(); 

        
        return  view('admin::admin.registrations.index',compact('registrations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $users = app('Modules\User\Entities\Sentinel\User')
            ->where('user_type_id','=',PASSENGER)
            ->select('users.id','users.first_name','users.last_name')
            ->get();

        $packages = app(PackageRepository::class)->allWithBuilder()
            ->orderBy('package')
            ->pluck('package','id');

        $registrations = app(RegistrationRepository::class)->allWithBuilder()
            ->orderBy('first_name')
            ->pluck('first_name','id');

        $packageAll = app(PackageRepository::class)->allWithBuilder()->get();

        return view('admin::admin.registrations.create',compact('users','packages','registrations','packageAll'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRegistrationRequest $request
     * @return Response
     */
    public function store(CreateRegistrationRequest $request)
    {
        try{
            DB::beginTransaction();

            $this->registration->newRegistration($request);
            
            DB::commit();

        return redirect()->route('admin.admin.registration.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('admin::registrations.title.registrations')]));

        }catch (\Exception $e) {

            DB::rollback();

            return redirect()->back()
                ->withError('Oopsss, Something went wrong. Please try again.'.$e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Registration $registration
     * @return Response
     */
    public function edit(Registration $registration)
    {
        $users = app('Modules\User\Entities\Sentinel\User')
            ->join('admin__registrations','users.id','=','admin__registrations.user_id')
            ->where('user_type_id','=',PASSENGER)
            ->where('deleted_at','=',Null)
            ->select('users.id','users.first_name','users.last_name')
            ->get();

        $packages = app(PackageRepository::class)->allWithBuilder()
            ->orderBy('package')
            ->pluck('package','id');

        $registrations = app(RegistrationRepository::class)->allWithBuilder()
            ->orderBy('first_name')
            ->pluck('first_name','id');

        $packageAll =  app(PackageRepository::class)->allWithBuilder()->get();

        return view('admin::admin.registrations.edit', compact('registration','packages','registrations','packageAll','users'));
    }

    /**
     * Show the form for view the specified resource.
     *
     * @param  Registration $registration
     * @return Response
     */
    public function view(Registration $registration)
    {
        $packages = app(PackageRepository::class)->allWithBuilder()
            ->orderBy('package')
            ->pluck('package','id');

        $registrations = app(RegistrationRepository::class)->allWithBuilder()
            ->orderBy('first_name')
            ->pluck('first_name','id');

        $packageAll = app(PackageRepository::class)->allWithBuilder()->get();

        return view('admin::admin.registrations.view', compact('registration','packages','registrations','packageAll'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Registration $registration
     * @param  UpdateRegistrationRequest $request
     * @return Response
     */
    public function update(Registration $registration, UpdateRegistrationRequest $request)
    {
        try{
            DB::beginTransaction();

            $oldAmount = $registration->paid;

            $updateRegistration = $this->registration->update($registration, $request->all());
            $updateRegistration->date_of_birth = carbon::parse($request->date_of_birth)->toDateString();
            $updateRegistration->save();

            if(isset($request->profile_img_id)) {
                if ($request->file('profile_img_id')->isValid()) {

                    $fileMetadata = new FileMetadata();

                    $fileMetadata->description = "Description";
                    $fileMetadata->fileStatus = 1;
                    $fileMetadata->ownerUser = $this->auth->user();
                    $fileMetadata->isPublic = true;
                    $fileMetadata->versionNo = 1;
                    $fileMetadata->fileIdentifierFolder = 'registration';

                    $fileObject = app('Modules\Filemanager\Repositories\FileRepository')->createNewFile($request->profile_img_id, PROFILE_IMG, $fileMetadata);

                }

                $updateRegistration->profile_img_id = $fileObject->id;
                $updateRegistration->save();
            }

            DB::commit();

            return redirect()->route('admin.admin.registration.index',$registration->id)
                ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('admin::registrations.title.registrations')]));


        }catch (\Exception $e) {

            DB::rollback();

            return redirect()->back()
                ->withError('Oopsss, Something went wrong. Please try again.'.$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Registration $registration
     * @return Response
     */
    public function destroy(Registration $registration)
    {
        $this->registration->destroy($registration);

        return redirect()->route('admin.admin.registration.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('admin::registrations.title.registrations')]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function fetchAllData(Request $request)
    {
        
        // \DB::connection()->enableQueryLog();
        $query = app(RegistrationRepository::class)->allWithBuilder()->with('packageInfo','referanceBy');

        if($request->status != null){
            $query =  $query->orWhere('status','=', $request->status);
        }
        if($request->first_name != null){
            $query =  $query->orWhere('first_name','like', '%'.$request->first_name.'%');
        }
        if($request->referance_by != null){
            $referance_id = app(RegistrationRepository::class)->allWithBuilder()->where('first_name','like', '%'.$request->referance_by.'%')->first();
            $query =  $query->orWhere('refrences_by','=',$referance_id->id);
        }

        $query =  $query->whereBetween('created_at', [Carbon::parse($request->startDate)->format('Y-m-d'), Carbon::parse($request->endDate)->format('Y-m-d')]);

        // $queries = \DB::getQueryLog();
        // return dd($queries);
        
        return DataTables::of($query)->make(true);
    }
    /**
     * Display a listing of the packages against request Id.
     *
     * @return Response
     */
    public function getPackages($Id) {

        $registrationUserId = app(RegistrationRepository::class)->allWithBuilder()->find($Id);

        $registorPackages = app(RegistrationRepository::class)->allWithBuilder()->with('packageInfo')->where('user_id','=', $registrationUserId->user_id)->distinct('package')->get();

        return response()->json([ 'success' => true,'registorPackages' => $registorPackages]);
    }

    public function getUserDetails(Request $request) {

        // $userDetails = app(RegistrationRepository::class)->allWithBuilder()->where('user_id','=',$request->user_id)->first();

        $userDetails = app('Modules\User\Entities\Sentinel\User')->getQuery()->where('id','=',$request->user_id)->first();
        
        return response()->json(['success' => true, 'userDetails' => $userDetails]); 
    }

    public function getRegistrationExcel($type)
    {
        $data = app(RegistrationRepository::class)->all();
        
        app(RegistrationRepository::class)->createExcel($data,$type);
        
        return redirect()->route('admin.accounting.registrations.index');
    }

    public function postGroupRegistrations(Request $request)
    {
        // try{
        //     DB::beginTransaction();
        
            $this->registration->groupRegistration($request->all());
            
            DB::commit();

            return redirect()->route('admin.admin.package.index')
            ->withSuccess('All Members Successfully register for Tour');

        // }catch (\Exception $e) {

        //     DB::rollback();

        //     return redirect()->back()
        //         ->withError('Oopsss, Something went wrong. Please try again.'.$e->getMessage());
        // }
    }
    
}
