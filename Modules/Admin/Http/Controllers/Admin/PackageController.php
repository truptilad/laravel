<?php

namespace Modules\Admin\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Admin\Entities\Package;
use Modules\Admin\Http\Requests\CreatePackageRequest;
use Modules\Admin\Http\Requests\UpdatePackageRequest;
use Modules\Admin\Repositories\PackageRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Admin\Repositories\RegistrationRepository;
use Modules\Accounting\Repositories\PaymentRepository;
use Modules\Accounting\Repositories\ExpenseRepository;
use Modules\Accounting\Repositories\IncomeRepository;
use DB;
use Carbon\Carbon;

class PackageController extends AdminBaseController
{
    /**
     * @var PackageRepository
     */
    private $package;

    public function __construct(PackageRepository $package)
    {
        parent::__construct();

        $this->package = $package;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $packages = app(PackageRepository::class)->allWithBuilder()->orderBy('date','desc')->get();

        $registrations = app(RegistrationRepository::class)->allWithBuilder()->with('referanceBy')->get();

        return view('admin::admin.packages.index', compact('packages','registrations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin::admin.packages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePackageRequest $request
     * @return Response
     */
    public function store(CreatePackageRequest $request)
    {
        try{
            DB::beginTransaction();

        $newPackage = $this->package->create($request->all());
        $newPackage->date = carbon::parse($request->date)->toDateString();
        $newPackage->save();
    
            DB::commit();

        return redirect()->route('admin.admin.package.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('admin::packages.title.packages')]));

        }catch (\Exception $e) {

            DB::rollback();

            return redirect()->back()
                ->withError('Oopsss, Something went wrong. Please try again.'.$e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Package $package
     * @return Response
     */
    public function edit(Package $package)
    {
        return view('admin::admin.packages.edit', compact('package'));
    }

    /**
     * Show the form for view the specified resource.
     *
     * @param  Package $package
     * @return Response
     */
    public function view(Package $package)
    {
        $package = $this->package->find($package->id);
        $registrations = app(RegistrationRepository::class)->allWithBuilder()->with('packageInfo','referanceBy')->where('package','=',$package->id)->get();
        $payments = app(PaymentRepository::class)->allWithBuilder()->where('package_id','=',$package->id)->with('registration','registration.referanceBy')->get();
        $incomes = app(IncomeRepository::class)->allWithBuilder()->where('package_id','=',$package->id)->get();
        $expences = app(ExpenseRepository::class)->allWithBuilder()->with('category','account')->where('package_id','=',$package->id)->get();

        return view('admin::admin.packages.view',compact('package','registrations','payments','incomes','expences'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Package $package
     * @param  UpdatePackageRequest $request
     * @return Response
     */
    public function update(Package $package, UpdatePackageRequest $request)
    {
        $updatePackage = $this->package->update($package, $request->all());
        $updatePackage->date = carbon::parse($request->date)->toDateString();
        $updatePackage->save();
        
        return redirect()->route('admin.admin.package.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('admin::packages.title.packages')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Package $package
     * @return Response
     */
    public function destroy(Package $package)
    {
        $this->package->destroy($package);

        return redirect()->route('admin.admin.package.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('admin::packages.title.packages')]));
    }

    public function getPackageExcel($type)
    {
        $data = app(PackageRepository::class)->all();
        
        app(RegistrationRepository::class)->createExcel($data,$type);
        
        return redirect()->route('admin.accounting.package.index');
    }

    public function getAllMenbers()
    {
        $allMembers = app('Modules\User\Entities\Sentinel\User')
            ->where('user_type_id','=',PASSENGER)
            ->select('users.id','users.first_name','users.last_name')
            ->get();
        
        return response()->json(['success' => true ,'allMembers' => $allMembers]);
    }

    public function getGroupModal(Request $request) {

        $packageId = $request->packageId;
        $packages = app(PackageRepository::class)->allWithBuilder()->orderBy('date','desc')->get();

        return view('accounting::modal.group-message-modal',compact('packages','packageId'));
    }

    public function postGroupMessage(Request $request) {

        $registrations = app(RegistrationRepository::class)->allWithBuilder()->where('package','=',$request->packageId)->get();

        foreach ($registrations as $registration) {
            
            app(RegistrationRepository::class)->sendSms($registration,$request->message);

        }

        return response()->json([ 'success' => true]);
    }
}
