<tbody>
                            <?php if (isset($registrations)): ?>
                            <?php foreach ($registrations as $registration): ?>
                            <tr>
                                <td>
                                    <a href="{{ route('admin.admin.registration.edit', [$registration->id]) }}">
                                        {{ $registration->id }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.admin.registration.edit', [$registration->id]) }}">
                                        {{ isset($registration->packageInfo)?$registration->packageInfo->package:'' }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.admin.registration.edit', [$registration->id]) }}">
                                        {{ $registration->first_name }} {{ $registration->last_name }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.admin.registration.edit', [$registration->id]) }}">
                                        {{ isset($registration->referanceBy)?'referance By -'.$registration->referanceBy->first_name:$registration->first_name }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.admin.registration.edit', [$registration->id]) }}">
                                        {{ $registration->phone_no }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.admin.registration.edit', [$registration->id]) }}">
                                        {{ isset($registration->status)?$registration->status:'' }}
                                    </a>
                                </td>
                                <td>
                                    <?php $pendingAmount = $registration->amount - $registration->paid ; ?>
                                    <a href="{{ route('admin.admin.registration.edit', [$registration->id]) }}">

                                        {{ $pendingAmount }}
                                    </a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.admin.registration.edit', [$registration->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <a href="{{ route('admin.admin.registration.view', [$registration->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-eye"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.admin.registration.destroy', [$registration->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>