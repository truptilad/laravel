<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/admin'], function (Router $router) {
    $router->bind('registration', function ($id) {
        return app('Modules\Admin\Repositories\RegistrationRepository')->find($id);
    });
    $router->get('registrations', [
        'as' => 'admin.admin.registration.index',
        'uses' => 'RegistrationController@index',
        'middleware' => 'can:admin.registrations.index'
    ]);
    $router->get('registrations/create', [
        'as' => 'admin.admin.registration.create',
        'uses' => 'RegistrationController@create',
        'middleware' => 'can:admin.registrations.create'
    ]);
    $router->post('registrations', [
        'as' => 'admin.admin.registration.store',
        'uses' => 'RegistrationController@store',
        'middleware' => 'can:admin.registrations.create'
    ]);
    $router->get('registrations/{registration}/edit', [
        'as' => 'admin.admin.registration.edit',
        'uses' => 'RegistrationController@edit',
        'middleware' => 'can:admin.registrations.edit'
    ]);
    $router->get('registrations/{registration}/view', [
        'as' => 'admin.admin.registration.view',
        'uses' => 'RegistrationController@view',
        'middleware' => 'can:admin.registrations.view'
    ]);
    $router->put('registrations/{registration}', [
        'as' => 'admin.admin.registration.update',
        'uses' => 'RegistrationController@update',
        'middleware' => 'can:admin.registrations.edit'
    ]);
    $router->delete('registrations/{registration}', [
        'as' => 'admin.admin.registration.destroy',
        'uses' => 'RegistrationController@destroy',
        'middleware' => 'can:admin.registrations.destroy'
    ]);
    $router->bind('package', function ($id) {
        return app('Modules\Admin\Repositories\PackageRepository')->find($id);
    });
    $router->get('packages', [
        'as' => 'admin.admin.package.index',
        'uses' => 'PackageController@index',
        'middleware' => 'can:admin.packages.index'
    ]);
    $router->get('packages/create', [
        'as' => 'admin.admin.package.create',
        'uses' => 'PackageController@create',
        'middleware' => 'can:admin.packages.create'
    ]);
    $router->post('packages', [
        'as' => 'admin.admin.package.store',
        'uses' => 'PackageController@store',
        'middleware' => 'can:admin.packages.create'
    ]);
    $router->get('packages/{package}/edit', [
        'as' => 'admin.admin.package.edit',
        'uses' => 'PackageController@edit',
        'middleware' => 'can:admin.packages.edit'
    ]);
    $router->get('packages/{package}/view', [
        'as' => 'admin.admin.package.view',
        'uses' => 'PackageController@view',
        'middleware' => 'can:admin.packages.view'
    ]);
    $router->put('packages/{package}', [
        'as' => 'admin.admin.package.update',
        'uses' => 'PackageController@update',
        'middleware' => 'can:admin.packages.edit'
    ]);
    $router->delete('packages/{package}', [
        'as' => 'admin.admin.package.destroy',
        'uses' => 'PackageController@destroy',
        'middleware' => 'can:admin.packages.destroy'
    ]);
    $router->get('/fetchAllData', [
        'as' => 'fetchAllData',
        'uses' => 'RegistrationController@fetchAllData',
    ]);
    $router->get('/getPackages/{Id}', [
        'as' => 'admin.admin.registration.getPackages',
        'uses' => 'RegistrationController@getPackages',
    ]);
    $router->get('/getUserDetails', [
        'as' => 'getUserDetails',
        'uses' => 'RegistrationController@getUserDetails',
    ]);
    $router->get('/getRegistrationExcel/{type}', [
        'as' => 'getRegistrationExcel',
        'uses' => 'RegistrationController@getRegistrationExcel',
    ]);
    $router->get('/getPackageExcel/{type}', [
        'as' => 'getPackageExcel',
        'uses' => 'PackageController@getPackageExcel',
    ]);
    $router->get('/getAllMenbers', [
        'as' => 'getAllMenbers',
        'uses' => 'PackageController@getAllMenbers',
    ]);
    $router->post('/postGroupRegistrations', [
        'as' => 'postGroupRegistrations',
        'uses' => 'RegistrationController@postGroupRegistrations',
    ]);
    $router->get('/getGroupModal', [
        'as' => 'getGroupModal',
        'uses' => 'PackageController@getGroupModal',
    ]);
    $router->post('/postGroupMessage', [
        'as' => 'postGroupMessage',
        'uses' => 'PackageController@postGroupMessage',
    ]);
    

    
    
    

    
// append


});
