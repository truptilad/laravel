<?php

namespace Modules\Admin\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreatePackageRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'package' => 'required|unique:admin__packages,package', 
         ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'package.required' => 'Please Select Package Name',
            
        ];
    }

    public function translationMessages()
    {
        return [];
    }
}
