<?php

namespace Modules\Admin\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateRegistrationRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_no' => 'required|numeric|digits:10'
            'adhar_no' => 'required',
            'date_of_birth' => 'required',
         ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Please enter First name',
            'last_name.required' => 'Please enter last name',
            'phone_no.required' => 'Phone number is required',
            'adhar_no.required' => 'Please enter Adhar No.',
            'date_of_birth.required' => 'Date of Birth is required',  
        ];
    }

    public function translationMessages()
    {
        return [];
    }
}
