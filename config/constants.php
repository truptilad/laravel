<?php


if(!(defined('APP_NAME'))){

    define('APP_NAME','Accunity Webapp');

    define('MOMENT_DATE_FORMAT','DD-MM-YYYY');
    define('MOMENT_DATE_TIME_FORMAT','DD-MM-YYYY h:mm:ss a');

    define('PHP_DATE_FORMAT','d-M-Y');
    define('PHP_DATE_TIME_FORMAT','d-m-Y g:i a');

    //Register Profile Image ID
    define('PROFILE_IMG', 1);

    //transaction type details

    define('DEBIT_INDICATOR', 'Expense');
    define('CREDIT_INDICATOR', 'Payment');
    define('CREDIT_BY_INCOME', 'Income');

    //branch details
    define('BRANCH_ID', 1);

    // File Type 
    define('PAYMENT_DOCS', 2);
    define('INCOME_DOCS', 4);

    //passenger role id
    define('PASSENGER_ROLE', 3);

    //passenger user type 
    define('PASSENGER', 3);

    //excel file type
    define('CSV_TYPE', 'CSV');


}