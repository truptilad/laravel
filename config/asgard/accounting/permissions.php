<?php

return [

    'accounting.module'=>[
        'index' => 'View Accounting Module'
    ],
    
    'accounting.accounttypes' => [
        'index' => 'accounting::accounttypes.list resource',
        'create' => 'accounting::accounttypes.create resource',
        'edit' => 'accounting::accounttypes.edit resource',
        'destroy' => 'accounting::accounttypes.destroy resource',
    ],
    'accounting.accounts' => [
        'index' => 'accounting::accounts.list resource',
        'create' => 'accounting::accounts.create resource',
        'edit' => 'accounting::accounts.edit resource',
        'destroy' => 'accounting::accounts.destroy resource',
    ],
    'accounting.expensecategories' => [
        'index' => 'accounting::expensecategories.list resource',
        'create' => 'accounting::expensecategories.create resource',
        'edit' => 'accounting::expensecategories.edit resource',
        'destroy' => 'accounting::expensecategories.destroy resource',
    ],
    'accounting.expenses' => [
        'index' => 'accounting::expenses.list resource',
        'create' => 'accounting::expenses.create resource',
        'edit' => 'accounting::expenses.edit resource',
        'destroy' => 'accounting::expenses.destroy resource',
    ],
    'accounting.invoices' => [
        'index' => 'accounting::invoices.list resource',
        'create' => 'accounting::invoices.create resource',
        'edit' => 'accounting::invoices.edit resource',
        'destroy' => 'accounting::invoices.destroy resource',
    ],
    'accounting.quotations' => [
        'index' => 'accounting::quotations.list resource',
        'create' => 'accounting::quotations.create resource',
        'edit' => 'accounting::quotations.edit resource',
        'destroy' => 'accounting::quotations.destroy resource',
    ],
    'accounting.payments' => [
        'index' => 'accounting::payments.list resource',
        'create' => 'accounting::payments.create resource',
        'edit' => 'accounting::payments.edit resource',
        'destroy' => 'accounting::payments.destroy resource',
    ],
    'accounting.transactions' => [
        'index' => 'accounting::transactions.list resource',
        'create' => 'accounting::transactions.create resource',
        'edit' => 'accounting::transactions.edit resource',
        'destroy' => 'accounting::transactions.destroy resource',
    ],
    'accounting.companies' => [
        'index' => 'accounting::companies.list resource',
        'create' => 'accounting::companies.create resource',
        'edit' => 'accounting::companies.edit resource',
        'destroy' => 'accounting::companies.destroy resource',
    ],
    'accounting.branches' => [
        'index' => 'accounting::branches.list resource',
        'create' => 'accounting::branches.create resource',
        'edit' => 'accounting::branches.edit resource',
        'destroy' => 'accounting::branches.destroy resource',
    ],
    'accounting.incomes' => [
        'index' => 'accounting::incomes.list resource',
        'create' => 'accounting::incomes.create resource',
        'edit' => 'accounting::incomes.edit resource',
        'destroy' => 'accounting::incomes.destroy resource',
    ],
    'accounting.outstandings' => [
        'index' => 'accounting::outstandings.list resource',
        'create' => 'accounting::outstandings.create resource',
        'edit' => 'accounting::outstandings.edit resource',
        'destroy' => 'accounting::outstandings.destroy resource',
    ],
// append







];
