<?php
/**
 * Created by PhpStorm.
 * User: kiran
 * Date: 30/9/17
 * Time: 4:51 PM
 */

namespace App\Libraries;


class Utils
{
    public static function parseFloat($value)
    {
        $value = preg_replace('/[^0-9\.\-]/', '', $value);

        return floatval($value);
    }

    public static function parseInt($value)
    {
        $value = preg_replace('/[^0-9]/', '', $value);

        return intval($value);
    }

    public static function getFromCache($id, $type)
    {
        $cache = Cache::get($type);

        if (! $cache) {
            static::logError("Cache for {$type} is not set");

            return null;
        }

        $data = $cache->filter(function ($item) use ($id) {
            return $item->id == $id;
        });

        return $data->first();
    }

    public static function formatMoney($value, $currencyId = false, $countryId = false, $decorator = false)
    {
        $value = floatval($value);

        if (! $currencyId) {
            $currencyId = Session::get(SESSION_CURRENCY, DEFAULT_CURRENCY);
        }

        if (! $decorator) {
            $decorator = Session::get(SESSION_CURRENCY_DECORATOR, CURRENCY_DECORATOR_SYMBOL);
        }

        if (! $countryId && Auth::check()) {
            $countryId = Auth::user()->account->country_id;
        }

        $currency = self::getFromCache($currencyId, 'currencies');
        $thousand = $currency->thousand_separator;
        $decimal = $currency->decimal_separator;
        $precision = $currency->precision;
        $code = $currency->code;
        $swapSymbol = $currency->swap_currency_symbol;

        if ($countryId && $currencyId == CURRENCY_EURO) {
            $country = self::getFromCache($countryId, 'countries');
            $swapSymbol = $country->swap_currency_symbol;
            if ($country->thousand_separator) {
                $thousand = $country->thousand_separator;
            }
            if ($country->decimal_separator) {
                $decimal = $country->decimal_separator;
            }
        }

        $value = number_format($value, $precision, $decimal, $thousand);
        $symbol = $currency->symbol;

        if ($decorator == CURRENCY_DECORATOR_NONE) {
            return $value;
        } elseif ($decorator == CURRENCY_DECORATOR_CODE || ! $symbol) {
            return "{$value} {$code}";
        } elseif ($swapSymbol) {
            return "{$value} " . trim($symbol);
        } else {
            return "{$symbol}{$value}";
        }
    }

}