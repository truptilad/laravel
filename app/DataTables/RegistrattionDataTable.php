<?php

namespace App\DataTables;

use Modules\Admin\Entities\Registration;
use Yajra\DataTables\Services\DataTable;
use DB;



class RegistrattionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', 'registrattiondatatable.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Registration $model)
    {
        // \DB::connection()->enableQueryLog();

        // $model->newQuery()->select('id');
        
        // $queries = \DB::getQueryLog();
        // return dd($queries);
        $registrations = $model->newQuery()->with('packageInfo','referanceBy','file');
        return  $this->applyScopes($registrations);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'package',
            'first_name',
            'last_name',
            'referanceBy',
            'phone_no',
            'amount',
            'paid',
            'status'

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'registrattiondatatable_' . time();
    }
}
